/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

atmega328p.c

*/

#include "microcontroller.h"
#include "timers.h"
#include "adc.h"
#include "memory.h"
#include "registers.h"
#include "debug.h"

#include "mcus/atmega328p.h"

void atmega328p_populate_microcontroller(microcontroller_t* microcontroller, uint8_t reset)
{
    uint16_t size_flash =  16384;
    uint16_t size_sram =   2304;
    uint16_t size_eeprom = 1024;
    
    microcontroller->model = "atmega328p";
    
    if (reset == 0)
    {
        microcontroller->flash = init_flash(size_flash);
        microcontroller->debugger = init_debugger(size_flash);
        microcontroller->debug_symbols = 0;
    }
    
    microcontroller->sram = init_sram(size_sram);
    microcontroller->eeprom = init_eeprom(size_eeprom);
    microcontroller->registers = init_registers();
    
    atmega328p_populate_vectors(microcontroller);
    atmega328p_populate_registers(microcontroller->registers, microcontroller->sram);
    
    microcontroller->ticks = 0;
    microcontroller->prev_ticks = 0;
    microcontroller->tick_time = 0;
    microcontroller->counter0 = 0;
    microcontroller->counter1 = 0;
    
    microcontroller->prev_PINB = 0;
    microcontroller->prev_PINC = 0;
    microcontroller->prev_PIND = 0;
    
    microcontroller->adc_start_set = 0;
    microcontroller->adc0 = 0;
    microcontroller->adc1 = 0;
    microcontroller->adc2 = 0;
    microcontroller->adc3 = 0;
    microcontroller->adc4 = 0;
    microcontroller->adc5 = 0;
    microcontroller->adc6 = 0;
    microcontroller->adc7 = 0;
    
    
    //microcontroller->n_timers = 3;
    microcontroller->n_timers = 2;

    timer_counter_t* timer0 = init_timer(0, &timer8, &timer8);
    timer0->timskx = microcontroller->registers->TIMSK0;
    timer0->tccrxa = microcontroller->registers->TCCR0A;
    timer0->tccrxb = microcontroller->registers->TCCR0B;
    timer0->tcntx = microcontroller->registers->TCNT0;
    timer0->ocrxa = microcontroller->registers->OCR0A;
    timer0->ocrxb = microcontroller->registers->OCR0B;
    microcontroller->timers[0] = timer0;
    
    timer_counter_t* timer1 = init_timer(1, &timer16_pre, &timer16_post);
    timer1->timskx = microcontroller->registers->TIMSK1;
    timer1->tccrxa = microcontroller->registers->TCCR1A;
    timer1->tccrxb = microcontroller->registers->TCCR1B;
    timer1->tccrxc = microcontroller->registers->TCCR1C;
    timer1->tcntxh = microcontroller->registers->TCNT1H;
    timer1->tcntxl = microcontroller->registers->TCNT1L;
    timer1->ocrxah = microcontroller->registers->OCR1AH;
    timer1->ocrxal = microcontroller->registers->OCR1AL;
    timer1->ocrxbh = microcontroller->registers->OCR1BH;
    timer1->ocrxbl = microcontroller->registers->OCR1BL;
    timer1->tifrx = microcontroller->registers->TIFR1;
    microcontroller->timers[1] = timer1;
/*
    timer_counter_t* timer2 = init_timer(2, &timer8);
    timer2->tccrxa = microcontroller->registers->TCCR2A;
    timer2->tccrxb = microcontroller->registers->TCCR2B;
    timer2->tcntx = microcontroller->registers->TCNT2;
    timer2->ocrxa = microcontroller->registers->OCR2A;
    timer2->ocrxb = microcontroller->registers->OCR2B;
    microcontroller->timers[2] = timer2;
*/  

    microcontroller->n_interrupts = 4;
    microcontroller->interrupts[0] = init_pin_change_interrupt(microcontroller->registers->PCMSK0, microcontroller->registers->PINB, 0x01, 0x06);
    microcontroller->interrupts[1] = init_pin_change_interrupt(microcontroller->registers->PCMSK1, microcontroller->registers->PINC, 0x02, 0x08);
    microcontroller->interrupts[2] = init_pin_change_interrupt(microcontroller->registers->PCMSK2, microcontroller->registers->PIND, 0x04, 0x0A);
    //microcontroller->interrupts[3] = init_timer_interrupt(timer0, 0x20, 0x1C, 0x1E, 0x00);
    microcontroller->interrupts[3] = init_timer_interrupt(timer1, 0x16, 0x18, 0x1A, 0x14);
  
    microcontroller->handle_adc = handle_adc_single_ended_only;
    
    char* sources;
    char* source_names;
    
    microcontroller->vcd_vars_count = 0;
    int i = 0;
    for (i; i<100; i++)
        microcontroller->vcd_vars[i] = 0;
}

void atmega328p_populate_registers(registers_t* registers, sram_t* sram)
{
    registers->SPL    = sram->data + 0x5D;
    registers->SPH    = sram->data + 0x5E;
    registers->SREG   = sram->data + 0x5F;
    registers->r      = sram->data + 0x00;
    
    registers->PORTB  = sram->data + 0x25;
    registers->DDRB   = sram->data + 0x24;
    registers->PINB   = sram->data + 0x23;
    
    registers->PORTC  = sram->data + 0x28;
    registers->DDRC   = sram->data + 0x27;
    registers->PINC   = sram->data + 0x26;
    
    registers->PORTD  = sram->data + 0x2B;
    registers->DDRD   = sram->data + 0x2A;
    registers->PIND   = sram->data + 0x29;
    
    registers->PCICR  = sram->data + 0x68;
    registers->PCIFR  = sram->data + 0x3B;
    registers->PCMSK2 = sram->data + 0x6D;
    registers->PCMSK1 = sram->data + 0x6C;
    registers->PCMSK0 = sram->data + 0x6B;
    
    registers->GTCCR  = sram->data + 0x43;

    registers->TIFR0  = sram->data + 0x35;
    registers->TCCR0A = sram->data + 0x44;
    registers->TCCR0B = sram->data + 0x45;
    registers->TCNT0  = sram->data + 0x46;
    registers->OCR0A  = sram->data + 0x47;
    registers->OCR0B  = sram->data + 0x48;
    registers->TIMSK0 = sram->data + 0x6E;
    
    registers->TIFR1  = sram->data + 0x36;
    registers->TCCR1A = sram->data + 0x80;
    registers->TCCR1B = sram->data + 0x81;
    registers->TCCR1C = sram->data + 0x82;
    registers->TCNT1L = sram->data + 0x84;
    registers->TCNT1H = sram->data + 0x85;
    registers->ICR1L  = sram->data + 0x86;
    registers->ICR1H  = sram->data + 0x87;
    registers->OCR1AL = sram->data + 0x88;
    registers->OCR1AH = sram->data + 0x89;
    registers->OCR1BL = sram->data + 0x8A;
    registers->OCR1BH = sram->data + 0x8B;
    registers->TIMSK1 = sram->data + 0x6F;
    
    registers->ADMUX  = sram->data + 0x7C;
    registers->ADCSRA = sram->data + 0x7A;
    registers->ADCSRB = sram->data + 0x7B;
    registers->ADCH   = sram->data + 0x79;
    registers->ADCL   = sram->data + 0x78;
}

void atmega328p_populate_vectors(microcontroller_t* microcontroller)
{
    microcontroller->RESET_vect =        0x0000;
    microcontroller->INT0_vect =         0x0002;
    microcontroller->INT1_vec =          0x0004;
    microcontroller->PCINT0_vect =       0x0006;
    microcontroller->PCINT1_vect =       0x0008;
    microcontroller->PCINT2_vect =       0x000A;
    microcontroller->WDT_vect =          0x000C;
    microcontroller->TIMER2_COMPA_vect = 0x000E;
    microcontroller->TIMER2_COMPB_vect = 0x0010;
    microcontroller->TIMER2_OFV_vect =   0x0012;
    microcontroller->TIMER1_CAPT_vect =  0x0014;
    microcontroller->TIMER1_COMPA_vect = 0x0016;
    microcontroller->TIMER1_COMPB_vect = 0x0018;
    microcontroller->TIMER1_OFV_vect =   0x001A;
    microcontroller->TIMER0_COMPA_vect = 0x001C;
    microcontroller->TIMER0_COMPB_vect = 0x001E;
    microcontroller->TIMER0_OFV_vect =   0x0020;
    microcontroller->SPI_STC_vect =      0x0022;
    microcontroller->USART_RX_vect =     0x0024;
    microcontroller->USART_UDRE_vect =   0x0026;
    microcontroller->USART_TX_vect =     0x0028;
    microcontroller->ADC_vect =          0x002A;
    microcontroller->EE_READY_vect =     0x002C;
    microcontroller->ANALOG_COMP_vect =  0x002E;
    microcontroller->TWI_vect =          0x0030;
    microcontroller->SPM_READY_vect =    0x0032;
}

void atmega328p_handle_timers(microcontroller_t* microcontroller)
{

}

void atmega328p_handle_adc(microcontroller_t* microcontroller)
{

}

void atmega328p_handle_interrupts(microcontroller_t* microcontroller)
{

}
