/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

registers.c

*/


#include "registers.h"

#include <stdlib.h>
#include <stdio.h>

// http://www.avr-asm-tutorial.net/avr_en/beginner/PDETAIL.html

registers_t* init_registers()
{
    registers_t* registers = (registers_t*) malloc(sizeof(registers_t));
    
    registers->PC = (uint16_t*) malloc(sizeof(uint16_t));
    *(registers->PC) = 0;
    
    return registers;
}

void free_registers(registers_t* registers)
{
    free(registers->PC);
    
    free(registers);
}

uint16_t get_sp(registers_t* registers)
{
    uint16_t sp = *(registers->SPH);
    sp = sp << 8;
    sp |= *(registers->SPL);
    
    return sp;
}

void set_sp(registers_t* registers, uint16_t sp)
{
    *(registers->SPL) = sp;
    *(registers->SPH) = sp >> 8;
}

uint8_t* get_register(registers_t* registers, char* name)
{
    if (strcmp(name, "PORTB") == 0)
        return registers->PORTB;
    if (strcmp(name, "DDRB") == 0)
        return registers->DDRB;
    if (strcmp(name, "PINB") == 0)
        return registers->PINB;
        
    if (strcmp(name, "PORTC") == 0)
        return registers->PORTC;
    if (strcmp(name, "DDRC") == 0)
        return registers->DDRC;
    if (strcmp(name, "PINC") == 0)
        return registers->PINC;
        
    if (strcmp(name, "PORTD") == 0)
        return registers->PORTD;
    if (strcmp(name, "DDRD") == 0)
        return registers->DDRD;
    if (strcmp(name, "PIND") == 0)
        return registers->PIND;
        
    if (strcmp(name, "PCICR") == 0)
        return registers->PCICR;
    if (strcmp(name, "PCIFR") == 0)
        return registers->PCIFR;
    if (strcmp(name, "PCMSK0") == 0)
        return registers->PCMSK0;
    if (strcmp(name, "PCMSK1") == 0)
        return registers->PCMSK1;
    if (strcmp(name, "PCMSK2") == 0)
        return registers->PCMSK2;

    if (strcmp(name, "TCNT0") == 0)
        return registers->TCNT0;
    if (strcmp(name, "TCCR0A") == 0)
        return registers->TCCR0A;
    if (strcmp(name, "TCCR0B") == 0)
        return registers->TCCR0B;
    if (strcmp(name, "OCR0A") == 0)
        return registers->OCR0A;
    if (strcmp(name, "OCR0B") == 0)
        return registers->OCR0B;
    if (strcmp(name, "TIMSK0") == 0)
        return registers->TIMSK0;
    
    if (strcmp(name, "TCNT1[H]") == 0)
        return registers->TCNT1H;
    if (strcmp(name, "TCNT1[L]") == 0)
        return registers->TCNT1L;
    if (strcmp(name, "TCCR1A") == 0)
        return registers->TCCR1A;
    if (strcmp(name, "TCCR1B") == 0)
        return registers->TCCR1B;
    if (strcmp(name, "TCCR1C") == 0)
        return registers->TCCR1C;
    if (strcmp(name, "OCR1A[H]") == 0)
        return registers->OCR1AH;
    if (strcmp(name, "OCR1A[L]") == 0)
        return registers->OCR1AL;
    if (strcmp(name, "OCR1B[H]") == 0)
        return registers->OCR1BH;
    if (strcmp(name, "OCR1B[L]") == 0)
        return registers->OCR1BL;
    if (strcmp(name, "TIMSK1") == 0)
        return registers->TIMSK1;       
    
    if (strcmp(name, "ADMUX") == 0)
        return registers->ADMUX;
    if (strcmp(name, "ADCSRA") == 0)
        return registers->ADCSRA;
    if (strcmp(name, "ADCSRB") == 0)
        return registers->ADCSRB;
    if (strcmp(name, "ADCL") == 0)
        return registers->ADCL;
    if (strcmp(name, "ADCH") == 0)
        return registers->ADCH;    
        
    if (strcmp(name, "R0") == 0)
        return registers->r + 0;
    if (strcmp(name, "R1") == 0)
        return registers->r + 1;
    if (strcmp(name, "R2") == 0)
        return registers->r + 2;
    if (strcmp(name, "R3") == 0)
        return registers->r + 3;
    if (strcmp(name, "R4") == 0)
        return registers->r + 4;
    if (strcmp(name, "R5") == 0)
        return registers->r + 5;
    if (strcmp(name, "R6") == 0)
        return registers->r + 6;
    if (strcmp(name, "R7") == 0)
        return registers->r + 7;
    if (strcmp(name, "R8") == 0)
        return registers->r + 8;
    if (strcmp(name, "R9") == 0)
        return registers->r + 9;
    if (strcmp(name, "R10") == 0)
        return registers->r + 10;
    if (strcmp(name, "R11") == 0)
        return registers->r + 11;
    if (strcmp(name, "R12") == 0)
        return registers->r + 12;
    if (strcmp(name, "R13") == 0)
        return registers->r + 13;
    if (strcmp(name, "R14") == 0)
        return registers->r + 14;
    if (strcmp(name, "R15") == 0)
        return registers->r + 15;
    if (strcmp(name, "R16") == 0)
        return registers->r + 16;
    if (strcmp(name, "R17") == 0)
        return registers->r + 17;
    if (strcmp(name, "R18") == 0)
        return registers->r + 18;
    if (strcmp(name, "R19") == 0)
        return registers->r + 19;
    if (strcmp(name, "R20") == 0)
        return registers->r + 20;
    if (strcmp(name, "R21") == 0)
        return registers->r + 21;
    if (strcmp(name, "R22") == 0)
        return registers->r + 22;
    if (strcmp(name, "R23") == 0)
        return registers->r + 23;
    if (strcmp(name, "R24") == 0)
        return registers->r + 24;
    if (strcmp(name, "R25") == 0)
        return registers->r + 25;
    if (strcmp(name, "R26") == 0 || strcmp(name, "R26-X[L]") == 0)
        return registers->r + 26;
    if (strcmp(name, "R27") == 0 || strcmp(name, "R27-X[H]") == 0)
        return registers->r + 27;
    if (strcmp(name, "R28") == 0 || strcmp(name, "R28-Y[L]") == 0)
        return registers->r + 28;
    if (strcmp(name, "R29") == 0 || strcmp(name, "R29-Y[H]") == 0)
        return registers->r + 29;
    if (strcmp(name, "R30") == 0 || strcmp(name, "R30-Z[L]") == 0)
        return registers->r + 30;
    if (strcmp(name, "R31") == 0 || strcmp(name, "R31-Z[H]") == 0)
        return registers->r + 31;
    
        
        
    return 0;
}
