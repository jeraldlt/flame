/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

microcontroller.c
    
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>
#include <math.h>
#include <unistd.h>
#include <libelf.h>
#include <fcntl.h>   // open()

#include "mcus/atmega328p.h"

#include "microcontroller.h"
#include "util.h"
#include "timers.h"
#include "context.h"
#include "globals.h"

context_t* context;

microcontroller_t* init_microcontroller(char* mcu, uint8_t clock_mhz)
{
    microcontroller_t* microcontroller  = (microcontroller_t*) malloc(sizeof(microcontroller_t));
    microcontroller->clock_mhz = clock_mhz;
    
    microcontroller->n_messages = 0;
        
    if (strcmp(mcu, "atmega328p") == 0)
    {
        atmega328p_populate_microcontroller(microcontroller, FALSE);
    }
    
    
    microcontroller->plugin_manager = init_plugin_manager(microcontroller, context);
    
    microcontroller->resetting = 0;
    
    return microcontroller;
}

void free_microcontroller(microcontroller_t* microcontroller)
{
    free_registers(microcontroller->registers);
    free_flash(microcontroller->flash);
    free_sram(microcontroller->sram);
    free_eeprom(microcontroller->eeprom);
    free_debugger(microcontroller->debugger);
    
    free(microcontroller);
}

void reset_microcontroller(microcontroller_t* microcontroller)
{
    char filename[128];
    strcpy(filename, context->options->last_program);
    
    if (strcmp(microcontroller->model, "atmega328p") == 0)
    {
        atmega328p_populate_microcontroller(microcontroller, TRUE);
    }
    

    
    microcontroller->resetting = 0;

    notice(microcontroller, "Microcontroller reset");
}

int is_ihex_file(char* filepath)
{
    // Open file in read mode
    FILE* fp = fopen(filepath, "r");
    if (fp == NULL)
        return 0;
    
    // Grab last 11 chars in file, close file
    char sig[12] = {0};
    fseek(fp, -13, SEEK_END);
    fread(sig, 1, 11, fp);
    fclose(fp);
    
    // If last 11 chars match, return TRUE
    if (strcmp(sig, ":00000001FF") == 0)
        return 1;
    
    return 0;
}

int is_elf_file(char* filepath)
{
    // Open file in read bytes mode
    FILE* fp = fopen(filepath, "rb");
    if (fp == NULL)
        return 0;
    
    // Grab the 2-4 bytes in file, close file
    char sig[4] = {0};
    fseek(fp, 1, SEEK_SET);
    fread(sig, 1, 3, fp);
    fclose(fp);
    
    // return TRUE if retrieved bits match "ELF"
    if (strcmp(sig, "ELF") == 0)
        return 1;
    
    return 0;
}

int is_debug_file(char* filepath)
{
    if (is_elf_file(filepath) == 0)
        return 0;
    
    FILE* fp = fopen(filepath, "r");
    if (fp == NULL)
        return 0;
    
    //dwarfLines_t* dwarf_lines = getDwarfLines(fp);
    //if (dwarf_lines->count == 0)
    //    return 0;
    
    return 1;
}

int load_elf(microcontroller_t* microcontroller, char* filepath)
{
	int i;
    if (elf_version (EV_CURRENT) == EV_NONE)
    {
        printf("Invalid libelf version...\n");
        exit(1);
    }
    
    int fd = open(filepath, O_RDONLY);
    
    Elf* elf;
    Elf_Scn* section;
    Elf_Data* data;
    size_t shstrndx = 0;
    uint32_t size = 0;
    
    elf = elf_begin(fd, ELF_C_READ, NULL);
    
    elf_getshdrstrndx(elf , &shstrndx);
    section = NULL;
    while ((section = elf_nextscn(elf, section)) != NULL)
    {
        Elf32_Shdr* shdr = elf32_getshdr(section);
        
        if (strcmp(".text", elf_strptr(elf, shstrndx, shdr->sh_name)) == 0)
        {
            Elf_Data* data;
            data = elf_getdata(section, NULL);
            
            uint16_t word;
            int i = 0;
            size = shdr->sh_size;
            for(i; i<size/2; i++)
			{
				uint32_t data_raw = *((unsigned*) data->d_buf+i);
				
				word = data_raw;
				word = word << 8;
				word |= ((data_raw >> 8) & 0x00FF);
				microcontroller->flash->data[i*2] = word;
				
				word = data_raw >> 16;
				word = word << 8;
				word |= ((data_raw >> 24) & 0x00FF);
				microcontroller->flash->data[i*2+1] = word;
			}
        }
    }
    
    
    microcontroller->debugger = init_debugger(microcontroller->flash->size);
    microcontroller->debug_symbols = 1;

	populate_sources(microcontroller, elf);

    microcontroller->flash->start = 0;
    microcontroller->flash->end = microcontroller->flash->start + size/2;
    microcontroller->flash->name = basename(filepath);
    
    notice(microcontroller, "Loaded %i words (%i bytes) into flash at memory location 0x%X to 0x%X", size, size*2, microcontroller->flash->start*2, microcontroller->flash->end*2-2);
    
    return 0;
}

int load_hex(microcontroller_t* microcontroller, char* filepath)
{
    FILE* fp = fopen(filepath, "r");
    
    char* line = 0;
    char* data_slice = 0;
    char* size = 0;
    char* address = 0;
    char* record_type = 0;
    char* data = 0;
    char* checksum = 0;
    
    size_t len = 0;
    ssize_t read;
    uint32_t n_words = 0;
    
    microcontroller->debug_symbols = 0;
    
    while ((read = getline(&line, &len, fp)) != -1) 
    {
        size = (char*) malloc(sizeof(char) * 2);
        data_slice = (char*) malloc(sizeof(char) * 4);
        address = (char*) malloc(sizeof(char) * 4);
        record_type = (char*) malloc(sizeof(char) * 2);
        data = (char*) malloc(sizeof(char) * (read - 10));
        checksum = (char*) malloc(sizeof(char) * 2);
        
        sliceStr(line, size, 1, 2);
        sliceStr(line, address, 3, 6);
        sliceStr(line, record_type, 7, 8);
        sliceStr(line, data, 9, read-5);
        sliceStr(line, checksum, read-4, read-3);
        
        uint32_t data_index = hexToDec(address, 4) / 2;
        
        if (microcontroller->flash->start == -1)
            microcontroller->flash->start = data_index;
            
        short data_length = ((read - 5) - 8);
        uint8_t i = 0;
        for (i; i<data_length; i+=4)
        {
            sliceStr(data, data_slice, i, i+3);
            microcontroller->flash->data[data_index++] = hexToDec(data_slice, 4);
            n_words++;
        }
        
        free(size);
        free(data_slice);
        free(address);
        free(record_type);
        free(data);
        free(checksum);
        
    }
    
    if (line)
        free(line);

    
    if (microcontroller->flash->start != -1)
    {
        microcontroller->flash->name = basename(filepath);
        microcontroller->flash->end = microcontroller->flash->start + n_words;
        notice(microcontroller, "Loaded %i words (%i bytes) into flash at memory location 0x%X to 0x%X", n_words, n_words*2, microcontroller->flash->start*2, microcontroller->flash->end*2);
        fclose(fp);
        return 0;
    }
    else
    {
        printf("Failed to load hex file\n");
        fclose(fp);
        return -1;
    }
}

void call_interrupt(microcontroller_t* microcontroller, const uint32_t location)
{
    // Clear global interrupts flag
    *(microcontroller->registers->SREG) &= ~(0x80);
    
    // Get stack pointer
    uint16_t SP = get_sp(microcontroller->registers);

	// Push current PC onto stack
    microcontroller->sram->data[SP] = (*(microcontroller->registers->PC) & 0xFF00) >> 8;
    SP--;
    microcontroller->sram->data[SP] = *(microcontroller->registers->PC);
    SP--;
    
    // Set new stack pointer
    set_sp(microcontroller->registers, SP);
    
    // Set PC to interrupt vector location
    *(microcontroller->registers->PC) = location;
}

void handle_adc(microcontroller_t* microcontroller)
{
}

void handle_timers_pre(microcontroller_t* microcontroller)
{
    int i = 0;
    for(i; i<microcontroller->n_timers; i++)
    {
        microcontroller->timers[i]->handle_timer_pre(microcontroller, microcontroller->timers[i]);
    }
}

void handle_timers_post(microcontroller_t* microcontroller)
{
    int i = 0;
    for(i; i<microcontroller->n_timers; i++)
    {
        microcontroller->timers[i]->handle_timer_post(microcontroller, microcontroller->timers[i]);
    }
}

void handle_interrupts(microcontroller_t* microcontroller)
{
    if (*(microcontroller->registers->SREG) & 0x80 == 0) // global interrupt flag is not set
        return;
    
    int i = 0;
    for (i; i<microcontroller->n_interrupts; i++)
    {
        microcontroller->interrupts[i]->handler(microcontroller->interrupts[i], microcontroller);
    }
}

void notice(microcontroller_t* microcontroller, const char* fmt, ...)
{
    va_list arglist1;
    va_start(arglist1, fmt);
    
    char buf[128];
    
    vsprintf(buf, fmt, arglist1);
    
    strcpy(microcontroller->messages[microcontroller->n_messages], buf);
    microcontroller->n_messages++;

    va_end(arglist1);
}
