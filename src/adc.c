#include "adc.h"

void handle_adc_single_ended_only(microcontroller_t* microcontroller)
{
    if (*(microcontroller->registers->ADCSRA) & (0x80) == 0) // ADC enabled (ADEN) is not set
        return;
        
    if (*(microcontroller->registers->ADCSRA) & (0x60)) // ADC start (ADSC) is set
    {
        if (microcontroller->adc_start_set == 0)
        {
            microcontroller->adc_start_set = 1;
            uint8_t prescalar = *(microcontroller->registers->ADCSRA) & (0x03);
            if (prescalar)
                prescalar = pow(2, prescalar);
            else
                prescalar = 2;
            
            microcontroller->adc_target_ticks = (prescalar * 13) + microcontroller->ticks;
            
            microcontroller->adc_mode = ADC_MODE_SINGLE_ENDED;
            microcontroller->adc_channel = *(microcontroller->registers->ADMUX) & 0x0F;
        }
        
        if (microcontroller->adc_start_set && microcontroller->ticks >= microcontroller->adc_target_ticks)
        {
            microcontroller->adc_start_set = 0;
            *(microcontroller->registers->ADCSRA) &= ~(0x60); // clear ADC start (ADSC)
            
            uint16_t val = 0;
            switch (microcontroller->adc_channel)
            {
                case 0:
                    val = microcontroller->adc0;
                    break;
                case 1:
                    val = microcontroller->adc1;
                    break;
                case 2:
                    val = microcontroller->adc2;
                    break;
                case 3:
                    val = microcontroller->adc3;
                    break;
                case 4:
                    val = microcontroller->adc4;
                    break;
                case 5:
                    val = microcontroller->adc5;
                    break;
                case 6:
                    val = microcontroller->adc6;
                    break;
                case 7:
                    val = microcontroller->adc7;
                    break;
                default:
                    break;
            }
            
            if (*(microcontroller->registers->ADMUX) & (0x20))
            {
                *(microcontroller->registers->ADCL) = (((uint8_t) val) << 6);
                *(microcontroller->registers->ADCH) = (uint8_t) (val >> 2);
            }
            else
            {
                *(microcontroller->registers->ADCL) = (uint8_t) val;
                *(microcontroller->registers->ADCH) = ((uint8_t) (val >> 8)) & (0x03);
            }
        }
    }
    
    else if (microcontroller->adc_start_set)
    {
        //microcontroller->adc_start_set = 0;
    }
}
