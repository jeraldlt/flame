/*
This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

instructions.c
    
*/

// Instructions that still need to be implemented:
//
// - ldx2
// - ldx3
// - ldy2
// - ldy3
// - ldz2
// - ldz3
// - lpm1   // Program memory functions to be added later
// - lpm2   // Program memory functions to be added later
// - lpm3   // Program memory functions to be added later
// - sleep  // Sleep to be added later
// - wdr    // Watchdog functions to be added later
// - elpmz1 // Program memory functions to be added later
// - elpmz2 // Program memory functions to be added later
// - elpmz3 // Program memory functions to be added later
// - fmul
// - fmuls
// - fmulsu
// - mulsu
// - spm    // Program memory functions to be added later
// - elpmx  // Program memory functions to be added later

#include "instructions.h"

#include <stdio.h>
#include <stdint.h>

#include "util.h"
#include "microcontroller.h"
#include "registers.h"
#include "context.h"
#include "globals.h"

context_t* context;

void (*opcodes[65527]) (microcontroller_t*);

void _and(microcontroller_t* microcontroller)
{

    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    if (r == d)
    {
        _tst(microcontroller);
        return;
    }
    
    microcontroller->registers->r[d] &= microcontroller->registers->r[r];
    
    uint8_t val = microcontroller->registers->r[d];
    uint8_t* sreg = microcontroller->registers->SREG;
    
    *sreg &= ~(1  << 3); // Clear V
    
    if (val & 0x80) // Check N
    {
        *sreg |= 0x14; // Set N, S
    }
    else
    {
        *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _adc(microcontroller_t* microcontroller)
{

    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    if (r == d)
    {
        _rol(microcontroller);
        return;
    }
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t val = regd << 1;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    val |= (*sreg & 0x01); // Roll Carry
    microcontroller->registers->r[d] = val;
    
    if (regd & 0x80) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    if (val & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x01)) ) // Check V
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
    if (regd & 0x08) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H    
    

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _add(microcontroller_t* microcontroller)
{

    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    if (r == d)
    {
        _lsl(microcontroller);
        return;
    }
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t regr = microcontroller->registers->r[r];
    uint8_t val = regd + regr;
    microcontroller->registers->r[d] = val;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (regd & 0x80) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    if (val & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if ((bit(regd, 7, 0) & bit(regr, 7, 1) & bit(val, 7, 1)) | (bit(regd, 7, 1) & bit(regr, 7, 0) & bit(val, 7, 0))) // Check V
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
    if (regd & 0x08) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H    
    

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _ori(microcontroller_t* microcontroller)
{

    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    microcontroller->registers->r[d] |= K;
    
    uint8_t val = microcontroller->registers->r[d];
    uint8_t* sreg = microcontroller->registers->SREG;
    
    *sreg &= ~(1  << 3); // Clear V
    
    if (val & 0x80) // Check N
    {
        *sreg |= 0x14; // Set N, S
    }
    else
    {
        *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _clr(microcontroller_t* microcontroller)
{

    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);

    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    microcontroller->registers->r[d] = 0x00;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    
    *sreg &= ~(0x1C);
    *sreg |= 0x02;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _rol(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t val = regd << 1;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    val |= (*sreg & 0x01); // Roll Carry
    microcontroller->registers->r[d] = val;
    
    if (regd & 0x80) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    if (val & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x01)) ) // Check V
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
    if (regd & 0x08) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H    
    

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _lsl(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t val = regd << 1;
    microcontroller->registers->r[d] = val;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (regd & 0x80) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    if (val & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x01)) ) // Check V
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
    if (regd & 0x08) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H    
    

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _tst(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t val = microcontroller->registers->r[d];
    uint8_t* sreg = microcontroller->registers->SREG;
    
    *sreg &= ~(1  << 3); // Clear V
    
    if (val & 0x80) // Check N
    {
        *sreg |= 0x14; // Set N, S
    }
    else
    {
        *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _andi(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    microcontroller->registers->r[d] &= K;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    *sreg &= ~(0x08); // Clear V
    
    if (microcontroller->registers->r[d] == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if (microcontroller->registers->r[d] & 0x80) // Check N
    {
        *sreg |= 0x14; // Set S, N
    }
    else
    {
        *sreg &= ~(0x14); // Clear S, N
    }

    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _asr(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t val = regd >> 1;
    val |= (regd & 0x80);
    
    microcontroller->registers->r[d] = val;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (regd & 0x01) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    if (val & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x01)) ) // Check V
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
    if (regd & 0x08) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H    
    

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _bclr(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
	printf("here\n");
	uint8_t s = (ins1 & 0x0070) >> 4;
    
    *(microcontroller->registers->SREG) &= ~(0x01 << s);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _bld(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t b = ins1 & 0x0007;
    
    if (*(microcontroller->registers->SREG) & 0x40) // Check T
        microcontroller->registers->r[d] |= (1 << b);
    else
        microcontroller->registers->r[d] &= ~(1 << b);
        
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _brbc(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t s = (ins1 & 0x0007);
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & s)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brbs(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t s = (ins1 & 0x0007);
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }    
    if (*(microcontroller->registers->SREG) & s)
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _brsh(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x01)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brlo(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x01)
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _breq(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x02)
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _brge(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if ( ((*(microcontroller->registers->SREG) & 0x08) >> 3) ^ ((*(microcontroller->registers->SREG) & 0x04) >> 2) ) // Can we just use S?
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brhc(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x20)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brhs(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x20)
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _brid(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x80)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brie(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x80)
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _brlt(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if ( ((*(microcontroller->registers->SREG) & 0x08) >> 3) ^ ((*(microcontroller->registers->SREG) & 0x04) >> 2) ) // Can we just use S?
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _brmi(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x04)
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _brne(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x02)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brpl(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x04)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brtc(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x40)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brts(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x40)
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _brvc(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x08)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
    else
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
}

void _brvs(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    if (*(microcontroller->registers->SREG) & 0x08)
    {
        (*(microcontroller->registers->PC)) += (1 + k);
        microcontroller->ticks += 2;
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _bset(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t s = (ins1 & 0x0070) >> 4;
    
    *(microcontroller->registers->SREG) |= (1 << s);
    
    (*(microcontroller->registers->PC))++;
    microcontroller->ticks += 1;
}

void _bst(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    uint8_t b = (ins1 & 0x0007);
    
    if (microcontroller->registers->r[d] & (1 << b))
        *(microcontroller->registers->SREG) |= 0x40;
    else
        *(microcontroller->registers->SREG) &= ~(0x40);
    
    (*(microcontroller->registers->PC))++;
    microcontroller->ticks += 1;
}

void _cbi(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = (ins1 & 0x00F8) >> 3;
    A += 0x20;
    uint8_t b = ins1 & 0x0007;
    
    microcontroller->sram->data[A] &= ~(1 << b);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _clc(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) &= ~(0x01);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _clh(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) &= ~(0x20);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _cli(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) &= ~(0x80);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _cln(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) &= ~(0x04);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _eor(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    if (r == d)
    {
        _clr(microcontroller);
        return;
    }
    
    microcontroller->registers->r[d] ^= microcontroller->registers->r[r];
    
    uint8_t val = microcontroller->registers->r[d];
    uint8_t* sreg = microcontroller->registers->SREG;
    
    *sreg &= ~(1  << 3); // Clear V
    
    if (val & 0x80) // Check N
    {
        *sreg |= 0x14; // Set N, S
    }
    else
    {
        *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _cls(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) &= ~(0x10);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _clt(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) &= ~(0x40);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _clv(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) &= ~(0x08);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _clz(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) &= ~(0x02);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _com(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    microcontroller->registers->r[d] = ~(microcontroller->registers->r[d]);
    
    uint8_t val = microcontroller->registers->r[d];
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    *sreg |= 0x01; // Set C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    if (val & 0x80) // Check N
    {
        *sreg |= 0x04; // Set N
        *sreg |= 0x10; // Set S
    }
    else
    {
        *sreg &= ~(0x04); // Clear N
        *sreg |= 0x10; // Set S
    }
        
    *sreg &= ~(0x08); // Clear V
        
    
    (*(microcontroller->registers->PC))++;
    microcontroller->ticks += 1;
}

void _cp(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t regr = microcontroller->registers->r[r];
    uint8_t val = regd - regr;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if ( (((~regr)+1) & 0x08) & (regd & 0x08) ) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H
    
    if ((bit(regd, 7, 0) & bit(regr, 7, 1) & bit(val, 7, 1)) | (bit(regd, 7, 1) & bit(regr, 7, 0) & bit(val, 7, 0))) // Check V
    {
        *sreg |= 0x08; // Set V
        if (val & 0x80) // Check N
        {
            *sreg |= 0x04; // Set N
            *sreg &= ~(0x10); // Clear S
        }
        else
        {
            *sreg &= ~(0x04); // Clear N
            *sreg |= 0x10; // Set S
        }
    }
    else
    {
        *sreg &= ~(0x08); // Clear V
        if (val & 0x80) // Check N
            *sreg |= 0x14; // Set N, S
        else
            *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if ( (bit(regd, 7, 1) & bit(regr, 7, 0)) | (bit(regr, 7, 0) & bit(val, 7, 0)) | (bit(val, 7, 0) & bit(regd, 7, 1)) ) // Check C
    {
        *sreg |= 0x01;
    }
    else
    {
        *sreg &= ~(0x01);
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _cpc(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t regr = microcontroller->registers->r[r];
    uint8_t regr2  = regr^ 0xFFFF;
    regr2 += 1;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    uint16_t c = *sreg & 0x01;
    c ^= 0xFFFF;
    c += 1;
    
    uint8_t val = regd + regr2 + c;
    
    //printf("cpc; c=%X, regd=%X, regr=%X, val=%X\n", c, regd, regr, val);
    
    if ( (((~regr)+1) & 0x08) & (regd & 0x08) ) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H
    
    if ((bit(regd, 7, 0) & bit(regr, 7, 1) & bit(val, 7, 1)) | (bit(regd, 7, 1) & bit(regr, 7, 0) & bit(val, 7, 0))) // Check V
    {
        *sreg |= 0x08; // Set V
        if (val & 0x80) // Check N
        {
            *sreg |= 0x04; // Set N
            *sreg &= ~(0x10); // Clear S
        }
        else
        {
            *sreg &= ~(0x04); // Clear N
            *sreg |= 0x10; // Set S
        }
    }
    else
    {
        *sreg &= ~(0x08); // Clear V
        if (val & 0x80) // Check N
            *sreg |= 0x14; // Set N, S
        else
            *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        //*sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if ( (bit(regd, 7, 1) & bit(regr, 7, 0)) | (bit(regr, 7, 0) & bit(val, 7, 0)) | (bit(val, 7, 0) & bit(regd, 7, 1)) ) // Check C
    {
        *sreg |= 0x01;
    }
    else
    {
        *sreg &= ~(0x01);
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _cpi(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t K2 = K ^ 0xFFFF;
    K2 += 1;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    uint8_t regval = microcontroller->registers->r[d];
    uint8_t val = regval + K2;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    
    if ( (((~K)+1) & 0x08) & (regval & 0x08) ) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H
    
    if ((bit(regval, 7, 0) & bit(K, 7, 1) & bit(val, 7, 1)) | (bit(regval, 7, 1) & bit(K, 7, 0) & bit(val, 7, 0))) // Check V
    {
        *sreg |= 0x08; // Set V
        if (val & 0x80) // Check N
        {
            *sreg |= 0x04; // Set N
            *sreg &= ~(0x10); // Clear S
        }
        else
        {
            *sreg &= ~(0x04); // Clear N
            *sreg |= 0x10; // Set S
        }
    }
    else
    {
        *sreg &= ~(0x08); // Clear V
        if (val & 0x80) // Check N
            *sreg |= 0x14; // Set N, S
        else
            *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if ( (bit(regval, 7, 1) & bit(K, 7, 0)) | (bit(K, 7, 0) & bit(val, 7, 0)) | (bit(val, 7, 0) & bit(regval, 7, 1)) ) // Check C
    {
        *sreg |= 0x01;
    }
    else
    {
        *sreg &= ~(0x01);
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _cpse(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    uint8_t r = ((ins1 & 0x0200) >> 5) | (ins1 & 0x000F);
    
    if (microcontroller->registers->r[r] == microcontroller->registers->r[d])
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
        if (opcodes[swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)])] == 0)
        {
            (*(microcontroller->registers->PC))++;
            microcontroller->ticks += 1;
        }
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _dec(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (microcontroller->registers->r[d] == 0x80)
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V

    microcontroller->registers->r[d]--;
    
    if (microcontroller->registers->r[d] & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if (microcontroller->registers->r[d] == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
        
    (*(microcontroller->registers->PC))++;
    microcontroller->ticks += 1;
}

void _in(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = ins1 & 0x000F;
    A |= (ins1 & 0x0600) >> 5;
    A += 0x20;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    microcontroller->registers->r[d] = microcontroller->sram->data[A];  
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _inc(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (microcontroller->registers->r[d] == 0x7F)
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V

    microcontroller->registers->r[d]--;
    
    if (microcontroller->registers->r[d] & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if (microcontroller->registers->r[d] == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
        
    (*(microcontroller->registers->PC))++;
    microcontroller->ticks += 1;
}

void _ldx1(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[26];
    address << 8;
    address |= microcontroller->registers->r[27];
    
    microcontroller->registers->r[d] = microcontroller->sram->data[address];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _ldx2(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _ldx3(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _ldy1(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[28];
    address << 8;
    address |= microcontroller->registers->r[29];
    
    microcontroller->registers->r[d] = microcontroller->sram->data[address];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _ldy2(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _ldy3(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _ldy4(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t q = (ins1 & 0x0007) | ((ins1 & 0x0C00) >> 7) | ((ins1 & 0x2000) >> 8);
    
    uint16_t address = microcontroller->registers->r[29] << 8;
    address |= microcontroller->registers->r[28];
    address += q;
    
    microcontroller->registers->r[d] = microcontroller->sram->data[address];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _ldz1(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[30];
    address << 8;
    address |= microcontroller->registers->r[31];
    
    microcontroller->registers->r[d] = microcontroller->sram->data[address];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _ldz2(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _ldz3(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _ldz4(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t q = (ins1 & 0x0007) | ((ins1 & 0x0C00) >> 7) | ((ins1 & 0x2000) >> 8);
    
    uint16_t address = microcontroller->registers->r[30];
    address << 8;
    address |= microcontroller->registers->r[31];
    address += q;
    
    microcontroller->registers->r[d] = microcontroller->sram->data[address];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _ldi(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    microcontroller->registers->r[d] = K;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _lpm1(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _lpm2(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _lpm3(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _lsr(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t val = regd >> 1;
    microcontroller->registers->r[d] = val;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (regd & 0x01) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    *sreg &= ~(0x04); // Clear N
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x01)) ) // Check V
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
    if (regd & 0x08) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H    
    

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _mov(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    microcontroller->registers->r[d] = microcontroller->registers->r[r];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _neg(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t regval = microcontroller->registers->r[d];
    uint8_t val = (~regval) + 1;
    microcontroller->registers->r[d] = val;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if ( (((~regval)+1) & 0x08) & (val & 0x08) ) // Check H // UNSURE
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H
    
    if (val == 0x80)
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V

    if (val & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S
    
    if (val == 0x00) // Check C
        *sreg &= ~(0x01); // Clear C
    else
        *sreg |= 0x01; // Set C
    
        
    (*(microcontroller->registers->PC))++;
    microcontroller->ticks += 1;
}

void _nop(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _or(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    microcontroller->registers->r[d] |= microcontroller->registers->r[r];
    
    uint8_t val = microcontroller->registers->r[d];
    uint8_t* sreg = microcontroller->registers->SREG;
    
    *sreg &= ~(1  << 3); // Clear V
    
    if (val & 0x80) // Check N
    {
        *sreg |= 0x14; // Set N, S
    }
    else
    {
        *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sbr(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    microcontroller->registers->r[d] |= K;
    
    uint8_t val = microcontroller->registers->r[d];
    uint8_t* sreg = microcontroller->registers->SREG;
    
    *sreg &= ~(1  << 3); // Clear V
    
    if (val & 0x80) // Check N
    {
        *sreg |= 0x14; // Set N, S
    }
    else
    {
        *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _out(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = ins1 & 0x000F;
    A |= (ins1 & 0x0600) >> 5;
    A += 0x20;
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    microcontroller->sram->data[A] = microcontroller->registers->r[r];  
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _rcall(microcontroller_t* microcontroller)
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint32_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    ins1 = ((ins1 << 3) & 0x0003) | (ins1 & 0x01F1);
    
    int16_t k = ins1 & 0x0FFF;
    
    if (k & 0x0800) // k is negative
    {
        k ^= 0x0FFF;
        k += 1;
        k *= -1;
    }
    
    uint16_t SP = get_sp(microcontroller->registers);
    
    microcontroller->sram->data[SP] = (*(microcontroller->registers->PC) & 0xFF00) >> 8;
    SP--;
    
    microcontroller->sram->data[SP] = *(microcontroller->registers->PC);
    SP--;
    
    set_sp(microcontroller->registers, SP);
    
    *(microcontroller->registers->PC) += (k + 1);
    
    microcontroller->ticks += 3;
}

void _ret(microcontroller_t* microcontroller)
{
    uint16_t pc = 0;
    
    uint16_t SP = get_sp(microcontroller->registers);
    
    SP++;
    pc = microcontroller->sram->data[SP+1];
    pc = pc << 8;
    pc |= microcontroller->sram->data[SP];
    SP++; 
    
    set_sp(microcontroller->registers, SP);
    
    *(microcontroller->registers->PC) = pc;
    
    microcontroller->ticks += 4;
    
}

void _reti(microcontroller_t* microcontroller)
{
    uint16_t pc = 0;
    
    uint16_t SP = get_sp(microcontroller->registers);
    
    SP++;
    pc = microcontroller->sram->data[SP+1];
    pc = pc << 8;
    pc |= microcontroller->sram->data[SP];
    SP++;    
    
    
    
    set_sp(microcontroller->registers, SP);
    
    *(microcontroller->registers->SREG) |= 0x80;
    
    *(microcontroller->registers->PC) = pc;
    
    microcontroller->ticks += 4;
}

void _rjmp(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = ins1 & 0x0FFF;
    
    if (k & 0x0800) // k is negative
    {
        k ^= 0x0FFF;
        k += 1;
        k *= -1;
    }
    
    *(microcontroller->registers->PC) += (1 + k);
    
    microcontroller->ticks += 2;
}

void _ror(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t val = regd >> 1;
    uint8_t* sreg = microcontroller->registers->SREG;
    
    val |= ((*sreg & 0x01) << 7); // Roll Carry
    microcontroller->registers->r[d] = val;
    
    if (regd & 0x01) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    if (val & 0x80) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x01)) ) // Check V
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S    
    

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sbc(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t r = ((ins1 & 0x0200) >> 5) | (ins1 & 0x000F);
    
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t regr = microcontroller->registers->r[r];
    uint8_t* sreg = microcontroller->registers->SREG;
    uint8_t val = regd - regr - (*sreg | 0x01);
    microcontroller->registers->r[d] = val;
    
    
    if ( (((~regr)+1) & 0x08) & (regd & 0x08) ) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H
    
    if ((bit(regd, 7, 0) & bit(regr, 7, 1) & bit(val, 7, 1)) | (bit(regd, 7, 1) & bit(regr, 7, 0) & bit(val, 7, 0))) // Check V
    {
        *sreg |= 0x08; // Set V
        if (val & 0x80) // Check N
        {
            *sreg |= 0x04; // Set N
            *sreg &= ~(0x10); // Clear S
        }
        else
        {
            *sreg &= ~(0x04); // Clear N
            *sreg |= 0x10; // Set S
        }
    }
    else
    {
        *sreg &= ~(0x08); // Clear V
        if (val & 0x80) // Check N
            *sreg |= 0x14; // Set N, S
        else
            *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        //*sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if ( (bit(regd, 7, 1) & bit(regr, 7, 0)) | (bit(regr, 7, 0) & bit(val, 7, 0)) | (bit(val, 7, 0) & bit(regd, 7, 1)) ) // Check C
    {
        *sreg |= 0x01;
    }
    else
    {
        *sreg &= ~(0x01);
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sbci(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    uint16_t K = ((ins1 & 0x0F00) >> 4) | (ins1 & 0x000F);
    
    uint8_t regval = microcontroller->registers->r[d];
    uint8_t* sreg = microcontroller->registers->SREG;
    uint8_t val = regval - K - (*sreg & 0x01);
    microcontroller->registers->r[d] = val;
    
    if ( (((~K)+1) & 0x08) & (regval & 0x08) ) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H
    
    if ((bit(regval, 7, 0) & bit(K, 7, 1) & bit(val, 7, 1)) | (bit(regval, 7, 1) & bit(K, 7, 0) & bit(val, 7, 0))) // Check V
    {
        *sreg |= 0x08; // Set V
        if (val & 0x80) // Check N
        {
            *sreg |= 0x04; // Set N
            *sreg &= ~(0x10); // Clear S
        }
        else
        {
            *sreg &= ~(0x04); // Clear N
            *sreg |= 0x10; // Set S
        }
    }
    else
    {
        *sreg &= ~(0x08); // Clear V
        if (val & 0x80) // Check N
            *sreg |= 0x14; // Set N, S
        else
            *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if ( (bit(regval, 7, 1) & bit(K, 7, 0)) | (bit(K, 7, 0) & bit(val, 7, 0)) | (bit(val, 7, 0) & bit(regval, 7, 1)) ) // Check C
    {
        *sreg |= 0x01;
    }
    else
    {
        *sreg &= ~(0x01);
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sbi(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = (ins1 & 0x00F8) >> 3;
    A += 0x20;
    uint8_t b = ins1 & 0x0007;
    
    microcontroller->sram->data[A] |= (1 << b);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _sbic(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = (ins1 & 0x00F8) >> 3;
    A += 0x20;
    uint8_t b = ins1 & 0x0007;
    
    (*(microcontroller->registers->PC))++;
    microcontroller->ticks += 1;
    
    if (microcontroller->sram->data[A] & (1 << b) == 0)
    {
        uint16_t ins2 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
        
        if (ins2 & 0xFE0E == 0x940E) // CALL
        {
            (*(microcontroller->registers->PC)) += 2;
            microcontroller->ticks += 2;
        }
        else if (ins2 & 0xFE0E == 0x940C) // JMP
        {
            (*(microcontroller->registers->PC)) += 2;
            microcontroller->ticks += 2;
        }
        else if (ins2 & 0xFE0E == 0x9000) // LDS32
        {
            (*(microcontroller->registers->PC)) += 2;
            microcontroller->ticks += 2;
        }
        else if (ins2 & 0xFE0E == 0x9200) // STS32
        {
            (*(microcontroller->registers->PC)) += 2;
            microcontroller->ticks += 2;
        }
        else
        {
            (*(microcontroller->registers->PC))++;
            microcontroller->ticks += 1;
        }
    }
}

void _sbis(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = (ins1 & 0x00F8) >> 3;
    A += 0x20;
    uint8_t b = ins1 & 0x0007;
    
    (*(microcontroller->registers->PC))++;
    microcontroller->ticks += 1;
    
    if (microcontroller->sram->data[A] & (1 << b))
    {
        uint16_t ins2 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
        
        if (ins2 & 0xFE0E == 0x940E) // CALL
        {
            (*(microcontroller->registers->PC)) += 2;
            microcontroller->ticks += 2;
        }
        else if (ins2 & 0xFE0E == 0x940C) // JMP
        {
            (*(microcontroller->registers->PC)) += 2;
            microcontroller->ticks += 2;
        }
        else if (ins2 & 0xFE0E == 0x9000) // LDS32
        {
            (*(microcontroller->registers->PC)) += 2;
            microcontroller->ticks += 2;
        }
        else if (ins2 & 0xFE0E == 0x9200) // STS32
        {
            (*(microcontroller->registers->PC)) += 2;
            microcontroller->ticks += 2;
        }
        else
        {
            (*(microcontroller->registers->PC))++;
            microcontroller->ticks += 1;
        }
    }
}

void _sbrc(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    uint16_t b = ins1 & 0x0007;
    
    if ((microcontroller->registers->r[r] & b) == 0)
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
        if (opcodes[swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)])] == 0)
        {
            (*(microcontroller->registers->PC))++;
            microcontroller->ticks += 1;
        }
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _sbrs(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    uint16_t b = ins1 & 0x0007;
    
    if ((microcontroller->registers->r[r] & b))
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
        if (opcodes[swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)])] == 0)
        {
            (*(microcontroller->registers->PC))++;
            microcontroller->ticks += 1;
        }
    }
    else
    {
        (*(microcontroller->registers->PC))++;
        microcontroller->ticks += 1;
    }
}

void _sec(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) |= 0x01;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _seh(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) |= 0x20;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sei(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) |= 0x80;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sen(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) |= 0x04;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _ser(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x00F0) >> 4;
    r |= 0x0010;
    
    microcontroller->registers->r[r] = 0xFF;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _ses(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) |= 0x10;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _set(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) |= 0x40;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sev(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) |= 0x08;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sez(microcontroller_t* microcontroller)
{
    *(microcontroller->registers->SREG) |= 0x02;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _sleep(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _stx1(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[27] << 8;
    address |= microcontroller->registers->r[26];
    
    microcontroller->sram->data[address] = microcontroller->registers->r[r];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _stx2(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[27] << 8;
    address |= microcontroller->registers->r[26];
    
    microcontroller->sram->data[address] = microcontroller->registers->r[r];

    address += 1;
    microcontroller->registers->r[26] = address;
    microcontroller->registers->r[27] = address >> 8;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _stx3(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[27] << 8;
    address |= microcontroller->registers->r[26];
    address -= 1;

    microcontroller->sram->data[address] = microcontroller->registers->r[r];    

    microcontroller->registers->r[26] = address;
    microcontroller->registers->r[27] = address >> 8;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _sty1(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[29] << 8;
    address |= microcontroller->registers->r[28];
    
    microcontroller->sram->data[address] = microcontroller->registers->r[r];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _sty2(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[29] << 8;
    address |= microcontroller->registers->r[28];
    
    microcontroller->sram->data[address] = microcontroller->registers->r[r];

    address += 1;
    microcontroller->registers->r[28] = address;
    microcontroller->registers->r[29] = address >> 8;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _sty3(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[29] << 8;
    address |= microcontroller->registers->r[28];
    address -= 1;

    microcontroller->sram->data[address] = microcontroller->registers->r[r]; 

    microcontroller->registers->r[28] = address;
    microcontroller->registers->r[29] = address >> 8;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _sty4(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    uint16_t q = (ins1 & 0x0007) | ((ins1 & 0x0C00) >> 7) | ((ins1 & 0x2000) >> 8);
    
    uint16_t address = microcontroller->registers->r[29] << 8;
    address |= microcontroller->registers->r[28];
    address += q;
    
    microcontroller->sram->data[address] = microcontroller->registers->r[r];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _stz1(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[31] << 8;
    address |= microcontroller->registers->r[30];
    
    microcontroller->sram->data[address] = microcontroller->registers->r[r];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _stz2(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[31] << 8;
    address |= microcontroller->registers->r[30];
    
    microcontroller->sram->data[address] = microcontroller->registers->r[r];

    address += 1;
    microcontroller->registers->r[30] = address;
    microcontroller->registers->r[31] = address >> 8;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _stz3(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t address = microcontroller->registers->r[31] << 8;
    address |= microcontroller->registers->r[30];
    address -= 1;

    microcontroller->sram->data[address] = microcontroller->registers->r[r];    

    microcontroller->registers->r[30] = address;
    microcontroller->registers->r[31] = address >> 8;
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _stz4(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t q = (ins1 & 0x0007) | ((ins1 & 0x0C00) >> 7) | ((ins1 & 0x2000) >> 8);
    
    uint16_t address = microcontroller->registers->r[31] << 8;
    address |= microcontroller->registers->r[30];
    address += q;
    
    microcontroller->sram->data[address] = microcontroller->registers->r[d];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _sub(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t r = ((ins1 & 0x0200) >> 5) | (ins1 & 0x000F);
    
    
    uint8_t regd = microcontroller->registers->r[d];
    uint8_t regr = microcontroller->registers->r[r];
    uint8_t* sreg = microcontroller->registers->SREG;
    uint8_t val = regd - regr;
    microcontroller->registers->r[d] = val;
    
    
    if ( (((~regr)+1) & 0x08) & (regd & 0x08) ) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H
    
    if ((bit(regd, 7, 0) & bit(regr, 7, 1) & bit(val, 7, 1)) | (bit(regd, 7, 1) & bit(regr, 7, 0) & bit(val, 7, 0))) // Check V
    {
        *sreg |= 0x08; // Set V
        if (val & 0x80) // Check N
        {
            *sreg |= 0x04; // Set N
            *sreg &= ~(0x10); // Clear S
        }
        else
        {
            *sreg &= ~(0x04); // Clear N
            *sreg |= 0x10; // Set S
        }
    }
    else
    {
        *sreg &= ~(0x08); // Clear V
        if (val & 0x80) // Check N
            *sreg |= 0x14; // Set N, S
        else
            *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if ( (bit(regd, 7, 1) & bit(regr, 7, 0)) | (bit(regr, 7, 0) & bit(val, 7, 0)) | (bit(val, 7, 0) & bit(regd, 7, 1)) ) // Check C
    {
        *sreg |= 0x01;
    }
    else
    {
        *sreg &= ~(0x01);
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _subi(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    
    uint8_t regval = microcontroller->registers->r[d];
    uint8_t* sreg = microcontroller->registers->SREG;
    uint8_t val = regval - K;
    microcontroller->registers->r[d] = val;
    
    if ( (((~K)+1) & 0x08) & (regval & 0x08) ) // Check H
        *sreg |= 0x20; // Set H
    else
        *sreg &= ~(0x20); // Clear H
    
    if ((bit(regval, 7, 0) & bit(K, 7, 1) & bit(val, 7, 1)) | (bit(regval, 7, 1) & bit(K, 7, 0) & bit(val, 7, 0))) // Check V
    {
        *sreg |= 0x08; // Set V
        if (val & 0x80) // Check N
        {
            *sreg |= 0x04; // Set N
            *sreg &= ~(0x10); // Clear S
        }
        else
        {
            *sreg &= ~(0x04); // Clear N
            *sreg |= 0x10; // Set S
        }
    }
    else
    {
        *sreg &= ~(0x08); // Clear V
        if (val & 0x80) // Check N
            *sreg |= 0x14; // Set N, S
        else
            *sreg &= ~(0x14); // Clear N, S
    }
    
    if (val == 0) // Check Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if ( (bit(regval, 7, 1) & bit(K, 7, 0)) | (bit(K, 7, 0) & bit(val, 7, 0)) | (bit(val, 7, 0) & bit(regval, 7, 1)) ) // Check C
    {
        *sreg |= 0x01;
    }
    else
    {
        *sreg &= ~(0x01);
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _swap(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint16_t regd = microcontroller->registers->r[d];
    
    uint16_t val = (regd << 4) | (regd >> 4);
    
    microcontroller->registers->r[d] = val;
    
    
    (*(microcontroller->registers->PC))++;
}

void _wdr(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _adiw(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x0030) >> 4;
    d = (d * 2) + 24;
    
    uint16_t K = ((ins1 & 0x00C0) >> 2) | (ins1 & 0x000F);
    
    uint8_t regdl = microcontroller->registers->r[d];
    uint8_t regdh = microcontroller->registers->r[d+1];
    
    
    uint16_t val = regdh;
    val = val << 8;
    val |= regdl;
    val += K;
    
    microcontroller->registers->r[d] = val;
    microcontroller->registers->r[d+1] = val >> 8;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (~(val | 0x7FFF) & (regdh & 0x80)) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    if (val & 0x8000) // Check N
        *sreg |= 0x04; // Set N
    else
        *sreg &= ~(0x04); // Clear N
        
    if ((val & 0x8000) & ~(regdh | 0x7F)) // Check V
        *sreg |= 0x08; // Set V
    else
        *sreg &= ~(0x08); // Clear V
        
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
        *sreg |= 0x10; // Set S
    else
        *sreg &= ~(0x10); // Clear S 
    

    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _icall(microcontroller_t* microcontroller)
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint16_t address = microcontroller->registers->r[30];
    address << 8;
    address |= microcontroller->registers->r[31];
    
    uint16_t SP = get_sp(microcontroller->registers);
    uint16_t PC = *(microcontroller->registers->PC);
    PC += 2;
    
    microcontroller->sram->data[SP] = (PC & 0xFF00) >> 8;
    SP--;
    
    microcontroller->sram->data[SP] = PC;
    SP--;
    
    set_sp(microcontroller->registers, SP);
    
    *(microcontroller->registers->PC) = address;
    
    microcontroller->ticks += 3;
}

void _ijmp(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint16_t address = microcontroller->registers->r[30];
    address << 8;
    address |= microcontroller->registers->r[31];
    
    microcontroller->ticks += 2;
    *(microcontroller->registers->PC) = address;
}

void _lds32(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint16_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t k = ins2;
    
    microcontroller->registers->r[d] = microcontroller->sram->data[k];
    
    (*(microcontroller->registers->PC)) += 2;
    
    microcontroller->ticks += 2;
}

void _lds16(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = ((ins1 & 0x00F0) >> 4) + 16;
    uint16_t k = ((ins1 & 0x0700) >> 4) | (ins1 & 0x000F);
    
    microcontroller->registers->r[d] = microcontroller->sram->data[k];
    
    (*(microcontroller->registers->PC)) += 1;
    
    microcontroller->ticks += 1;
}

void _pop(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    uint16_t SP = get_sp(microcontroller->registers);
    
    SP++;
    microcontroller->registers->r[d] = microcontroller->sram->data[SP];
    
    set_sp(microcontroller->registers, SP);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _push(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    uint16_t SP = get_sp(microcontroller->registers);
    
    printf("SP: %X\n", SP);
    
    microcontroller->sram->data[SP] = microcontroller->registers->r[r];
    SP--;
    
    set_sp(microcontroller->registers, SP);
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _sbiw(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x0030) >> 4;
    d = (d * 2) + 24;
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x00C0) >> 2;
    
    uint8_t regval = microcontroller->registers->r[d];
    uint16_t val = microcontroller->registers->r[d+1] << 8;
    val |= microcontroller->registers->r[d];
    val -= K;
    
    microcontroller->registers->r[d] = val;
    microcontroller->registers->r[d+1] = val >> 8;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if ((val & (0x8000)) && (regval & (0x80))) // Check C, V
    {
        *sreg |= 0x01; // Set C
        *sreg |= 0x08; // Set V
    }
    else
    {
        *sreg &= ~(0x01); // Clear C
        *sreg &= ~(0x08); // Clear V
    }
    
    if (val == 0) // Set Z
    {
        *sreg |= 0x02; // Set Z
    }
    else
    {
        *sreg &= ~(0x02); // Clear Z
    }
    
    if (val & (0x8000)) // Check N
    {
        *sreg |= 0x04; // Set N
    }
    else
    {
        *sreg &= ~(0x04); // Clear N
    }
    
    if ( ((*sreg & 0x04) >> 2) ^ ((*sreg & 0x08) >> 3) ) // Check S
    {
        *sreg |= 0x10; // Set S
    }
    else
    {
        *sreg &= ~(0x10); // Clear S
    }
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 2;
}

void _sts32(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint16_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t k = ins2;
    
    microcontroller->sram->data[k] = microcontroller->registers->r[d];
    
    (*(microcontroller->registers->PC)) += 2;
    
    microcontroller->ticks += 2;
}

void _sts16(microcontroller_t* microcontroller)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = ((ins1 & 0x00F0) >> 4) + 16;
    uint16_t k = ((ins1 & 0x0700) >> 4) | (ins1 & 0x000F);
    
    microcontroller->sram->data[k] = microcontroller->registers->r[d];
    
    (*(microcontroller->registers->PC)) += 1;
    
    microcontroller->ticks += 1;
}

void _movw(microcontroller_t* microcontroller)
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t r = ins1 & 0x000F;
    r *= 2;
    uint8_t d = (ins1 & 0x00F0) >> 4;
    d *= 2;
    
    microcontroller->registers->r[d] = microcontroller->registers->r[r];
    microcontroller->registers->r[d+1] = microcontroller->registers->r[r+1];
    
    (*(microcontroller->registers->PC))++;
    
    microcontroller->ticks += 1;
}

void _call(microcontroller_t* microcontroller)
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint32_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    ins1 = ((ins1 << 3) & 0x0003) | (ins1 & 0x01F1);
    
    uint32_t k = (ins1 << 13) | ins2;
    
    uint16_t SP = get_sp(microcontroller->registers);
    uint16_t PC = *(microcontroller->registers->PC);
    PC += 2;
    
    microcontroller->sram->data[SP] = (PC & 0xFF00) >> 8;
    SP--;
    
    microcontroller->sram->data[SP] = PC;
    SP--;
    
    set_sp(microcontroller->registers, SP);
    
    *(microcontroller->registers->PC) = k;
    
    microcontroller->ticks += 4;
}

void _jmp(microcontroller_t* microcontroller)
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint32_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    ins1 = ((ins1 << 3) & 0x0003) | (ins1 & 0x01F1);
    
    uint32_t k = (ins1 << 13) | ins2;
    
    *(microcontroller->registers->PC) = k;
    
    microcontroller->ticks += 3;
}

void _elpmz1(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _elpmz2(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _elpmz3(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _fmul(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _fmuls(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _fmulsu(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _mul(microcontroller_t* microcontroller)
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t r = ((ins1 & 0x0200) >> 5) | (ins1 & 0x000F);
    uint8_t d = (ins1 & 0x01F0) >> 4;
    
    uint16_t val = microcontroller->registers->r[d] * microcontroller->registers->r[r];
    microcontroller->registers->r[0] = val;
    microcontroller->registers->r[1] = val >> 8;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (val & 0x8000) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    
    (*(microcontroller->registers->PC))++;

    microcontroller->ticks += 1;
}

void _muls(microcontroller_t* microcontroller)
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t r = ins1 & 0x000F;
    r |= 0x0010;
    uint8_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    uint16_t val = microcontroller->registers->r[d] * microcontroller->registers->r[r];
    microcontroller->registers->r[0] = val;
    microcontroller->registers->r[1] = val >> 8;
    
    uint8_t* sreg = microcontroller->registers->SREG;
    
    if (val & 0x8000) // Check C
        *sreg |= 0x01; // Set C
    else
        *sreg &= ~(0x01); // Clear C
        
    if (val == 0) // Check Z
        *sreg |= 0x02; // Set Z
    else
        *sreg &= ~(0x02); // Clear Z
    
    
    (*(microcontroller->registers->PC))++;

    microcontroller->ticks += 1;
}

void _mulsu(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _spm(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}

void _break(microcontroller_t* microcontroller)
{
    context->state = STATE_INDEBUG;
    (*(microcontroller->registers->PC))++;
}

void _elpmx(microcontroller_t* microcontroller)
{
    (*(microcontroller->registers->PC))++;
}
