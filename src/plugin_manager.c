/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

plugin_manager.c
    
*/

#include "plugin_manager.h"

#include "plugin.h"

#include <stdio.h> 
#include <stdlib.h>
#include <dirent.h> 
#include <string.h>
#include <dlfcn.h>
#include <libgen.h>
#include <sys/stat.h>

plugin_manager_t* init_plugin_manager(microcontroller_t* microcontroller, context_t* context)
{
	plugin_manager_t* manager = (plugin_manager_t*) malloc(sizeof(plugin_manager_t));	
	
	manager->n_plugins = 0;
	
	char plugin_dir[256];
	sprintf(plugin_dir, "%s/.flame/plugins/", getenv("HOME"));
    
    
    struct dirent *de;   
    DIR *dr = opendir(plugin_dir); 
  
    if (dr == NULL) 
    { 
        printf("Could not open plugin directory\n"); 
        return manager; 
    } 

    while ((de = readdir(dr)) != NULL)
    {
        char prefix[8];
        char suffix[5];
        char name[128];
        
        strncpy(prefix, de->d_name, 7);
        prefix[7] = '\0';
        strncpy(suffix, de->d_name+(strlen(de->d_name)-3), 3);
        suffix[3] = '\0';
        
        
        if (strcmp(prefix, "plugin_") == 0 && strcmp(suffix, ".so") == 0)
        {
        	strncpy(name, de->d_name, strlen(de->d_name)-3);
    		name[strlen(de->d_name)-2] = '\0';
			        
        	char plugin_path[128];
        	char config_path[128];
        	
        	sprintf(plugin_path, "%s/%s", plugin_dir, de->d_name);
        	sprintf(config_path, "%s/%s.cfg", plugin_dir, name);
        	
        	plugin_container_t* container = load_plugin(manager, plugin_path, microcontroller, context); 
        	
			strcpy(container->config_path, config_path);

			if (access(config_path, F_OK) == -1)
			{
				generate_plugin_config(config_path, container);
			}
			
			else
			{
				load_plugin_config(config_path, container);
			}        	
        }
    }
  
    closedir(dr);
    
    return manager; 
}

plugin_container_t* load_plugin(plugin_manager_t* manager, const char* path, microcontroller_t* microcontroller, context_t* context)
{
	
	void* lib = dlopen(path, RTLD_NOW);
	
	if (lib == NULL)
	{
		printf("Null lib\n");
		return;	
	}
	
	plugin_container_t* container = (plugin_container_t*) malloc(sizeof(plugin_container_t));
	
	//void (*func_print_name)(const char*);
	plugin_t* (*func_init_plugin)(context_t* context, microcontroller_t* microcontroller);
	
	//*(void**)(&func_print_name) = dlsym(lib, "init_plugin");
	*(plugin_t**)(&func_init_plugin) = dlsym(lib, "init_plugin");
	
	container->plugin = func_init_plugin(context, microcontroller);
	
	container->is_registered = 0;
	container->dock_item_hidden = 1;
	container->parent_window = NULL;
	container->button = NULL;
	
	manager->plugins[manager->n_plugins] = container;
	manager->n_plugins++;
	
	return container;
}

void generate_plugin_config(const char* path, plugin_container_t* plugin)
{
	GKeyFile* file = g_key_file_new();
    GError* error = NULL;
    
    g_key_file_set_integer(file, "flame", "registered", 0);
    
    plugin->plugin->on_generate_config(plugin->plugin, file);
    
    if (!g_key_file_save_to_file(file, path, &error))
        printf("Error saving key file: %s\n", error->message);
}

void load_plugin_config(const char* path, plugin_container_t* plugin)
{
	GKeyFile* file = g_key_file_new();
    GError* error = NULL;
    
    if (!g_key_file_load_from_file(file, path, G_KEY_FILE_NONE, &error))
    {
        printf("Error loading key file: %s\n", error->message);
        return;
    }
	
	error = NULL;
    plugin->is_registered = g_key_file_get_integer(file, "flame", "registered", error);
    
    plugin->plugin->on_load_config(plugin->plugin, file);
}

void save_plugin_config(const char* path, plugin_container_t* plugin)
{
	GKeyFile* file = g_key_file_new();
    GError* error = NULL;
    
    if (!g_key_file_load_from_file(file, path, G_KEY_FILE_NONE, &error))
    {
        printf("Error loading key file: %s\n", error->message);
        error = NULL;
    }
    
    g_key_file_set_integer(file, "flame", "registered", plugin->is_registered);
    
    plugin->plugin->on_save_config(plugin->plugin, file);
    
    if (!g_key_file_save_to_file(file, path, &error))
        printf("Error saving key file: %s\n", error->message);
}

