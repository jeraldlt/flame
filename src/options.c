#include "options.h"

#include "globals.h"
#include "context.h"

#include <stdlib.h>
#include <stdio.h>

context_t* context;

options_t* init_options()
{
    options_t* options = (options_t*) malloc(sizeof(options_t));
    
    strcpy(options->last_program, "");
    
    options->gui_byte_addressing = 1;
    options->gui_theme = THEME_SYSTEM_DEFAULT;
    //g_object_get(gtk_settings_get_default(), "gtk_theme_name", &options->gui_default_theme, NULL);
    
    strcpy(options->gui_last_layout, "Debug.xml");
    
    return options;
}

void generate_config(char* path)
{
    GKeyFile* file = g_key_file_new();
    GError* error = NULL;
    
    set_options(file);
    
    if (!g_key_file_save_to_file(file, path, &error))
        printf("Error saving key file: %s\n", error->message);
}

void save_config(char* path)
{
    GKeyFile* file = g_key_file_new();
    GError* error = NULL;
    
    if (!g_key_file_load_from_file(file, path, G_KEY_FILE_NONE, &error))
    {
        printf("Error loading key file: %s\n", error->message);
        error = NULL;
    }

    set_options(file);
    
    if (!g_key_file_save_to_file(file, path, &error))
        printf("Error saving key file: %s\n", error->message);
}

void load_config(char* path)
{
    GKeyFile* file = g_key_file_new();
    GError* error = NULL;
    
    if (!g_key_file_load_from_file(file, path, G_KEY_FILE_NONE, &error))
    {
        printf("Error loading key file: %s\n", error->message);
        return;
    }
    
    
    // group GUI
    error = NULL;
    context->options->gui_byte_addressing = g_key_file_get_integer(file, "gui", "gui_byte_addressing", error);
    
    error = NULL;
    strcpy(context->options->last_program, g_key_file_get_string(file, "history", "last_program", error));
    
    error = NULL;
    strcpy(context->options->gui_last_layout, g_key_file_get_string(file, "gui", "last_layout", error));
}

void set_options(GKeyFile* file)
{
    // group history
    g_key_file_set_string(file, "history", "last_program", context->options->last_program);
    
    // group GUI
    g_key_file_set_integer(file, "gui", "gui_byte_addressing", context->options->gui_byte_addressing);
    g_key_file_set_integer(file, "gui", "theme", context->options->gui_theme);
    g_key_file_set_string(file, "gui", "last_layout", context->options->gui_last_layout);
}
