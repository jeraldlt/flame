/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

interrupts.c

*/

#include "interrupts.h"

#include <stdlib.h>

interrupt_t* init_interrupt()
{
    interrupt_t* interrupt = (interrupt_t*) malloc(sizeof(interrupt_t));
    
    return interrupt;
}

interrupt_t* init_pin_change_interrupt(uint8_t* pcmsk, uint8_t* pin, uint8_t flag_mask, uint16_t vector)
{
    interrupt_t* interrupt = (interrupt_t*) malloc(sizeof(interrupt_t));
    
    interrupt->pcmsk = pcmsk;
    interrupt->pin = pin;
    interrupt->flag_mask = flag_mask;
    interrupt->pin_change_vector = vector;
    
    interrupt->handler = &pin_change_interrupt_handler;
    
    return interrupt;
}

interrupt_t* init_timer_interrupt(timer_counter_t* timer, uint16_t compare_a, uint16_t compare_b, uint16_t overflow, uint16_t capture)
{
    interrupt_t* interrupt = (interrupt_t*) malloc(sizeof(interrupt_t));
    
    interrupt->timer = timer;
    interrupt->compare_a_vector = compare_a;
    interrupt->compare_b_vector = compare_b;
    interrupt->overflow_vector = overflow;
    interrupt->capture_vector = capture;
    
    interrupt->handler = &timer_interrupt_handler;
    
    return interrupt;
}

void pin_change_interrupt_handler(interrupt_t* interrupt, microcontroller_t* microcontroller)
{
    if (*(microcontroller->registers->PCICR) & interrupt->flag_mask) // PCIE0 set
    {
        if ( (*(interrupt->pcmsk) & *(interrupt->pin)) ^ interrupt->pin_last )
        {
            *(microcontroller->registers->PCIFR) |= interrupt->flag_mask;
            call_interrupt(microcontroller, interrupt->pin_change_vector); // Call PCINT0_vect 0x0006
        }
        interrupt->pin_last = *(interrupt->pin);
    }
}

void timer_interrupt_handler(interrupt_t* interrupt, microcontroller_t* microcontroller)
{
    if (*(microcontroller->registers->SREG) & 0x80)
    {
        if (*(interrupt->timer->timskx) & 0x07) // TCX enabled
        {
            if (*(interrupt->timer->timskx) & 0x01) // Overflow interrupt enabled
            {
                if (*(interrupt->timer->tifrx) & 0x01) // overflow happened
                {
                    *(interrupt->timer->tifrx) &= ~(0x01);
                    //printf("overflow, %X\n", interrupt->overflow_vector);
                    call_interrupt(microcontroller, interrupt->overflow_vector);
                }
            }
            
            if (*(interrupt->timer->timskx) & 0x02) // Compare A match interrupt enabled
            {
                if (*(interrupt->timer->tifrx) & 0x02)
                {
                    *(interrupt->timer->tifrx) &= ~(0x02);
                    //printf("compare a, %X\n", interrupt->compare_a_vector);                    
                    call_interrupt(microcontroller, interrupt->compare_a_vector);

                }
            }
            
            if (*(interrupt->timer->timskx) & 0x04) // Compare B match interrupt enabled
            {
                if (*(interrupt->timer->tifrx) & 0x04)
                {
                    *(interrupt->timer->tifrx) &= ~(0x04);
                    //printf("compare b, %X %X\n", *(interrupt->timer->timskx), *(microcontroller->registers->TIMSK1));
                    call_interrupt(microcontroller, interrupt->compare_b_vector);
                }
            }
        }
    }
}
