/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

main.c
    
*/


#define _POSIX_C_SOURCE 199309L
#define _DEFAULT_SOURCE
#define BILLION 1000000000L


#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <ncurses.h>
#include <form.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/stat.h>

#include "globals.h"
#include "util.h"
#include "context.h"
#include "memory.h"
#include "microcontroller.h"
#include "debug.h"
#include "options.h"

#include "gui/window_new_microcontroller.h"

// Extern globals
context_t* context;

pthread_t runThread;



void cleanup()
{
    free_context(context);
}

void  SIGhandler(int sig)
{
    cleanup();

    switch(sig)
    {
        case SIGINT:
            printf("CTRL-C, cleaning up...\n");
            break;
        case SIGSEGV:
            printf("Segfault, cleaning up...\n");
            break;
        case SIGKILL:
            printf("Kill signal, cleaning up...\n");
            break;
        default:
            printf("Signal %i detected, cleaning up...\n", sig);
            break;
    }

    exit(0);
}

// The main run function
// gets its own thread

void* run(context_t* context)
{
    signal(SIGINT, SIGhandler);
    signal(SIGSEGV, SIGhandler);  
    signal(SIGKILL, SIGhandler); 
    
    
    
    while (1)
    {
        if (context->state == STATE_RUNNING)
        {
            step(context);
        }
        
        else if (context->state == STATE_STEPPING)
        {
            step(context);
            context->state = STATE_STOPPED;
        }
        
        else if (context->state == STATE_STEPPING_DEBUG)
        {
            step(context);
            context->state = STATE_INDEBUG;
        }
        
        else
            usleep(250 * 1000);
        
        context->state_last = context->state;
    }
}


int main(int argc, char** argv)
{   
    signal(SIGINT, SIGhandler);
    signal(SIGSEGV, SIGhandler);  
    signal(SIGKILL, SIGhandler); 

    gtk_init (&argc, &argv);
    
    context = init_context();
    
    pthread_create(&runThread, 0, run, context);
    
    // create config path
    sprintf(context->config_path, "%s/.flame/config.cfg", getenv("HOME"));
    
    // check if config path exists
    // if not -> create it
    // else -> load config
    if (access(context->config_path, F_OK) == -1)
	{
	    struct stat st = {0};
        char config_dir[256];
        sprintf(config_dir, "%s/.flame/", getenv("HOME"));
        
        if (stat(config_dir, &st) == -1) 
        {
            mkdir(config_dir, 0700);
        }
        
        generate_config(context->config_path);
	}
	
	else
	{
	    load_config(context->config_path);
	}
    
    
    // get command line options
    int opt;
    char* mcu = NULL;
    char* clock = NULL;
    char* file = NULL;
    
    while ((opt = getopt(argc, argv, "m:c:f:")) != -1)
    {
        switch(opt)
        {
            case 'm':
                mcu = malloc(strlen(optarg) + 1);
                strcpy(mcu, optarg);
                break;
            case 'c':
                clock = malloc(strlen(optarg) + 1);
                strcpy(clock, optarg);
                break;
            case 'f':
                file = malloc(strlen(optarg) + 1);
                strcpy(file, optarg);
                break;
            case 'h':
                printf("usage:\n"); // TODO: print usage
            default:
                printf("Unknown command line option\n"); // TODO: print usage
        }
    }
    
    // set default options if not set via command line
    if (mcu == NULL)
        mcu = "atmega328p";
    if (clock == NULL)
        clock = "16 MHz";
    if (file == NULL)
        file = "tests/adc.elf";
    
    // create the new microcontroller window    
	window_new_microcontroller_new_microcontroller(mcu, clock, file);
    
	gtk_main();
    cleanup();
    return 0;
}
