/*
This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

memory.c
    
*/


#include "memory.h"
#include "util.h"
#include "debug.h"
#include <stdio.h>
#include <stdlib.h>

flash_t* init_flash(const uint32_t size)
{
    flash_t* flash = (flash_t*) malloc(sizeof(flash_t));
    
    flash->size = size;
    flash->start = -1;
    flash->end = -1;
    flash->name = NULL;
    
    flash->data = calloc(size, sizeof(uint16_t));
    uint32_t i = 0;
    for(i; i<size; i++)
        flash->data[i] = 0;
    
    return flash;
}

void free_flash(flash_t* flash)
{
    free(flash->data);
    
    free(flash);
}

sram_t* init_sram(const uint32_t size)
{
    sram_t* sram = (sram_t*) malloc(sizeof(sram_t));
    
    sram->size = size;
    
    sram->data = calloc(size, sizeof(uint8_t));
    uint32_t i=0;
    for(i; i<size; i++)
        sram->data[i] = 0;
        
    return sram;
}

void free_sram(sram_t* sram)
{
    free(sram->data);
    
    free(sram);
}

eeprom_t* init_eeprom(const uint32_t size)
{
    eeprom_t* eeprom = (eeprom_t*) malloc(sizeof(eeprom_t));
    
    eeprom->size = size;
    
    eeprom->data = calloc(size, sizeof(uint8_t));
    uint32_t i=0;
    for(i; i<size; i++)
        eeprom->data[i] = 0;
        
    return eeprom;
}

void free_eeprom(eeprom_t* eeprom)
{
    free(eeprom->data);
    
    free(eeprom);
}
