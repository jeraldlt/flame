/*
This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

debug.c
    
*/


#include <stdlib.h>
#include <stdio.h>
#include <libgen.h>
#include <string.h>

#include "debug.h"


debugger_t* init_debugger(const uint32_t size)
{
    debugger_t* debugger = (debugger_t*) malloc(sizeof(debugger_t));
    
    debugger->breakpoints = calloc(size, sizeof(uint8_t));
    
    int i=0;
    for (i; i<size; i++)
    {
        debugger->breakpoints[i] = 0;
    }
    
    debugger->sources = 0;
    debugger->n_sources = 0;
    
    debugger->n_types = 0;
    
    debugger->cur_breakpoint = -1;
    
    debugger->n_hardware_breakpoints = 0;
    
    return debugger;
}

void free_debugger(debugger_t* debugger)
{
    free(debugger->breakpoints);
    
    /*
    int i = 0;
    for (i; i<debugger->n_sources; i++)
    {
        int j = 0;
        for (j; j<debugger->sources[i]->line_count; j++)
        {
            free(debugger->sources[i]->lines[j]);
        }
        free(debugger->sources[i]->lines);
        free(debugger->sources[i]->file_name);
        free(debugger->sources[i]->file_path);
        free(debugger->sources[i]);
    }
    */
    free(debugger);
}

void add_breakpoint(debugger_t* debugger, const uint32_t location)
{
    debugger->breakpoints[location] = 1;
}

/*
void add_breakpoint_source(debugger_t* debugger, const char* filename, const uint16_t line)
{
    if (!debugger->n_sources)
        return;
        
    int i=0;
    int j=0;
    int line_number = 0;
    int location = -1;
    for (i; i<debugger->sources->n_sources; i++)
    {
        
        char* source_file = basename(debugger->dwarf_lines->sourceFile[i]);
        
        if (strcmp(filename, source_file) == 0)
        {
             if (line == debugger->dwarf_lines->lineNumber[i])
             {
                line_number = debugger->dwarf_lines->lineNumber[i];
                location = debugger->dwarf_lines->location[i];
             }
        }
    }
    
    if (location >= 0)
        debugger->breakpoints[location/2] = 1;
}
*/

void clear_breakpoint(debugger_t* debugger, const uint32_t location)
{
    debugger->breakpoints[location] = 0;
}

void toggle_breakpoint(debugger_t* debugger, const uint32_t location)
{
    debugger->breakpoints[location] ^= 1;
}

void add_hardware_breakpoint(debugger_t* debugger, uint8_t* reg, char* reg_name, uint8_t condition, uint8_t compare_value)
{
    hardware_breakpoint_t* breakpoint = (hardware_breakpoint_t*) malloc(sizeof(hardware_breakpoint_t));
    
    breakpoint->reg = reg;
    breakpoint->reg_name = reg_name;
    breakpoint->condition = condition;
    breakpoint->compare_value = compare_value;
    breakpoint->active = 1;
    
    debugger->hardware_breakpoints[debugger->n_hardware_breakpoints] = breakpoint;
    
    debugger->n_hardware_breakpoints++;
}

int hardware_breakpoint_condition_met(hardware_breakpoint_t* breakpoint)
{
    switch (breakpoint->condition)
    {
        case HARDWARE_BREAKPOINT_CONDITION_LESSTHAN:
            if (*(breakpoint->reg) < breakpoint->compare_value)
            {
                return 1;
            }
            break;
        
        case HARDWARE_BREAKPOINT_CONDITION_LESSTHAN_EQUAL:
        	if (*(breakpoint->reg) <= breakpoint->compare_value)
            {
                return 1;
            }
            break;
        
        case HARDWARE_BREAKPOINT_CONDITION_GREATERTHAN:
        	if (*(breakpoint->reg) > breakpoint->compare_value)
            {
                return 1;
            }
            break;
            
        case HARDWARE_BREAKPOINT_CONDITION_GREATERTHAN_EQUAL:
        	if (*(breakpoint->reg) >= breakpoint->compare_value)
            {
                return 1;
            }
            break;
            
        case HARDWARE_BREAKPOINT_CONDITION_EQUAL:
            if (*(breakpoint->reg) == breakpoint->compare_value)
            {
                return 1;
            }
            break;
        
        case HARDWARE_BREAKPOINT_CONDITION_CHANGED:
        	if ((*(breakpoint->reg) & breakpoint->compare_value) != (breakpoint->prev_reg & breakpoint->compare_value))
            {
                return 1;
            }
            break;
            
        default:
            return 0;
            break;    
    }
    
    return 0;
}

void populate_sources(microcontroller_t* microcontroller, Elf* elf)
{
    debugger_t* debugger = microcontroller->debugger;
    
    Dwarf_Bool is_info = 1;
	Dwarf_Unsigned cu_header_length = 0;
	Dwarf_Half version_stamp = 0;
	Dwarf_Unsigned abbrev_offset = 0;
	Dwarf_Half address_size = 0;
	Dwarf_Half offset_size = 0;
	Dwarf_Half extension_size = 0;
	Dwarf_Unsigned next_cu_header = 0;
	Dwarf_Sig8 type_signature;
	Dwarf_Unsigned type_offset = 0;
	Dwarf_Error error;
	Dwarf_Attribute attr;
	int cu_number = 0;
	int n_sources = 0;

	Dwarf_Error err; 
    Dwarf_Debug dbg; 
    int rc; 
    rc = dwarf_elf_init(elf, DW_DLC_READ, NULL, NULL, &dbg, &err);
    if (rc == DW_DLV_NO_ENTRY)
    {
        printf("No DWARF debug entry found...\n");
        exit(0);
    }
    else if (rc == DW_DLV_ERROR)
    {
        printf("Error reading DWARF information: %s...\n", dwarf_errmsg(err));
        exit(1);
    }
	
	
	for(;;cu_number++) 
	{
		Dwarf_Die no_die = 0;
		Dwarf_Die cu_die = 0;
		int res = DW_DLV_ERROR;
		
		// Get compilation unit
        res = dwarf_next_cu_header_c(dbg, is_info,
         &cu_header_length, &version_stamp,
         &abbrev_offset, &address_size,
         &offset_size, &extension_size,
         &type_signature, &type_offset,
         &next_cu_header, &error);

        // Check to make sure CU is good
		if(res == DW_DLV_ERROR) 
		{
			printf("Error in dwarf_next_cu_header\n");
			exit(1);
		}
		
		// Check to see if we reached the end of CU list
		if(res == DW_DLV_NO_ENTRY) 
		{
            int i = 0;
            int j = 0;
            int k = 0;
            int l = 0;
            for (i; i<debugger->n_sources; i++)
            {
                debug_source_t* source = debugger->sources[i];
                for (j=0; j<source->n_variables; j++)
                {
                    for (k=0; k<debugger->n_types; k++)
                    {
                        if (source->variables[j]->type_offset == debugger->types[k]->offset)
                        {
                            source->variables[j]->type = debugger->types[k];
                        }
                        else
                        {
                            for (l=0; l<debugger->types[k]->n_typedef_offsets; l++)
                            {
                                if (source->variables[j]->type_offset == debugger->types[k]->typedef_offsets[l])
                                {
                                    source->variables[j]->type = debugger->types[k];
                                }
                            }
                        }
                    }
                }
                /*
                for (l=0; l<source->n_subprograms; l++)
                {
                    debug_subprogram_t* subprogram = source->subprograms[l];
                    for (j=0; j<subprogram->n_variables; j++)
                    {
                        for (k=0; k<source->n_types; k++)
                        {
                            if (subprogram->variables[j]->type_offset == source->types[k]->offset)
                            {
                                subprogram->variables[j]->type = source->types[k];
                            }
                        }
                    }
                }
                */
            }
			return;
		}
		
		// Get the sibiling of the CU, should be the first DIE
		res = dwarf_siblingof(dbg,no_die,&cu_die,&error);
		if(res == DW_DLV_ERROR) 
		{
			printf("Error in dwarf_siblingof on CU die \n");
			exit(1);
		}
		if(res == DW_DLV_NO_ENTRY) 
		{
			printf("no entry! in dwarf_siblingof on CU die \n");
			exit(1);
		}
		
		
		// Get source file names and create source struct instances
		Dwarf_Signed src_cnt = 0;
		char** src_names;
		if (dwarf_srcfiles(cu_die, &src_names, &src_cnt, &error) == DW_DLV_OK)
		{
		    // If debugger->sources has not been allocated, allocate for src_cnt
		    if (debugger->sources == 0)
		    {
		        debugger->sources = (debug_source_t**) malloc(sizeof(debug_source_t*) * src_cnt);
		    }
		    // Else reallocate for existing + src_cnt
		    else
		    {
		        debug_source_t** sources = realloc(debugger->sources, sizeof(debug_source_t*) * (debugger->n_sources + src_cnt));
		        debugger->sources = sources;
		    }
		    
		    
		    // Add new sources
		    int i = debugger->n_sources;
		    for (i; i<src_cnt+debugger->n_sources; i++)
		    {
		        debugger->sources[i] = init_debug_source();
		        
		        debugger->sources[i]->name = basename(src_names[i]);
		        strcpy(debugger->sources[i]->file_path, src_names[i]);
	
		        dwarf_attr(cu_die, DW_AT_comp_dir, &attr, &error);
                dwarf_formstring(attr, &(debugger->sources[i]->comp_dir), &error);
                
                dwarf_attr(cu_die, DW_AT_producer, &attr, &error);
                dwarf_formstring(attr, &(debugger->sources[i]->producer), &error);

                dwarf_highpc(cu_die, &(debugger->sources[i]->high_pc), &error);
                dwarf_lowpc(cu_die, &(debugger->sources[i]->low_pc), &error);
                debugger->sources[i]->high_pc += debugger->sources[i]->low_pc;
		    }
		    
		    debugger->n_sources += src_cnt;
		}
		
		if (cu_number > 0)
		{
		    crawl_dwarf_tree(debugger, dbg, cu_die, populate_variable);
		    
		    crawl_dwarf_tree(debugger, dbg, cu_die, populate_abstract_variable);
		    
		    crawl_dwarf_tree(debugger, dbg, cu_die, populate_type);
		    
		    crawl_dwarf_tree(debugger, dbg, cu_die, populate_typedef);
		    
		    // Get line numbers
            Dwarf_Line* dwarf_lines;
            Dwarf_Signed line_count;
            
            if (dwarf_srclines(cu_die, &dwarf_lines, &line_count, &error) == DW_DLV_OK)
            {
                int i = 0;
                int j = 0;
                for (i; i<line_count; i++)
                {
                    debug_source_t* source = 0;
                    if (dwarf_attr(cu_die, DW_AT_name, &attr, &error) == DW_DLV_OK)
                    {
                        char* source_name;
                        dwarf_linesrc(dwarf_lines[i], &source_name, &error);
                        
                        for (j=0; j<debugger->n_sources; j++)
                        {
                            if (strcmp(debugger->sources[j]->file_path, source_name) == 0)
                            {
                                source = debugger->sources[j];
                                break;
                            }
                        }
                        
                        if (source != 0)
                        {
                            debug_line_t* line = init_debug_line();
                            dwarf_lineno(dwarf_lines[i], &(line->line_number), &error);
                            dwarf_lineaddr(dwarf_lines[i], &(line->address), &error);
                            line->line_number--;
                            
                            printf("Source: %s; Line: %u; Address: %u\n", source->name, line->line_number, line->address);
                            
                            int line_num = line->line_number;
                            
                            if (source->bp_addresses[line_num] == 0)
                            {
                                source->bp_addresses[line_num] = line->address;
                            }
                            
                            source->lines[source->n_lines] = line;
                            source->n_lines++;
                        }
                    }
                }
            }
		}
        
		dwarf_dealloc(dbg,cu_die,DW_DLA_DIE);
	}
	
}

void crawl_dwarf_tree(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die cu_die, void (*callback)(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die))
{
    Dwarf_Error error;
    
    Dwarf_Die child_die = 0;
    Dwarf_Die sibling_die = 0;
    Dwarf_Die cur_die = cu_die;
    
    callback(debugger, dbg, cur_die);
    
    if (dwarf_child(cur_die, &child_die, &error) == DW_DLV_OK)
    {
        crawl_dwarf_tree(debugger, dbg, child_die, callback); 
    }
    
    
    while (dwarf_siblingof(dbg, cur_die, &sibling_die, &error) == DW_DLV_OK)
    {
        cur_die = sibling_die;
        
        callback(debugger, dbg, cur_die);
        
        if (dwarf_child(cur_die, &child_die, &error) == DW_DLV_OK)
        {
            crawl_dwarf_tree(debugger, dbg, child_die, callback); 
        }
    }
}

void populate_variable(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die)
{
    Dwarf_Attribute attr;
    Dwarf_Error error;
    Dwarf_Half tag = 0;
    Dwarf_Bool flag = 0;
    Dwarf_Unsigned file = 0;
    Dwarf_Off offset = 0;
    
    // Check to make sure that the tag is either variable or formal parameter
    dwarf_tag(die, &tag, &error);
    if (tag == DW_TAG_variable || tag == DW_TAG_formal_parameter)
    {
        if (dwarf_attr(die, DW_AT_decl_file, &attr, &error) != DW_DLV_OK)
        {
            return;            
        }
        if (dwarf_formudata(attr, &file, &error) != DW_DLV_OK)
        {
            return;
        }
        file--; // file should be 0-indexed
            
        debug_source_t* source = debugger->sources[file];
        
        debug_variable_t* variable = init_debug_variable();
        
        if (dwarf_attr(die, DW_AT_name, &attr, &error) != DW_DLV_OK)
        {
            return;
        }
        dwarf_formstring(attr, &(variable->name), &error);
        
        if (dwarf_attr(die, DW_AT_type, &attr, &error) != DW_DLV_OK)
        {
            return;
        }
        dwarf_formref(attr, &(variable->type_offset), &error);
        
        if (dwarf_die_CU_offset(die, &offset, &error) != DW_DLV_OK)
        {
            return;
        }
        variable->offset = offset;
        
        if (dwarf_attr(die, DW_AT_location, &attr, &error) == DW_DLV_OK) // If DIE contains address info
        {
            Dwarf_Locdesc* locations;
            Dwarf_Signed n_locations = 0;
            
            if (dwarf_loclist(attr, &locations, &n_locations, &error) == DW_DLV_OK) // If we successfully got the locations
            {
                variable->locations = locations;
                variable->n_locations = n_locations;
            }
        }
        
        source->variables[source->n_variables] = variable;
        source->n_variables++;
           
    }
}

void populate_abstract_variable(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die)
{
    Dwarf_Attribute attr;
    Dwarf_Error error;
    Dwarf_Half tag = 0;
    Dwarf_Bool flag = 0;
    Dwarf_Unsigned file = 0;
    Dwarf_Off offset = 0;
    int i = 0;
    int j = 0;
    
    dwarf_tag(die, &tag, &error);
    if (tag == DW_TAG_variable || tag == DW_TAG_formal_parameter)
    {
        if (dwarf_attr(die, DW_AT_abstract_origin, &attr, &error) != DW_DLV_OK)
        {
            return;
        }
        dwarf_formref(attr, &offset, &error);
        

        for (i; i<debugger->n_sources; i++)
        {
            debug_source_t* source = debugger->sources[i];
            
            for (j=0; j<source->n_variables; j++)
            {
                if (source->variables[j]->offset == offset)
                {
                    if (dwarf_attr(die, DW_AT_location, &attr, &error) != DW_DLV_OK)
                    {
                        return;
                    }
                    
                    Dwarf_Locdesc* locations;
                    Dwarf_Signed n_locations = 0;
                    
                    if (dwarf_loclist(attr, &locations, &n_locations, &error) != DW_DLV_OK)
                    {
                        return;
                    }
                    
                    source->variables[j]->locations = locations;
                    source->variables[j]->n_locations = n_locations;
                }
            }
        }
    }
}

void populate_type(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die)
{
    Dwarf_Attribute attr;
    Dwarf_Error error;
    Dwarf_Half tag = 0;
    Dwarf_Unsigned encoding = 0;
    Dwarf_Unsigned size = 0;
    Dwarf_Bool flag = 0;
    Dwarf_Off offset = 0;
    
    dwarf_tag(die, &tag, &error);
    if (tag == DW_TAG_base_type)
    {
        debug_type_t* type = init_debug_type();
        
        if (dwarf_attr(die, DW_AT_name, &attr, &error) != DW_DLV_OK)
        {
            return;
        }
        dwarf_formstring(attr, &(type->name), &error);
        
        if (dwarf_die_CU_offset(die, &offset, &error) != DW_DLV_OK)
        {
            return;
        }
        type->offset = offset;
        
        if (dwarf_attr(die, DW_AT_encoding, &attr, &error) != DW_DLV_OK)
        {
            return;
        }
        dwarf_formudata(attr, &encoding, &error);
        type->encoding = encoding;
        
        if (dwarf_attr(die, DW_AT_byte_size, &attr, &error) != DW_DLV_OK)
        {
            return;
        }
        dwarf_formudata(attr, &size, &error);
        type->size = size;
        
        debugger->types[debugger->n_types] = type;
        debugger->n_types++;
    }
}

void populate_typedef(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die)
{
    Dwarf_Attribute attr;
    Dwarf_Error error;
    Dwarf_Half tag = 0;
    Dwarf_Bool flag = 0;
    Dwarf_Off offset = 0;
    int i = 0;
    
    dwarf_tag(die, &tag, &error);
    if (tag == DW_TAG_typedef)
    {
        if (dwarf_attr(die, DW_AT_type, &attr, &error) != DW_DLV_OK)
        {
            return;
        }
        
        dwarf_formref(attr, &offset, &error);
        
        for (i; i<debugger->n_types; i++)
        {
            if (offset == debugger->types[i]->offset)
            {
                if (dwarf_die_CU_offset(die, &offset, &error) != DW_DLV_OK)
                {
                    return;
                }
                
                debugger->types[i]->typedef_offsets[debugger->types[i]->n_typedef_offsets] = offset;
                debugger->types[i]->n_typedef_offsets++;
                break;
            }
        }
    }
}

void populate_source_variable(microcontroller_t* microcontroller, Dwarf_Debug dbg, Dwarf_Die die, debug_source_t* source)
{
    Dwarf_Attribute attr;
    Dwarf_Error error;
    
    source->variables[source->n_variables] = init_debug_variable();
    source->variables[source->n_variables]->low_pc = source->low_pc;
    source->variables[source->n_variables]->high_pc = source->high_pc;
    
    dwarf_attr(die, DW_AT_name, &attr, &error);
    dwarf_formstring(attr, &(source->variables[source->n_variables]->name), &error);
    
    dwarf_attr(die, DW_AT_type, &attr, &error);
    dwarf_formref(attr, &(source->variables[source->n_variables]->type_offset), &error);
    
    if (dwarf_attr(die, DW_AT_location, &attr, &error) == DW_DLV_OK)
    {
        Dwarf_Locdesc* locations;
        Dwarf_Signed n_locations = 0;
        
        
        if (dwarf_loclist(attr, &locations, &n_locations, &error) == DW_DLV_OK)
        {
            source->variables[source->n_variables]->n_locations = n_locations;
            
            
            printf("n_locations: %i\n", n_locations);
            printf("ld_cents: %u\n", locations[0].ld_cents);
            int i = 0;
            for (i; i<locations[0].ld_cents; i++)
            {
                printf("Atom: %X; N1: %X; N2: %X\n", locations[0].ld_s[i].lr_atom, locations[0].ld_s[i].lr_number, locations[0].ld_s[i].lr_number2);
            }
            
        }
        else
        {
            printf("loclist error: %s...\n", dwarf_errmsg(error));
        }
    }
    else
    {
        printf("location attribute error\n");
    }
    
    source->n_variables++;
}

void populate_subprogram_variable(microcontroller_t* microcontroller, Dwarf_Debug dbg, Dwarf_Die die, debug_subprogram_t* subprogram)
{
    Dwarf_Attribute attr;
    Dwarf_Error error;
    Dwarf_Off offset;
    Dwarf_Half form;
    Dwarf_Die type_die;
    char* type_name;
    
    subprogram->variables[subprogram->n_variables] = init_debug_variable();
    subprogram->variables[subprogram->n_variables]->low_pc = subprogram->low_pc;
    subprogram->variables[subprogram->n_variables]->high_pc = subprogram->high_pc;
    
    dwarf_attr(die, DW_AT_name, &attr, &error);
    dwarf_formstring(attr, &(subprogram->variables[subprogram->n_variables]->name), &error);
    
    
    dwarf_attr(die, DW_AT_type, &attr, &error);
    dwarf_formref(attr, &(subprogram->variables[subprogram->n_variables]->type_offset), &error);

    
    if (dwarf_attr(die, DW_AT_location, &attr, &error) == DW_DLV_OK)
    {
        Dwarf_Locdesc* locations;
        Dwarf_Signed n_locations = 0;
        
        if (dwarf_loclist(attr, &locations, &n_locations, &error) == DW_DLV_OK)
        {
            subprogram->variables[subprogram->n_variables]->locations = locations;
            subprogram->variables[subprogram->n_variables]->n_locations = n_locations;
            
        }
        else
        {
            printf("loclist error: %s...\n", dwarf_errmsg(error));
        }
    }
    else
    {
        printf("location attribute error\n");
    }
    

    subprogram->n_variables++;
}

void populate_subprogram(microcontroller_t* microcontroller, Dwarf_Debug dbg, Dwarf_Die die, debug_source_t* source)
{
    Dwarf_Attribute attr;
    Dwarf_Error error;
    
    source->subprograms[source->n_subprograms] = init_debug_subprogram();
    
    
    dwarf_attr(die, DW_AT_name, &attr, &error);
    dwarf_formstring(attr, &(source->subprograms[source->n_subprograms]->name), &error);
    dwarf_lowpc(die, &(source->subprograms[source->n_subprograms]->low_pc), &error);
    dwarf_highpc(die, &(source->subprograms[source->n_subprograms]->high_pc), &error);
    source->subprograms[source->n_subprograms]->high_pc += source->subprograms[source->n_subprograms]->low_pc;
    
    
    
    Dwarf_Die child_die = 0;
    Dwarf_Die sibling_die = 0;
    Dwarf_Die cur_die = 0;
    
    int rc = 0;
    
    if (rc = dwarf_child(die, &cur_die, &error) == DW_DLV_OK)
    {
        Dwarf_Half tag = 0;
        dwarf_tag(cur_die, &tag, &error);
        if (tag == DW_TAG_variable)
        {
            populate_subprogram_variable(microcontroller, dbg, cur_die, source->subprograms[source->n_subprograms]);
        }
        else if (tag == DW_TAG_base_type)
        {
            populate_type(dbg, cur_die, source);
        }
        
        rc = dwarf_siblingof(dbg, cur_die, &sibling_die, &error);
        while (rc == DW_DLV_OK)
        {
            cur_die = sibling_die;
            
            dwarf_tag(cur_die, &tag, &error);
            
            if (tag == DW_TAG_variable)
            {
                populate_subprogram_variable(microcontroller, dbg, cur_die, source->subprograms[source->n_subprograms]);
            }
            else if (tag == DW_TAG_base_type)
            {
                populate_type(dbg, cur_die, source);
            }
            
            rc = dwarf_siblingof(dbg, cur_die, &sibling_die, &error);
        }
    }
            
            

    source->n_subprograms++;
}

uint8_t* debug_variable_location(microcontroller_t* microcontroller, debug_variable_t* variable)
{
    uint16_t pc = *(microcontroller->registers->PC);
    int i = 0;
    int j = 0;
    
    for (i; i<variable->n_locations; i++)
    {
        
        Dwarf_Locdesc location_description = variable->locations[i];
        if (location_description.ld_lopc <= pc && location_description.ld_hipc >= pc)
        {
            for (j; j<location_description.ld_cents; j++)
            {
                Dwarf_Loc location = location_description.ld_s[j];
                
                Dwarf_Small atom = location.lr_atom;
                Dwarf_Unsigned n1 = location.lr_number;
                Dwarf_Unsigned n2 = location.lr_number2;
                Dwarf_Unsigned offset = location.lr_offset;
                
                uint16_t address = 0;
                
                switch (atom)
                {
                    case DW_OP_addr:
                        return 0;
                    
                    case DW_OP_breg28:
                        memcpy(&address, microcontroller->registers->r + 28, 2); 
                        address += n1;
                        //printf("%X: %X\n", address, microcontroller->sram->data[address]);
                        if (address > microcontroller->sram->size)
                        {
                            return 0;
                        }
                        return microcontroller->sram->data + address;
                        
                    default:
                        return 0;
                }
            }
        }
    }
    
    return 0; 
    
}


char* debug_variable_value_hex(microcontroller_t* microcontroller, debug_variable_t* variable)
{
    if (*(microcontroller->registers->PC) < variable->locations[0].ld_lopc ||
        *(microcontroller->registers->PC) > variable->locations[0].ld_hipc)
    {
        return "N/A";
    }
    
    uint8_t* loc = debug_variable_location(microcontroller, variable);
    //printf("%s:%u -> %X:%X\n", variable->name, variable->type->size, variable->locations[0].ld_lopc, variable->locations[0].ld_hipc);
    //printf("%u: %X %X\n", loc, *loc, *(loc+1));
    if (loc == 0)
    {
        return "Bad Location";
    }
    char* val_string = (char*) calloc(32, sizeof(char));
    unsigned val_unsigned = 0;
    memcpy(&val_unsigned, loc, variable->type->size);
    sprintf(val_string, "0x%X", val_unsigned);
    return val_string;
    
}

char* debug_variable_value_string(microcontroller_t* microcontroller, debug_variable_t* variable)
{
    if (*(microcontroller->registers->PC) < variable->locations[0].ld_lopc ||
        *(microcontroller->registers->PC) > variable->locations[0].ld_hipc)
    {
        return "N/A";
    }
    uint8_t* loc = debug_variable_location(microcontroller, variable);
    //printf("%s:%u -> %X:%X\n", variable->name, variable->type->size, variable->locations[0].ld_lopc, variable->locations[0].ld_hipc);
    //printf("%u: %X %X\n", loc, *loc, *(loc+1));
    if (loc == 0)
    {
        return "Bad Location";
    }
    char* val_string = (char*) calloc(32, sizeof(char));
    int val_signed = 0;
    unsigned val_unsigned = 0;
    double val_double = 0.0;
    
    switch (variable->type->encoding)
    {
        case DW_ATE_address:
            return "N/A";
        case DW_ATE_boolean:
            return "N/A";
        case DW_ATE_complex_float:
            return "N/A";
        case DW_ATE_float:
            memcpy(&val_double, loc, variable->type->size);
            sprintf(val_string, "%f", val_double);
            return val_string;
        case DW_ATE_signed:
            memcpy(&val_signed, loc, variable->type->size);
            //printf("%X\n", loc);
            sprintf(val_string, "%i", val_signed);
            return val_string;
        case DW_ATE_signed_char:
            return "N/A Singed Char";
        case DW_ATE_unsigned:
            memcpy(&val_unsigned, loc, variable->type->size);
            sprintf(val_string, "%u", val_unsigned);
            return val_string;
        case DW_ATE_unsigned_char:
            return "N/A";
        case DW_ATE_imaginary_float:
            return "N/A";
        case DW_ATE_packed_decimal:
            return "N/A";
        case DW_ATE_numeric_string:
            return "N/A";
        case DW_ATE_edited:
            return "N/A";
        case DW_ATE_signed_fixed:
            return "N/A";
        case DW_ATE_unsigned_fixed:
            return "N/A";
        case DW_ATE_decimal_float:
            return "N/A";
        case DW_ATE_UTF:
            return "N/A";
        case DW_ATE_UCS:
            return "N/A";
        case DW_ATE_ASCII:
            return "N/A";
        default:
            return "N/A";
    } 
}

char* debug_variable_type_name(debug_variable_t* variable)
{
    if (variable->type == 0)
        return "N/A";
    else
        return variable->type->name;
}

debug_source_t* init_debug_source()
{
    debug_source_t* source = (debug_source_t*) malloc(sizeof(debug_source_t));
    source->name = 0;
    source->comp_dir = 0;
    //source->file_path = 0;
    source->producer = 0;
    source->low_pc = 0;
    source->high_pc = 0;
    source->source_num = 0;
    source->n_variables = 0;
    source->n_subprograms = 0;
    source->n_lines = 0;
    
    int i = 0;
    for (i; i<1024; i++)
    {
        source->bp_addresses[i] = 0;
    }
    
    return source;
}

debug_variable_t* init_debug_variable()
{
    debug_variable_t* var = (debug_variable_t*) malloc(sizeof(debug_variable_t));
    var->name = 0;
    var->low_pc = 0;
    var->high_pc = 0;
    var->offset = 0;
    var->type_offset = 0;
    var->type = 0;
    var->n_locations = 0;
    
    return var;
}

debug_subprogram_t* init_debug_subprogram()
{
    debug_subprogram_t* subprog = (debug_subprogram_t*) malloc(sizeof(debug_subprogram_t));
    subprog->name = 0;
    subprog->low_pc = 0;
    subprog->high_pc = 0;
    subprog->n_variables = 0;
    
    return subprog;
}

debug_line_t* init_debug_line()
{
    debug_line_t* line = (debug_line_t*) malloc(sizeof(debug_line_t));
    line->line_number = 0;
    line->address = 0;
    
    return line;
}

debug_location_t* init_debug_location()
{
    debug_location_t* loc = (debug_location_t*) malloc(sizeof(debug_location_t));
    loc->low_pc = 0;
    loc->high_pc = 0;
    loc->value = 0;
    
    return loc;
}

debug_type_t* init_debug_type()
{
    debug_type_t* type = (debug_type_t*) malloc(sizeof(debug_type_t));
    type->avr_type = 0;
    type->name = 0;
    type->size = 0;
    type->encoding = 0;
    type->offset = 0;
    type->n_typedef_offsets = 0;
    
    return type;
}
