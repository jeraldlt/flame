/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

timers.c

*/


#include "timers.h"
#include "util.h"
#include <stdlib.h>


timer_counter_t* init_timer(uint8_t timer_num, void (*pre_handler)(), void (*post_handler)())
{
    timer_counter_t* timer = (timer_counter_t*) malloc(sizeof(timer_counter_t));
    timer->timer_num = timer_num;
    timer->handle_timer_pre = pre_handler;
    timer->handle_timer_post = post_handler;
    
    return timer;
}

void timer8(microcontroller_t* microcontroller, timer_counter_t* timer)
{
    
    if ((*(timer->tccrxb) & 0x07)) // TC enabled
    {
        uint16_t prescalar;
        if ((*(timer->tccrxb) & 0x07) == 1)
            prescalar = 1;
        else if ((*(timer->tccrxb) & 0x07) == 2)
            prescalar = 8;
        else if ((*(timer->tccrxb) & 0x07) == 3)
            prescalar = 64;
        else if ((*(timer->tccrxb) & 0x07) == 4)
            prescalar = 256;
        else if ((*(timer->tccrxb) & 0x07) == 5)
            prescalar = 1024;
        
        timer->prev_tcnt = *(microcontroller->registers->TCNT0);
        timer->counter += (microcontroller->ticks - microcontroller->prev_ticks);
        *(timer->tcntx) = (timer->counter / prescalar) % 256;            
    }
}

void timer16_pre(microcontroller_t* microcontroller, timer_counter_t* timer)
{
    if ((*(timer->tccrxb) & 0x07)) // TC enabled
    {
        uint16_t prescalar;
        if ((*(timer->tccrxb) & 0x07) == 1)
            prescalar = 1;
        else if ((*(timer->tccrxb) & 0x07) == 2)
            prescalar = 8;
        else if ((*(timer->tccrxb) & 0x07) == 3)
            prescalar = 64;
        else if ((*(timer->tccrxb) & 0x07) == 4)
            prescalar = 256;
        else if ((*(timer->tccrxb) & 0x07) == 5)
            prescalar = 1024;
            
        timer->prev_tcnt = (*(timer->tcntxh) << 8) | *(timer->tcntxl);
        timer->counter += (microcontroller->ticks - microcontroller->prev_ticks);
        
        uint16_t tcnt = (timer->counter / prescalar) % 65536;
        *(timer->tcntxh) = (tcnt & 0xFF00) >> 8;
        *(timer->tcntxl) = (tcnt & 0x00FF);
        
        uint16_t ocrxa = (*(timer->ocrxah) << 8) | *(timer->ocrxal);
        uint16_t ocrxb = (*(timer->ocrxbh) << 8) | *(timer->ocrxbl);
        
        *(timer->tifrx) = 0;
        if (*(timer->timskx) & 0x01 && timer->prev_tcnt > tcnt) // overflow
        {
            *(timer->tifrx) |= 0x01;
        }
        if (*(timer->timskx) & 0x02 && tcnt >= ocrxa) // OCRA Match
        {
            *(timer->tifrx) |= 0x02;
        }
        if (*(timer->timskx) & 0x04 && tcnt >= ocrxb) // OCRB Match
        {
            *(timer->tifrx) |= 0x04;
        }
    }
}

void timer16_post(microcontroller_t* microcontroller, timer_counter_t* timer)
{
    if ((*(timer->tccrxb) & 0x07)) // TC enabled
    {
        uint16_t prescalar;
        if ((*(timer->tccrxb) & 0x07) == 1)
            prescalar = 1;
        else if ((*(timer->tccrxb) & 0x07) == 2)
            prescalar = 8;
        else if ((*(timer->tccrxb) & 0x07) == 3)
            prescalar = 64;
        else if ((*(timer->tccrxb) & 0x07) == 4)
            prescalar = 256;
        else if ((*(timer->tccrxb) & 0x07) == 5)
            prescalar = 1024;
        
        uint16_t tcnt = (timer->counter / prescalar) % 65536;
        
        uint8_t mode = (*(timer->tccrxa) & 0x03) | ((*(timer->tccrxb) & 0x18) >> 1);
        uint8_t com = (*(timer->tccrxa) & 0xF0) >> 4;
        switch(mode)
        {
            case 0:
                
                break;
            case 4:
                if (*(timer->tifrx) & 0x02)
                {
                    *(timer->tcntxh) = 0;
                    *(timer->tcntxl) = 0;
                    timer->counter = 0;
                }
                break;
            
            case 15:
                if ((com & 0x3) == 2) // Channel B non-inverting
                {
                    if (*(timer->tifrx) & 0x04)
                    {
                        *(microcontroller->registers->PORTB) &= ~(0x20);
                    }
                    else if (timer->prev_tcnt > tcnt) // Overflow
                    {
                        *(microcontroller->registers->PORTB) |= 0x20;
                    }                
                }
                else if ((com & 0x3) == 3) // Channel B inverting
                {
                    if (*(timer->tifrx) & 0x04)
                    {
                        *(microcontroller->registers->PORTB) |= 0x20;
                    }
                    else if (timer->prev_tcnt > tcnt) // Overflow
                    {
                        *(microcontroller->registers->PORTB) &= ~(0x20);
                    } 
                }
                break;
                
            default:
                notice(microcontroller, "Timer: In Default Timer");
                break;
        }
    }
}
