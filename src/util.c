/*
This file is part of glaed.

    glaed is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    glaed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with glaed.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

util.c
    
*/


#include "util.h"
#include "globals.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gtksourceview/gtksource.h>


void sliceStr(const char* str, char* buffer, size_t start, size_t end)
{
    size_t j = 0;
    for ( size_t i = start; i <= end; ++i ) {
        buffer[j++] = str[i];
    }
    buffer[j] = '\0';
}

uint8_t bit(uint32_t val, uint8_t place, uint8_t negate)
{
    if (val & (1 << place))
    {
        if (negate)
            return 0;
        return 1;
    }
    
    if (negate)
        return 1;
    return 0;
}

uint32_t hexToDec(const char* hex, uint8_t size)
{
    short i = size - 1;
    uint8_t power = 0;
    uint32_t dec;
    
    for (i; i>=0; i--)
    {
        dec += hexCharToDec(hex[i]) * (pow(16, power++));
    }
    
    return dec;
}

uint8_t hexCharToDec(const char hex)
{
    switch(hex)
    {
        case '0':
            return 0;
        case '1':
            return 1;
        case '2': 
            return 2;
        case '3':
            return 3;
        case '4':
            return 4;
        case '5':
            return 5;
        case '6':
            return 6;
        case '7':
            return 7;
        case '8':
            return 8;
        case '9':
            return 9;
        case 'a':
        case 'A':
            return 10;
        case 'b':
        case 'B':
            return 11;
        case 'c':
        case 'C':
            return 12;
        case 'd':
        case 'D':
            return 13;
        case 'e':
        case 'E':
            return 14;
        case 'f':
        case 'F':
            return 15;
        default:
            return 0;
    }
}

void hexToBin(const char* hex, char* bin, uint8_t size)
{
    char* tmp;
    int i = 0;
    int power = 0;
    
    for (i; i<size; i++)
    {
        tmp = hexCharToBin(hex[i]);
        int j = i * 4;
        int k = 0;
        for (j,k; k<4; j++,k++)
        {
            bin[j] = tmp[k];
        }
    }
    
    bin[4*size] = 0;
}

void decToBin(uint32_t dec, char* bin, uint8_t nbits)
{
    char tmp[100];
    
    int i = 0;
    for (i; i<nbits; i++)
    {
        if (dec & 1)
            tmp[i] = '1';
        else
            tmp[i] = '0';
        
        dec = dec >> 1;
    }
    
    int j = 0;
    i--;
    for (i, j; i>=0; i--, j++)
    {
        bin[j] = tmp[i];
    }
    
    bin[nbits] = 0;
}

char* hexCharToBin(const char hex)
{
    switch(hex)
    {
        case '0':
            return "0000";
        case '1':
            return "0001";
        case '2': 
            return "0010";
        case '3':
            return "0011";
        case '4':
            return "0100";
        case '5':
            return "0101";
        case '6':
            return "0110";
        case '7':
            return "0111";
        case '8':
            return "1000";
        case '9':
            return "1001";
        case 'a':
        case 'A':
            return "1010";
        case 'b':
        case 'B':
            return "1011";
        case 'c':
        case 'C':
            return "1100";
        case 'd':
        case 'D':
            return "1101";
        case 'e':
        case 'E':
            return "1110";
        case 'f':
        case 'F':
            return "1111";
        default:
            return "0000";
    }
}


// Pulled From (with modifications):
// https://www.geeksforgeeks.org/program-decimal-hexadecimal-conversion/
char* decToHex(int n) 
{    
    // char array to store hexadecimal number 
    char hexaDeciNum[100]; 
      
    // counter for hexadecimal number array 
    int i = 0; 
    while(n!=0) 
    {    
        // temporary variable to store remainder 
        int temp  = 0; 
          
        // storing remainder in temp variable. 
        temp = n % 16; 
          
        // check if temp < 10 
        if(temp < 10) 
        { 
            hexaDeciNum[i] = temp + 48; 
            i++; 
        } 
        else
        { 
            hexaDeciNum[i] = temp + 55; 
            i++; 
        } 
          
        n = n/16; 
    }
    
     
      
    int j = i - 1;
    int k = 0;
    
    char* retVal = (char*) malloc(sizeof(char) * (j+1));     
    for(j; j>=0; j--, k++) 
        retVal[k] = hexaDeciNum[j];
    
    retVal[k] = 0;
    
    return retVal;
} 

char decToHexChar(const int dec)
{
    switch(dec)
    {
        case 0:
            return '0';
        case 1:
            return '1';
        case 2:
            return '2';
        case 3:
            return '3';
        case 4:
            return '4';
        case 5:
            return '5';
        case 6:
            return '6';
        case 7:
            return '7';
        case 8:
            return '8';
        case 9:
            return '9';
        case 10:
            return 'a';
        case 11:
            return 'b';
        case 12:
            return 'c';
        case 13:
            return 'd';
        case 14:
            return 'e';
        case 15:
            return 'f';
        default:
            return '0';
    }
}

uint16_t inline swapBytes(const uint16_t word)
{
    return ( (word & 0x00FF) << 8 | (word & 0xFF00) >> 8 );
}


