/*
This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

vcd.c
    
*/


#include <stdlib.h>
#include <stdio.h>
#include "vcd.h"
#include "util.h"

vcd_variable_t* init_vcd_variable(char* name, char identifier, char* register_name, uint8_t* byte, uint8_t bit)
{
    vcd_variable_t* vcd_var = (vcd_variable_t*) malloc(sizeof(vcd_variable_t));
    
    vcd_var->name = name;
    vcd_var->identifier = identifier;
    vcd_var->register_name = register_name;
    vcd_var->byte = byte;
    vcd_var->bit = bit;
    vcd_var->last_val = (*byte & (1 << bit));
    vcd_var->change_count = 1;
    vcd_var->change_ticks = calloc(1, sizeof(uint64_t)*1000);
    vcd_var->change_ticks[0] = 0;
    vcd_var->change_vals = calloc(1, sizeof(uint8_t)*1000);
    vcd_var->change_vals[0] = (*byte & (1 << bit)) >> bit;
    
    return vcd_var;
}

void vcd_change(vcd_variable_t* vcd, uint64_t ticks)
{
    if (((*(vcd->byte) & (1 << vcd->bit)) ^ vcd->last_val) == 0) // no change occured
        return;

    //vcd->changeTicks = realloc(vcd->changeTicks, (vcd->changeCount + 1) * sizeof(uint64_t));
    //vcd->changeVals = realloc(vcd->changeVals, (vcd->changeCount + 1) * sizeof(uint8_t));
    
    vcd->change_ticks[vcd->change_count] = ticks;
    vcd->change_vals[vcd->change_count] = (*(vcd->byte) & (1 << vcd->bit)) >> vcd->bit;
    
    vcd->change_count++;
    
    vcd->last_val = (*(vcd->byte) & (1 << vcd->bit));
    
}

char vcdIdentifiers[10] = {'!', '@', '#', '$', '%', '^', '&', '*', '(', ')'};
