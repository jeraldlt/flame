/*
This file is part of glaed.

    glaed is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    glaed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with glaed.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

print_instructions.c
    
*/


#include <stdint.h>
#include <stdarg.h>
#include "print_instructions.h"
#include "util.h"
#include "microcontroller.h"
#include "registers.h"

void print_and(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    if (r == d)
    {
        print_tst(microcontroller, str);
        return;
    }
    
    sprintf(str, "and; d=%i, r=%i", d, r);
}

void print_adc(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    if (r == d)
    {
        print_rol(microcontroller, str);
        return;
    }
    
    sprintf(str, "adc; d=%i, r=%i", d, r);
}

void print_add(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    if (r == d)
    {
        print_lsl(microcontroller, str);
        return;
    }
    
    sprintf(str, "add; d=%i, r=%i", d, r);
}

void print_ori(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    sprintf(str, "ori; d=%i, K=0x%X", d, K);
}

void print_clr(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "clr; d=%i", d);
}

void print_rol(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "rol; d=%i", d);
}

void print_lsl(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "lsl; d=%i", d);
}

void print_tst(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "tst; d=%i", d);
}

void print_andi(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    sprintf(str, "andi; d=%i, K=0x%X", d, K);
}

void print_asr(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "asr; d=%i", d);
}

void print_bclr(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t s = (ins1 & 0x0070) >> 4;
    
    sprintf(str, "bclr; s=%i", s);
}

void print_bld(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t b = ins1 & 0x0007;
    
    sprintf(str, "bld; d=%i, b=%i", d, b);
}

void print_brbc(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t s = (ins1 & 0x0007);
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brsh; s=%i, k=%i; PC->0x%X|0x%X", s, k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brbs(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t s = (ins1 & 0x0007);
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brsh; s=%i, k=%i; PC->0x%X|0x%X", s, k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brsh(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brsh; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brlo(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brlo; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_breq(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "breq; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brge(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brge; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brhc(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brhc; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brhs(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brhs; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brid(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brid; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brie(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brie; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brlt(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brlt; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brmi(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brmi; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brne(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brne; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brpl(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brpl; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brtc(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brtc; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brts(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brts; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brvc(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brvc; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_brvs(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = (ins1 & 0x03F8) >> 3;
    if (k & 0x0040) // k is negative
    {
        k ^= 0x007F;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "brvs; k=%i; PC->0x%X|0x%X", k, (*(microcontroller->registers->PC) + (k + 1)) * 2, (*(microcontroller->registers->PC) + 1) * 2);
}

void print_bset(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t s = (ins1 & 0x0070) >> 4;
    
    sprintf(str, "bset; s=%i", s);
}

void print_bst(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    uint8_t b = (ins1 & 0x0007);
    
    sprintf(str, "bst; d=%i, b=%i", d, b);
}

void print_cbi(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = (ins1 & 0x00F8) >> 3;
    A += 0x20;
    uint8_t b = ins1 & 0x0007;
    
    sprintf(str, "cbi; A=0x%X", A);
}

void print_clc(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "clc;");
}

void print_clh(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "clh;");
}

void print_cli(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "cli;");
}

void print_cln(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "cln;");
}

void print_eor(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    if (r == d)
    {
        print_clr(microcontroller, str);
        return;
    }
    
    sprintf(str, "eor; r=%i, d=%i", r, d);
}

void print_cls(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "cls;");
}

void print_clt(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "clt;");
}

void print_clv(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "clv;");
}

void print_clz(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "clz;");
}

void print_com(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "com; d=%i", d);
}

void print_cp(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "cp; d=%i, r=%i", d, r);
}

void print_cpc(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "cpc; d=%i, r=%i", d, r);
}

void print_cpi(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    sprintf(str, "cpi; d=%i, K=0x%X", d, K);
}

void print_cpse(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    uint8_t r = ((ins1 & 0x0200) >> 5) | (ins1 & 0x000F);
    
    sprintf(str, "cpse; d=%i, r=%i", d, r);
}

void print_dec(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "dec; d=%i", d);
}

void print_in(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = ins1 & 0x000F;
    A |= (ins1 & 0x0600) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "in; A=0x%X, d=%i", A, d);
}

void print_inc(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "inc; d=%i", d);
}

void print_ldx1(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld X; d=%i", d);
}

void print_ldx2(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld X+; d=%i", d);
}

void print_ldx3(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld -X; d=%i", d);
}

void print_ldy1(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld Y; d=%i", d);
}

void print_ldy2(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld Y+; d=%i", d);
}

void print_ldy3(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld -Y; d=%i", d);}

void print_ldy4(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t q = (ins1 & 0x0007) | ((ins1 & 0x0C00) >> 7) | ((ins1 & 0x2000) >> 8);
    
    uint16_t address = microcontroller->registers->r[28];
    address << 8;
    address |= microcontroller->registers->r[29];
    address += q;
    
    sprintf(str, "ldd Y+q; d=%i, q=%i", d, q);
}

void print_ldz1(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld Z; d=%i", d);
}

void print_ldz2(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld Z+; d=%i", d);
}

void print_ldz3(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    sprintf(str, "ld -Z; d=%i", d);
}

void print_ldz4(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t q = (ins1 & 0x0007) | ((ins1 & 0x0C00) >> 7) | ((ins1 & 0x2000) >> 8);
    
    uint16_t address = microcontroller->registers->r[31];
    address << 8;
    address |= microcontroller->registers->r[31];
    address += q;
    
    sprintf(str, "ldd Z+q; d=%i, q=%i", d, q);
}

void print_ldi(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    sprintf(str, "ldi; d=%i, K=0x%X", d, K);
}

void print_lpm1(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _lpm1");
}

void print_lpm2(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _lpm2");
}

void print_lpm3(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _lpm3");
}

void print_lsr(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "lsr; d=%i", d);
}

void print_mov(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "mov; d=%i, r=%i", d, r);
}

void print_neg(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "neg; d=%i", d);
}

void print_nop(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "nop");
}

void print_or(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = ins1 & 0x000F;
    r |= (ins1 & 0x0200) >> 5;
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    
    sprintf(str, "or; d=%i, r=%i", d, r);
}

void print_sbr(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    
    sprintf(str, "sbr; d=%i, K=0x%X", d, K);
}

void print_out(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = ins1 & 0x000F;
    A |= (ins1 & 0x0600) >> 5;
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "out; A=0x%X, r=%i", A, r);
}

void print_rcall(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = ins1 & 0x0FFF;
    
    if (k & 0x0800) // k is negative
    {
        k ^= 0x0FFF;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "rcall; k=%i, PC->0x%X", k, (*(microcontroller->registers->PC) + (1 + k)) * 2 );
}

void print_ret(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "ret;");
}

void print_reti(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "reti;");
}

void print_rjmp(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    int16_t k = ins1 & 0x0FFF;
    
    if (k & 0x0800) // k is negative
    {
        k ^= 0x0FFF;
        k += 1;
        k *= -1;
    }
    
    sprintf(str, "rjmp; k=%i, PC->0x%X", k, (*(microcontroller->registers->PC) + (1 + k)) * 2 );
}

void print_ror(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "ror; d=%i", d);
}

void print_sbc(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t r = ((ins1 & 0x0200) >> 5) | (ins1 & 0x000F);
    
    sprintf(str, "sbc; d=%i, r=%i", d, r);
}

void print_sbci(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    uint16_t K = ((ins1 & 0x0F00) >> 4) | (ins1 & 0x000F);
    
    sprintf(str, "sbci; d=%i, K=%i", d, K);
}

void print_sbi(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = (ins1 & 0x00F8) >> 3;
    uint16_t b = ins1 & 0x0007;
    
    sprintf(str, "sbi; A=0x%X, b=%i", A, b);
}

void print_sbic(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = (ins1 & 0x00F8) >> 3;
    A += 0x20;
    uint8_t b = ins1 & 0x0007;
    
    sprintf(str, "sbic; A=0x%X, b=%i", A, b);
}

void print_sbis(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t A = (ins1 & 0x00F8) >> 3;
    A += 0x20;
    uint8_t b = ins1 & 0x0007;
    
    sprintf(str, "sbis; A=0x%X, b=%i", A, b);
}

void print_sbrc(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    uint16_t b = ins1 & 0x0007;
    
    sprintf(str, "sbrc; r=%i, b=%i", r, b);
}

void print_sbrs(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    uint16_t b = ins1 & 0x0007;
    
    sprintf(str, "sbrs; r=%i, b=%i", r, b);
}

void print_sec(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "sec;");
}

void print_seh(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "seh;");
}

void print_sei(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "sei;");
}

void print_sen(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "sen;");
}

void print_ser(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x00F0) >> 4;
    r |= 0x0010;
    
    sprintf(str, "ser; r=%i", r);
}

void print_ses(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "ses;");
}

void print_set(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "set;");
}

void print_sev(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "sev;");
}

void print_sez(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    sprintf(str, "sez;");
}

void print_sleep(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _sleep");
}

void print_stx1(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "st X; r=%i", r);
}

void print_stx2(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    sprintf(str, "st X+; r=%i", r);
}

void print_stx3(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    sprintf(str, "st -X; r=%i", r);
}

void print_sty1(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    sprintf(str, "st Y; r=%i", r);
}

void print_sty2(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    sprintf(str, "st Y+; r=%i", r);
}

void print_sty3(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    sprintf(str, "st -Y; r=%i", r);
}

void print_sty4(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    uint16_t q = (ins1 & 0x0007) | ((ins1 & 0x0C00) >> 7) | ((ins1 & 0x2000) >> 8);
    
    sprintf(str, "std Y+q; r=%i, q=%i", r, q);
}

void print_stz1(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "st Z; r=%i", r);
}

void print_stz2(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    sprintf(str, "st Z+; r=%i", r);
}

void print_stz3(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    sprintf(str, "st -Z; r=%i", r);
}

void print_stz4(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t q = (ins1 & 0x0007) | ((ins1 & 0x0C00) >> 7) | ((ins1 & 0x2000) >> 8);
    
    sprintf(str, "std Z+q; d=%i, q=%i", d, q);
}

void print_sub(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t r = ((ins1 & 0x0200) >> 5) | (ins1 & 0x000F);
    
    sprintf(str, "sub; d=%i, r=%i", d, r);
}

void print_subi(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x0F00) >> 4;
    uint16_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    sprintf(str, "subi; d=%i, K=0x%04X", d, K);
}

void print_swap(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "swap; d=%i", d);
}

void print_wdr(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _wdr");
}

void print_adiw(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x0030) >> 4;
    d = (d * 2) + 24;
    //uint16_t d = ((ins1 & 0x0030) >> 3) | 0x001F;
    
    uint16_t K = ((ins1 & 0x00C0) >> 2) | (ins1 & 0x000F);


    sprintf(str, "adiw; d=%i, K=0x%04X", d, K);    
}

void print_icall(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "icall;");
}

void print_ijmp(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "ijmp;");
}

void print_lds32(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint16_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t k = ins2;
    
    sprintf(str, "lds32; d=%i, k=0x%04X", d, k);
}

void print_lds16(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = ((ins1 & 0x00F0) >> 4) + 16;
    uint16_t k = ((ins1 & 0x0700) >> 4) | (ins1 & 0x000F);
    
    sprintf(str, "lds16; d=%i, k=0x%04X", d, k);
}

void print_pop(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "pop; d=%i", d);
}

void print_push(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t r = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "push; r=%i", r);
}

void print_sbiw(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = (ins1 & 0x0030) >> 4;
    d = (d * 2) + 24;
    uint16_t K = ins1 & 0x000F;
    K |= (ins1 & 0x00C0) >> 2;
    
    sprintf(str, "sbiw; d=%i, K=0x%04X", d, K);
}

void print_sts32(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint16_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    
    uint16_t d = (ins1 & 0x01F0) >> 4;
    uint16_t k = ins2;
    
    sprintf(str, "lds32; k=0x%04X, d=%i", k, d);
}

void print_sts16(microcontroller_t* microcontroller, char* str)
{
    uint16_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint16_t d = ((ins1 & 0x00F0) >> 4) + 16;
    uint16_t k = ((ins1 & 0x0700) >> 4) | (ins1 & 0x000F);
    
    sprintf(str, "sts16; d=%i, k=0x%04X", d, k);
}

void print_movw(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t r = ins1 & 0x000F;
    r *= 2;
    uint8_t d = (ins1 & 0x00F0) >> 4;
    d *= 2;
    
    sprintf(str, "movw; d=%i, r=%i", d, r);
}

void print_call(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint32_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    ins1 = ((ins1 << 3) & 0x0003) | (ins1 & 0x01F1);
    
    uint32_t k = (ins1 << 13) | ins2;

    sprintf(str, "call; k=0x%04X", k*2);
}

void print_jmp(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    uint32_t ins2 = swapBytes(microcontroller->flash->data[(*(microcontroller->registers->PC)) + 1]);
    ins1 = ((ins1 << 3) & 0x0003) | (ins1 & 0x01F1);
    
    uint32_t k = (ins1 << 13) | ins2;
    
    sprintf(str, "jmp; k=0x%04X", k*2);
}

void print_elpmz1(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _elpmz1");
}

void print_elpmz2(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _elpmz2");
}

void print_elpmz3(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _elpmz3");
}

void print_fmul(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _fmul");
}

void print_fmuls(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _fmuls");
}

void print_fmulsu(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _fmulsu");
}

void print_mul(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t r = ((ins1 & 0x0200) >> 5) | (ins1 & 0x000F);
    uint8_t d = (ins1 & 0x01F0) >> 4;
    
    sprintf(str, "mul; d=%i, r=%i", d, r);
}

void print_muls(microcontroller_t* microcontroller, char* str) // IMPLEMENTED
{
    uint32_t ins1 = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
    
    uint8_t r = ins1 & 0x000F;
    r |= 0x0010;
    uint8_t d = (ins1 & 0x00F0) >> 4;
    d |= 0x0010;
    
    sprintf(str, "muls; d=%i, r=%i", d, r);
}

void print_mulsu(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _mulsu");
}

void print_spm(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _spm");
}

void print_break(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _break");
}

void print_elpmx(microcontroller_t* microcontroller, char* str)
{
    sprintf(str, "********** Calling _elpmx");
}
