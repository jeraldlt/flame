/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

context.c
    
*/
#include "context.h"

#define _POSIX_C_SOURCE 199309L
#define _DEFAULT_SOURCE
#define BILLION 1000000000L

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "util.h"
#include "instructions.h"
#include "vcd.h"

struct timespec start, end, sleep_t;
    uint32_t sleep_amount = 0;
    uint32_t target = 63;
    int j = 0;
    int n_breakpoints = 0;
    uint32_t delta_time = 0;

context_t* init_context()
{
    context_t* context = (context_t*) malloc(sizeof(context_t));
    
    context->n_mcus = 0;
    //context->cur_mcu = -1;
    context->n_connections = 0;
    
    //context->running = 0;
    context->debugging_enabled = 0;
    //context->quit = 0;
    //context->inBreakpoint = 0;
    //context->continueBreakpoint = 0;
    //context->mcuBreakpoint = -1;
    context->breakpoint_triggered = 0;
    
    //context->interruptRefresh = 0;
    context->refresh_rate = 50;
    //context->curRefresh = 0;
    
    context->vcd_vars_count = 0;
    
    context->state = STATE_STOPPED;
    context->state_last = STATE_STOPPED;
    
    context->options = init_options();
    
    return context;
}

void free_context(context_t* context)
{
    int i = 0;
    for (i; i<context->n_mcus; i++)
    {
        free_microcontroller(context->mcus[i]);
    }
    
    for (i=0; i<context->n_connections; i++)
    {
        free(context->connections[i]);
    }
    
    free(context);
}

microcontroller_t* add_mcu(context_t* context, char* model, uint8_t clock_mhz)
{
    if (context->n_mcus >= 4)
        return NULL;
        
    uint8_t n = context->n_mcus;
    microcontroller_t* mcu = init_microcontroller(model, clock_mhz);
    if (mcu == 0)
        return NULL;
    
    context->mcus[n] = mcu;
    //context->curMcu = n;
    (context->n_mcus)++;
    return mcu;
}

int step(context_t* context)
{
    
    
    connection_t* connection;
    int i = 0;
    for (i; i<context->n_connections; i++)
    {
        connection = context->connections[i];
        if (*(connection->port_out) & (1 << connection->bit_out))
            *(connection->port_in) |= (1 << connection->bit_in);
        else
            *(connection->port_in) &= ~(1 << connection->bit_in);
    }
    clock_gettime(CLOCK_MONOTONIC, &start);
    
    i = 0;
    //for (i=0; i<context->n_mcus; i++)
    {
        microcontroller_t* microcontroller = context->mcus[i];
        if (context->debugging_enabled) // If debugging is enabled
        {                                                               
            
            
            if (microcontroller->debugger->breakpoints[*(microcontroller->registers->PC)]) // If we are at a firmware breakpoint
            {
                // If we have not tripped this breakpoint yet
                if (microcontroller->debugger->cur_breakpoint != *(microcontroller->registers->PC))
                {
                    // This notice statement causes segfault, not sure why...
                    // Should fix that...
                    notice(microcontroller, "Firmware breakpoint triggered at PC=0x%X", *(microcontroller->registers->PC) * 2);
                    
                    microcontroller->debugger->cur_breakpoint = *(microcontroller->registers->PC);
                    context->state = STATE_INDEBUG;
                    
                    n_breakpoints++;
                }
                else
                {
                    microcontroller->debugger->cur_breakpoint = -1;
                }
            }
            
            
            for (j=0; j<microcontroller->debugger->n_hardware_breakpoints; j++)
            {
                hardware_breakpoint_t* breakpoint = microcontroller->debugger->hardware_breakpoints[j];
                
                if (breakpoint->active && !breakpoint->cur_breakpoint)
                {
		            if (hardware_breakpoint_condition_met(breakpoint))
		            {
		            	n_breakpoints++;
		            	breakpoint->cur_breakpoint = 1;
		            }
                }
                
                else if (breakpoint->cur_breakpoint)
		        {
		        	breakpoint->cur_breakpoint = 0;
		        }
            }
            
            if (n_breakpoints > 0)
            {
            	context->state = STATE_INDEBUG;
                return 1;
            }
        }
        
        uint16_t instruction = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);
        if (*opcodes[instruction] == 0)
        {
            printf("Microcontroller %i trying to execute 0; PC=0x%X\n", i, *(microcontroller->registers->PC) * 2);
            return 1;
        }
        (*opcodes[instruction])(microcontroller);
        
        handle_timers_pre(microcontroller);
        
        (*(microcontroller->handle_adc))(microcontroller);

        handle_interrupts(microcontroller);
        
        handle_timers_post(microcontroller);
        
        for (j=0; j<context->mcus[i]->vcd_vars_count; j++)
        {
            vcd_change(context->mcus[i]->vcd_vars[j], context->mcus[i]->ticks);
        }
        
        context->mcus[i]->prev_ticks = context->mcus[i]->ticks;
        
        
        //for (j=0; j<5; j++)
        //{
        //    clock_gettime(CLOCK_MONOTONIC, &end);
        //}
        
        
        
        clock_gettime(CLOCK_MONOTONIC, &end);
        
        delta_time = (BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec);
        
        context->mcus[i]->tick_time += delta_time;
        
        
        //if (microcontroller->tick_time / (microcontroller->ticks + 1) < target)
        //    sleep_amount += 1000;
        //else if (microcontroller->tick_time / (microcontroller->ticks + 1) > target)
        //    sleep_amount -= 1000;
        //printf("%u\n", sleep_amount);
        
        // 0: 52ns
        // 500: 53ns
        // 1000: 54ns
        // 10000: 63ns
        // 100000: 101ns
        
        //for (j=0; j<sleep_amount; j++)
        //{
        //    asm("nop");
        //}
        
        
        
    }
    
    
    
    return 0;
}

void save_vcd(context_t* context, char* filepath)
{
    FILE* fp = fopen(filepath, "w");
    
    if (fp != 0)
    {
        int vars_count = 0;
        fprintf(fp, "$timescale 63ns $end\n");
        
        int i = 0;
        for (i; i<context->n_mcus; i++)
        {
            if (context->mcus[i]->vcd_vars_count > 0)
            {
                fprintf(fp, "$scope module mcu%i $end\n", i);
                int j = 0;
                for (j; j<context->mcus[i]->vcd_vars_count; j++)
                {
                    vars_count++;
                    fprintf(fp, "$var wire 1 %c %s $end\n", context->mcus[i]->vcd_vars[j]->identifier, context->mcus[i]->vcd_vars[j]->name);
                }
                fprintf(fp, "$upscope $end\n");
            }
        }
        
        fprintf(fp, "$enddefinitions $end\n");
        
        for (i=0; i<context->n_mcus; i++)
        {
            if (context->mcus[i]->vcd_vars_count > 0)
            {
                fprintf(fp, "$dumpvars\n");
                int j = 0;
                for (j; j<context->mcus[i]->vcd_vars_count; j++)
                {
                    fprintf(fp, "%i%c\n", context->mcus[i]->vcd_vars[j]->change_vals[0], context->mcus[i]->vcd_vars[j]->identifier);
                }
                fprintf(fp, "$end\n");
            }
        }
        
        for (i=0; i<context->n_mcus; i++)
        {
            uint64_t cur_tick = 0;
            uint64_t next_closest_tick = 0xFFFFFFFFFFFFFFFF;
            uint64_t difference = 0;
            
            while(cur_tick != 0xFFFFFFFFFFFFFFFF)
            {
                int j = 0;
                for (j; j<context->mcus[i]->vcd_vars_count; j++)
                {
                    int k = 1;
                    for (k; k<context->mcus[i]->vcd_vars[j]->change_count; k++)
                    {
                        if (context->mcus[i]->vcd_vars[j]->change_ticks[k] > cur_tick)
                        {
                            if (context->mcus[i]->vcd_vars[j]->change_ticks[k] < next_closest_tick)
                                next_closest_tick = context->mcus[i]->vcd_vars[j]->change_ticks[k];
                        }
                    }
                }
                
                cur_tick = next_closest_tick;
                next_closest_tick = 0xFFFFFFFFFFFFFFFF;
                
                for (j=0; j<context->mcus[i]->vcd_vars_count; j++)
                {
                    int k = 1;
                    for (k; k<context->mcus[i]->vcd_vars[j]->change_count; k++)
                    {
                        if (context->mcus[i]->vcd_vars[j]->change_ticks[k] == cur_tick)
                        {
                            fprintf(fp, "#%lu\n", context->mcus[i]->vcd_vars[j]->change_ticks[k]);
                            fprintf(fp, "%i%c\n", context->mcus[i]->vcd_vars[j]->change_vals[k], context->mcus[i]->vcd_vars[j]->identifier);                
                        }
                    }
                }
            }
        }
    
        fclose(fp);
        
        for (i=0; i<context->n_mcus; i++)
        {
            notice(context->mcus[i], "VCD: Wrote %i variables to %s", vars_count, filepath);
        }
    }
    
    else
    {
        int i = 0;
        for (i; i<context->n_mcus; i++)
        {
            notice(context->mcus[i], "VCD: Invalid filepath");
        }
    }
}

void add_vcd_variable(context_t* context, uint8_t mcu, uint8_t* byte, char* register_name, uint8_t bit, char* name)
{
    vcd_variable_t* vcd = init_vcd_variable(name, context->vcd_vars_count+33, register_name, byte, bit);
    context->mcus[mcu]->vcd_vars[context->mcus[mcu]->vcd_vars_count] = vcd;
    context->mcus[mcu]->vcd_vars_count++;
    context->vcd_vars_count++;
}
