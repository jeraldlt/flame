/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_registers.c

*/


#include "gui/dock_registers.h"
#include "gui/window_change_register.h"
#include "gui/window_hardware_breakpoint.h"

#include "globals.h"
#include "context.h"

#include <stdlib.h>
#include <stdio.h>

context_t* context;

void launch_change_register_window(GtkWidget* menu_item, window_registers_t* window)
{
    window_change_register_create_register_window(window->selected_register, window->selected_register_name);
}

void launch_breakpoint_window(GtkWidget* menu_item, window_registers_t* window)
{
    window_hardware_breakpoint_create_register_window(window->selected_register, window->selected_register_name, window->microcontroller, window->treestore_data, window->selected_register_path);
}

gboolean treeview_button_pressed(GtkWidget* tree_view, GdkEventButton* event, window_registers_t* window)
{
    if (event->type == GDK_BUTTON_PRESS  &&  event->button == 3)
    {
        GtkWidget* menu;
        GtkWidget* item_change_register;
        GtkWidget* item_breakpoint;
        GtkWidget* menu_dialog;
        GtkWidget* item_dialog;
        GtkWidget* item_dialog_splitv;
        GtkWidget* item_dialog_splith;
        
        // Create main menu
        menu = gtk_menu_new();
        item_change_register = gtk_menu_item_new_with_label("Change Register");
        gtk_widget_set_sensitive(item_change_register, FALSE);
        item_breakpoint = gtk_menu_item_new_with_label("Hardware Breakpoint");
        gtk_widget_set_sensitive(item_breakpoint, FALSE);
        
        // Create dialog submenu
        menu_dialog = gtk_menu_new();
        item_dialog = gtk_menu_item_new_with_label("Dialog");
        item_dialog_splitv = gtk_menu_item_new_with_label("Split Vertically");
        item_dialog_splith = gtk_menu_item_new_with_label("Split Horizontally");
        
        // Connect signals
        g_signal_connect(item_change_register, "activate", G_CALLBACK (launch_change_register_window), window);
        g_signal_connect(item_breakpoint, "activate", G_CALLBACK (launch_breakpoint_window), window);

        // Construct dialog submenu
        gtk_menu_shell_append(GTK_MENU_SHELL (menu_dialog), item_dialog_splitv);
        gtk_menu_shell_append(GTK_MENU_SHELL (menu_dialog), item_dialog_splith);
        gtk_menu_item_set_submenu(GTK_MENU_ITEM (item_dialog), menu_dialog);

        // Construct main menu
        gtk_menu_shell_append(GTK_MENU_SHELL (menu), item_change_register);
        gtk_menu_shell_append(GTK_MENU_SHELL (menu), item_breakpoint);
        gtk_menu_shell_append(GTK_MENU_SHELL (menu), gtk_separator_menu_item_new());
        gtk_menu_shell_append(GTK_MENU_SHELL (menu), item_dialog);
        
        // Check to see if right click was on a register tree item
        GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view));
        if (gtk_tree_selection_count_selected_rows(selection) <= 1)
        {
            GtkTreePath* tree_path;
            GtkTreeModel* model = GTK_TREE_MODEL (window->treestore_data);//gtk_tree_view_get_model(tree_view);
            GtkTreeIter tree_iter; 

            // Get tree path for row that was clicked
            if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(tree_view),
                                              event->x, event->y,
                                              &tree_path, NULL, NULL, NULL))
            {
               
                if (gtk_tree_model_get_iter(model, &tree_iter, tree_path))
                {
                    gchar *name;
                    gtk_tree_model_get(model, &tree_iter, 0, &name, -1);
                   
                    uint8_t* reg = get_register(window->microcontroller->registers, (char*) name);
                    if (reg)
                    {
                        window->selected_register = reg;
                        window->selected_register_name = name;
                        window->selected_register_path = tree_path;
                        
                        gtk_widget_set_sensitive(item_change_register, TRUE);
                        gtk_widget_set_sensitive(item_breakpoint, TRUE);
                    }
                }
            }
        }
        
        gtk_widget_show_all(menu);
        gtk_menu_popup_at_pointer(GTK_MENU(menu), NULL);
        
        return TRUE;
    }
    
    else if (event->type == GDK_BUTTON_PRESS  &&  event->button == 1)
    {
        GtkTreeSelection* selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(tree_view));
        
        if (gtk_tree_selection_count_selected_rows(selection) <= 1)
        {
            GtkTreePath* tree_path;
            GtkTreeModel* model = GTK_TREE_MODEL (window->treestore_data);//gtk_tree_view_get_model(tree_view);
            GtkTreeIter tree_iter; 
            GtkTreeViewColumn* column = gtk_tree_view_column_new();

            /* Get tree path for row that was clicked */
            if (gtk_tree_view_get_path_at_pos(GTK_TREE_VIEW(tree_view),
                                              event->x, event->y,
                                              &tree_path, &column, NULL, NULL))
            {
               
                if (gtk_tree_model_get_iter(model, &tree_iter, tree_path))
                {
                    gchar *name;
                    gtk_tree_model_get(model, &tree_iter, 0, &name, -1);
                    
                    uint8_t* reg = get_register(window->microcontroller->registers, (char*) name);
                    if (reg)
                    {
                        if (column == gtk_tree_view_get_column(GTK_TREE_VIEW(tree_view), 3))
                        {
                            
                            char* bin = "";
                            char* hex = "";
                            char* comparison = "";
                            GdkPixbuf* pixbuf = NULL;
                            gtk_tree_model_get(model, &tree_iter, 1, &bin, 2, &hex, 3, &pixbuf, 4, &comparison, -1);
                            
                            if (pixbuf != NULL)
                            {
                                int i = 0;
                                for (i; i<window->microcontroller->debugger->n_hardware_breakpoints; i++)
                                {
                                    if (reg == window->microcontroller->debugger->hardware_breakpoints[i]->reg)
                                    {
                                        if (window->microcontroller->debugger->hardware_breakpoints[i]->active)
                                        {
                                            GtkImage* icon = gtk_image_new_from_file("../data/icon_bp_inactive.png");
                                            GdkPixbuf* pixbuf = gtk_image_get_pixbuf(icon);
                                            
                                            gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter, 0, name, 1, bin, 2, hex, 3, pixbuf, 4, comparison, -1);
                                            window->microcontroller->debugger->hardware_breakpoints[i]->active = 0;
                                        }
                                        else
                                        {
                                            GtkImage* icon = gtk_image_new_from_file("../data/icon_bp_active.png");
                                            GdkPixbuf* pixbuf = gtk_image_get_pixbuf(icon);
                                            
                                            gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter, 0, name, 1, bin, 2, hex, 3, pixbuf, 4, comparison, -1);
                                            window->microcontroller->debugger->hardware_breakpoints[i]->active = 1;
                                        }
                                    }
                                }
                            }                                                        
                        }
                    }
                }
            }
        }
    }
    
    return FALSE;
}

GtkTreeStore* window_registers_populate_tree(window_registers_t* window)
{
    GtkTreeStore *treestore_data;
    GtkListStore *liststore_data;
    GtkTreeIter tree_iter;
    GtkTreeIter tree_iter_child;
    GtkTreePath* tree_path;
    
    treestore_data = gtk_tree_store_new(5, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, GDK_TYPE_PIXBUF, G_TYPE_STRING);
    GTK_IS_TREE_STORE(&treestore_data);
    //printf("%i\n", GTK_IS_TREE_STORE(&treestore_data));
    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "I/O B", 1, "", 2, "", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "DDRB", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PORTB", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PINB", 1, "00000000", 2, "0x00", -1);
    
    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "I/O C", 1, "", 2, "", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "DDRC", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PORTC", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PINC", 1, "00000000", 2, "0x00", -1);
    
    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "I/O D", 1, "", 2, "", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "DDRD", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PORTD", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PIND", 1, "00000000", 2, "0x00", -1);
    
    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "PC Interrupt", 1, "", 2, "", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PCICR", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PCIFR", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PCMSK0", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PCMSK1", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "PCMSK2", 1, "00000000", 2, "0x00", -1);
    
    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "Timer 0", 1, "", 2, "", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCNT0", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCCR0A", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCCR0B", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR0A", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR0B", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TIMSK0", 1, "00000000", 2, "0x00", -1);
    
    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "Timer 1", 1, "", 2, "", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCNT1[H]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCNT1[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCCR1A", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCCR1B", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCCR1C", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR1A[H]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR1A[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR1B[H]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR1B[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TIMSK1", 1, "00000000", 2, "0x00", -1);
    
    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "Timer 3", 1, "", 2, "", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCNT3[H]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCNT3[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCCR3A", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCCR3B", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TCCR3C", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR3A[H]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR3A[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR3B[H]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "OCR3B[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "TIMSK3", 1, "00000000", 2, "0x00", -1);
    
    
    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    GTK_IS_TREE_STORE(&treestore_data);
    window->tree_path_adc = gtk_tree_model_get_path(GTK_TREE_MODEL(treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "ADC", 1, "", 2, "", -1);
    

    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, window->tree_path_adc);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "ADMUX", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, window->tree_path_adc);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "ADCSRA", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, window->tree_path_adc);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "ADCSRB", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, window->tree_path_adc);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "ADCL", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, window->tree_path_adc);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "ADCH", 1, "00000000", 2, "0x00", -1);

    
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, "General Purpose Registers", 1, "", 2, "", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R0", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R1", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R2", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R3", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R4", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R5", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R6", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R7", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R8", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R9", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R10", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R11", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R12", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R13", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R14", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R15", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R16", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R17", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R18", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R19", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R20", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R21", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R22", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R23", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R24", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R25", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R26-X[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R27-X[H]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R28-Y[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R29-Y[H]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R30-Z[L]", 1, "00000000", 2, "0x00", -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
    gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
    gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, "R31-Z[H]", 1, "00000000", 2, "0x00", -1);
    
    return treestore_data;
}

gboolean window_registers_update_window(window_registers_t* window)
{
    if (window->microcontroller->resetting)
        return TRUE;
        
    GtkTreeIter tree_iter;
    GtkTreeIter tree_iter_child;
    GtkTreePath *tree_path;
    
    microcontroller_t* microcontroller = window->microcontroller;
    char bin[9];
    char hex[8];
    
    gtk_tree_model_get_iter_first(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 0);
    decToBin(*(microcontroller->registers->DDRB), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->DDRB));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "DDRB", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 1);
    decToBin(*(microcontroller->registers->PORTB), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->PORTB));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "PORTB", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 2);
    decToBin(*(microcontroller->registers->PINB), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->PINB));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "PINB", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 0);
    decToBin(*(microcontroller->registers->DDRC), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->DDRC));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "DDRC", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 1);
    decToBin(*(microcontroller->registers->PORTC), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->PORTC));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "PORTC", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 2);
    decToBin(*(microcontroller->registers->PINC), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->PINC));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "PINC", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 0);
    decToBin(*(microcontroller->registers->DDRD), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->DDRD));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "DDRD", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 1);
    decToBin(*(microcontroller->registers->PORTD), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->PORTD));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "PORTD", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 2);
    decToBin(*(microcontroller->registers->PIND), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->PIND));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "PIND", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter); // PC Interrupt
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter); // Timer 0
    
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter); // Timer 1
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 0);
    decToBin(*(microcontroller->registers->TCNT1H), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->TCNT1H));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "TCNT1[H]", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 1);
    decToBin(*(microcontroller->registers->TCNT1L), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->TCNT1L));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "TCNT1[L]", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 2);
    decToBin(*(microcontroller->registers->TCCR1A), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->TCCR1A));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "TCCR1A", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 3);
    decToBin(*(microcontroller->registers->TCCR1B), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->TCCR1B));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "TCCR1B", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 4);
    decToBin(*(microcontroller->registers->TCCR1C), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->TCCR1C));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "TCCR1C", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 5);
    decToBin(*(microcontroller->registers->OCR1AH), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->OCR1AH));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "OCR1A[H]", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 6);
    decToBin(*(microcontroller->registers->OCR1AL), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->OCR1AL));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "OCR1A[L]", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 7);
    decToBin(*(microcontroller->registers->OCR1BH), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->OCR1BH));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "OCR1B[H]", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 8);
    decToBin(*(microcontroller->registers->OCR1BL), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->OCR1BL));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "OCR1B[L]", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 9);
    decToBin(*(microcontroller->registers->TIMSK1), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->TIMSK1));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "TIMSK1", 1, bin, 2, hex, -1);
    
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter); // Timer 3
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter); // ADC
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 0);
    decToBin(*(microcontroller->registers->ADMUX), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->ADMUX));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "ADMUX", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 1);
    decToBin(*(microcontroller->registers->ADCSRA), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->ADCSRA));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "ADCSRA", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 2);
    decToBin(*(microcontroller->registers->ADCSRB), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->ADCSRB));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "ADCSRB", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 3);
    decToBin(*(microcontroller->registers->ADCL), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->ADCL));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "ADCL", 1, bin, 2, hex, -1);
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(window->treestore_data), &tree_iter_child, &tree_iter, 4);
    decToBin(*(microcontroller->registers->ADCH), bin, 8);
    sprintf(hex, "0x%02X", *(microcontroller->registers->ADCH));
    gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, "ADCH", 1, bin, 2, hex, -1);
    
    
    
    
    gtk_tree_model_get_iter(GTK_TREE_MODEL(window->treestore_data), &tree_iter, tree_path);
    gtk_tree_model_iter_next(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL(window->treestore_data), &tree_iter);
    
    int i = 0;
    for (i; i<32; i++)
    {
        gtk_tree_model_get_iter(GTK_TREE_MODEL (window->treestore_data), &tree_iter, tree_path);
        gtk_tree_model_iter_nth_child(GTK_TREE_MODEL (window->treestore_data), &tree_iter_child, &tree_iter, i);
        decToBin(microcontroller->registers->r[i], bin, 8);
        sprintf(hex, "0x%02X", microcontroller->registers->r[i]);
        
        char name[9] = {0};
        if (i == 26)
            strcpy(name, "R26-X[L]");
        else if (i == 27)
            strcpy(name, "R27-X[H]");
        else if (i == 28)
            strcpy(name, "R28-Y[L]");
        else if (i == 29)
            strcpy(name, "R29-Y[H]");
        else if (i == 30)
            strcpy(name, "R30-Z[L]");
        else if (i == 31)
            strcpy(name, "R31-Z[H]");
        else
            sprintf(name, "R%i", i);
        
        gtk_tree_store_set(GTK_TREE_STORE(window->treestore_data), &tree_iter_child, 0, name, 1, bin, 2, hex, -1);
    }
    
    return TRUE;
}

window_registers_t* window_registers_create_registers_window (microcontroller_t* microcontroller)
{
    window_registers_t* window = (window_registers_t*) malloc(sizeof(window_registers_t));
    window->microcontroller = microcontroller;

    window->dock_item = gdl_dock_item_new ("registers", "Registers", GDL_DOCK_ITEM_BEH_CANT_ICONIFY | GDL_DOCK_ITEM_BEH_CANT_CLOSE);
    
    window->treestore_data = window_registers_populate_tree(window);
    
    window->treeview_data = gtk_tree_view_new();
    
    window->renderer = gtk_cell_renderer_text_new ();
    window->pixbuf_renderer = gtk_cell_renderer_pixbuf_new();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeview_data), -1, "Register", window->renderer, "text", 0, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeview_data), -1, "Binary", window->renderer, "text", 1, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeview_data), -1, "Hexadecimal", window->renderer, "text", 2, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeview_data), -1, "", window->pixbuf_renderer,"pixbuf", 3, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeview_data), -1, "", window->renderer, "text", 4, NULL);
    
    gtk_tree_view_set_model(GTK_TREE_VIEW (window->treeview_data), GTK_TREE_MODEL (window->treestore_data));
    //gtk_tree_view_set_model(treeview_data, GTK_TREE_MODEL (liststore_data));
    g_signal_connect(window->treeview_data, "button-press-event", G_CALLBACK (treeview_button_pressed), window);
    
    //window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    //gtk_window_set_default_size (GTK_WINDOW (window->window), 400, 600);
    //gtk_window_set_title(window->window, "Device Registers");
    
    window->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_add (GTK_CONTAINER (window->scrolled_window), GTK_WIDGET (window->treeview_data));
    
    //gtk_container_add (GTK_CONTAINER (window->window), window->scrolled_window);
    gtk_container_add (GTK_CONTAINER (window->dock_item), window->scrolled_window);
    
    g_timeout_add(context->refresh_rate, G_CALLBACK (window_registers_update_window), window);
    
    //gtk_widget_show_all (window->window);
    
    window->dock_item_hidden = 0;
    
    return window;
}
