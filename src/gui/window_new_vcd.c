#include "gui/window_new_vcd.h"

#include <stdint.h>

#include "registers.h"
#include "context.h"
#include "globals.h"

context_t* context;

void window_new_vcd_new_window(microcontroller_t* microcontroller)
{
    new_vcd_window_t* window = (new_vcd_window_t*) malloc(sizeof(new_vcd_window_t));
    window->microcontroller = microcontroller;
    
    window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window->window), 300, 200);
	gtk_window_set_title(GTK_WINDOW (window->window), "New VCD Variable");  
    
    window->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    window->hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    
    window->label_register = gtk_label_new("Register:");
    g_object_set (window->label_register, "xalign", 0.05, NULL);
    
    window->label_bit = gtk_label_new("Bit:");
    g_object_set (window->label_bit, "xalign", 0.05, NULL);
    
    window->label_name = gtk_label_new("Name:");
    g_object_set (window->label_name, "xalign", 0.05, NULL);
    
    window->entry_register = gtk_entry_new();
    window->entry_name = gtk_entry_new();
    
    window->spin_button_bit = gtk_spin_button_new_with_range(0, 7, 1);
    
    window->button_ok = gtk_button_new_with_label("OK");
    g_signal_connect (window->button_ok, "clicked", G_CALLBACK (window_new_vcd_button_ok_pressed), window);
    
    gtk_container_add(GTK_CONTAINER (window->vbox), window->label_register);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->entry_register);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->label_bit);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->spin_button_bit);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->label_name);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->entry_name);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->button_ok);
    
    gtk_container_add(GTK_CONTAINER (window->window), window->vbox);
    gtk_widget_show_all (window->window);
    
    //g_timeout_add(context->refreshRate, G_CALLBACK (window_new_vcd_update_window), window);
}

void window_new_vcd_button_ok_pressed(GtkButton* button, new_vcd_window_t* window)
{
    char* reg_name = g_convert (gtk_entry_get_text(GTK_ENTRY (window->entry_register)), -1, "ISO-8859-1", "UTF-8", NULL, NULL, NULL);
    uint8_t* reg = get_register(window->microcontroller->registers, reg_name);
    uint8_t bit = (uint8_t) gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON (window->spin_button_bit));
    
    add_vcd_variable(context, 0, reg, reg_name, bit, g_convert (gtk_entry_get_text(GTK_ENTRY (window->entry_name)), -1, "ISO-8859-1", "UTF-8", NULL, NULL, NULL));
    
    
    
    gtk_window_close(window->window);
}

gboolean window_new_vcd_update_window(new_vcd_window_t* window)
{
    if(get_register(window->microcontroller->registers, g_convert (gtk_entry_get_text(GTK_ENTRY (window->entry_register)), -1, "ISO-8859-1", "UTF-8", NULL, NULL, NULL)))
    {
        gtk_widget_set_sensitive (window->button_ok, TRUE);
    }
    else
    {
        gtk_widget_set_sensitive (window->button_ok, FALSE);
    }
    return TRUE;
}
