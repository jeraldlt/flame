#include "gui/window_settings.h"

#include <stdint.h>

#include "context.h"
#include "options.h"
#include "globals.h"


context_t* context;

void window_settings_create_settings_window(window_microcontroller_t* parent)
{
    settings_window_t* window = (settings_window_t*) malloc(sizeof(settings_window_t));
    
    window->parent = parent;
    
    window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window->window), 600, 600);
	gtk_window_set_title(GTK_WINDOW (window->window), "Settings");
    g_signal_connect (window->window, "destroy", G_CALLBACK (window_settings_destroy), window);	
	
	window->notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (window->notebook), GTK_POS_LEFT);
    
    
    window_settings_populate_gui_tab(window);
    
    window_settings_populate_plugins_tab(window);
    
    window->scrolled_window_context = gtk_scrolled_window_new (NULL, NULL);
    window->scrolled_window_microcontroller = gtk_scrolled_window_new (NULL, NULL);
    
    
    
    gtk_notebook_append_page(GTK_NOTEBOOK (window->notebook), GTK_WIDGET (window->scrolled_window_gui), gtk_label_new("GUI"));
    gtk_notebook_append_page(GTK_NOTEBOOK (window->notebook), GTK_WIDGET (window->scrolled_window_context), gtk_label_new("Context"));
    gtk_notebook_append_page(GTK_NOTEBOOK (window->notebook), GTK_WIDGET (window->scrolled_window_microcontroller), gtk_label_new("Microcontroller"));
    gtk_notebook_append_page(GTK_NOTEBOOK (window->notebook), GTK_WIDGET (window->scrolled_window_plugins), gtk_label_new("Plugins"));
    
    gtk_container_add (GTK_CONTAINER (window->window), window->notebook);
    
    gtk_widget_show_all (window->window);
}

void window_settings_populate_gui_tab(settings_window_t* window)
{
    window->scrolled_window_gui = gtk_scrolled_window_new (NULL, NULL);
    
    GtkWidget* tmp_label;
    
    window->vbox_gui = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_container_add(GTK_CONTAINER (window->scrolled_window_gui), window->vbox_gui);

    tmp_label = gtk_label_new("Gutter Display Addressing Mode");
    g_object_set (tmp_label, "xalign", 0.01, NULL);
    gtk_container_add (GTK_CONTAINER (window->vbox_gui), tmp_label);
    window->check_button_byte_addressing = gtk_check_button_new_with_label("Byte Addressing");
    gtk_widget_set_tooltip_text(window->check_button_byte_addressing, "Display memory locations using byte or word addressing");
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON (window->check_button_byte_addressing), context->options->gui_byte_addressing);
    g_signal_connect (window->check_button_byte_addressing, "toggled",  G_CALLBACK (window_settings_check_button_byte_addressing_toggled), NULL);
    g_object_set (window->check_button_byte_addressing, "xalign", 0.1, NULL);
    gtk_container_add (GTK_CONTAINER (window->vbox_gui), window->check_button_byte_addressing);
    
    tmp_label = gtk_label_new("Theme");
    g_object_set (tmp_label, "xalign", 0.01, NULL);
    gtk_container_add (GTK_CONTAINER (window->vbox_gui), tmp_label);
    window->combo_theme = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_theme), "System Default");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_theme), "Dark");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_theme), "Light");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_theme), "Dark Mono");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_theme), "Light Mono");
    gtk_combo_box_set_active(GTK_COMBO_BOX (window->combo_theme), context->options->gui_theme);
    gtk_container_add(GTK_CONTAINER (window->vbox_gui), window->combo_theme);
}

void window_settings_populate_plugins_tab(settings_window_t* window)
{
    window->scrolled_window_plugins = gtk_scrolled_window_new (NULL, NULL);
    
    GtkWidget* vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 15);
    gtk_widget_set_margin_start(vbox, 10);
    
    int i = 0;
    for (i; i<window->parent->microcontroller->plugin_manager->n_plugins; i++)
    {
        plugin_container_t* container = window->parent->microcontroller->plugin_manager->plugins[i];
        plugin_t* plugin = container->plugin;
        
        GtkWidget* vbox2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
        
        GtkWidget* hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
        
        GtkWidget* hbox2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
        
        GtkWidget* check_button = gtk_check_button_new();
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON (check_button), container->is_registered);
        g_signal_connect(check_button, "toggled", G_CALLBACK (window_settings_check_button_plugin_toggled), container);
        
        char* markup1 = g_markup_printf_escaped ("<span size=\"large\">\%s - %s</span>", plugin->name, plugin->version);
        GtkWidget* label1 = gtk_label_new(NULL);
        gtk_label_set_markup(GTK_LABEL (label1), markup1);

        char* markup2 = g_markup_printf_escaped ("<span size=\"small\">\%s</span>", plugin->description);
        GtkWidget* label2 = gtk_label_new(NULL);
        gtk_label_set_markup(GTK_LABEL (label2), markup2);
        gtk_label_set_line_wrap(GTK_LABEL (label2), TRUE);
        
        GtkWidget* button = gtk_button_new_with_label("About");
        g_signal_connect(button, "pressed", G_CALLBACK (window_settings_about_button_plugin_pressed), plugin);
        
        gtk_container_add(GTK_CONTAINER (hbox), check_button);
        
        gtk_container_add(GTK_CONTAINER (hbox2), label1);
        gtk_container_add(GTK_CONTAINER (hbox2), button);
        
        gtk_container_add(GTK_CONTAINER (vbox2), hbox2);
        
        gtk_container_add(GTK_CONTAINER (vbox2), label2);
        
        gtk_container_add(GTK_CONTAINER (hbox), vbox2);
        
        gtk_container_add(GTK_CONTAINER (vbox), hbox);
        
        
        
    }
    
    
    gtk_container_add(GTK_CONTAINER (window->scrolled_window_plugins), vbox);
    
    
}

void window_settings_destroy(GtkWidget* object, settings_window_t* window)
{
    save_config(context->config_path);
    
    plugin_manager_t* manager = window->parent->microcontroller->plugin_manager;
    
    int i = 0;
    for (i; i<manager->n_plugins; i++)
    {
        save_plugin_config(manager->plugins[i]->config_path, manager->plugins[i]);
    }
}

void window_settings_check_button_plugin_toggled(GtkToggleButton* button, plugin_container_t* container)
{
    if (container->is_registered)
    {
        container->plugin->on_unregister(container->plugin);
        container->is_registered = 0;
        gtk_widget_hide(container->button);
        printf("Unregistering plugin %s\n", container->plugin->name);
    }
    
    else
    {
        container->plugin->on_register(container->plugin);
        container->is_registered = 1;
        gtk_widget_show(container->button);
        printf("Registering plugin %s\n", container->plugin->name);
    }
}

void window_settings_about_button_plugin_pressed(GtkButton* button, plugin_t* plugin)
{
    gtk_show_about_dialog (NULL,
                           "program-name", plugin->name,
                           "version", plugin->version,
                           "copyright", plugin->author,
                           "website", plugin->website,
                           NULL);
}

void window_settings_check_button_byte_addressing_toggled(GtkToggleButton* button)
{
    context->options->gui_byte_addressing = (uint8_t) gtk_toggle_button_get_active(button);
    if (context->options->gui_byte_addressing)
        gtk_button_set_label(GTK_BUTTON (button), "Byte Addressing");
    else
        gtk_button_set_label(GTK_BUTTON (button), "Word Addressing");
}

void window_settings_combo_theme_changed(GtkComboBox* combo)
{
    ;//g_object_set(gtk_settings_get_default(), "gtk_theme_name", "Adwaita-light", NULL);;
}
