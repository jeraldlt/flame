/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_change_register.c

*/
 


#include "gui/window_change_register.h"

#include "registers.h"

#include "globals.h"
#include "util.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

context_t* context;

gboolean window_change_register_update_change_register_window(window_change_register_t* window)
{
	char hex[8];
	sprintf(hex, "0x%04X", *(window->reg));
	gtk_entry_set_text(GTK_ENTRY (window->entry_reg_cur), hex);	

	return TRUE;
}

void window_change_register_button_ok_pressed(GtkButton* button, window_change_register_t* window)
{
	uint8_t val = (uint8_t) strtol(gtk_entry_get_text(GTK_ENTRY (window->entry_reg_new)), 0, 16);
	*(window->reg) = val;
	gtk_window_close(GTK_WINDOW (window->window));
}

window_change_register_t* window_change_register_create_register_window(uint8_t* reg, char* reg_name)
{
	window_change_register_t* window = (window_change_register_t*) malloc(sizeof(window_change_register_t));
	window->reg = reg;

	window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window->window), 300, 100);
	char title[64];
	sprintf(title, "Change Register %s", reg_name);
	gtk_window_set_title(GTK_WINDOW (window->window), title);

	window->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);	

	window->label_reg_cur = gtk_label_new("Register Current:");
	g_object_set (window->label_reg_cur, "xalign", 0.05, NULL);
	
	window->label_reg_new = gtk_label_new("Register New:");
	g_object_set (window->label_reg_new, "xalign", 0.05, NULL);
	
	window->entry_reg_cur = gtk_entry_new();
	char hex[8];
	sprintf(hex, "0x%04X", *reg);
	gtk_entry_set_text(GTK_ENTRY (window->entry_reg_cur), hex);
	gtk_widget_set_sensitive (window->entry_reg_cur, FALSE);
	
	window->entry_reg_new = gtk_entry_new();
	gtk_entry_set_placeholder_text (GTK_ENTRY (window->entry_reg_new), "New Register Value");
	gtk_entry_set_text(GTK_ENTRY (window->entry_reg_new), hex);
	
	window->button_ok = gtk_button_new_with_label("OK");
	g_signal_connect (window->button_ok, "clicked", G_CALLBACK (window_change_register_button_ok_pressed), window);
	
	gtk_container_add(GTK_CONTAINER (window->vbox), window->label_reg_cur);
	gtk_container_add(GTK_CONTAINER (window->vbox), window->entry_reg_cur);
	gtk_container_add(GTK_CONTAINER (window->vbox), window->label_reg_new);
	gtk_container_add(GTK_CONTAINER (window->vbox), window->entry_reg_new);
	gtk_container_add(GTK_CONTAINER (window->vbox), window->button_ok);
	
	gtk_container_add(GTK_CONTAINER (window->window), window->vbox);
	
	int width, height;
    	gtk_window_get_size(window->window, &width, &height);
    
   	width = (gdk_screen_width() - width) / 2;
 	height = (gdk_screen_height() - height) / 2;
    
  	gtk_window_move(window->window, width, height);
	
	gtk_widget_show_all (window->window);  
	
	//g_timeout_add(context->refreshRate, G_CALLBACK (window_change_register_update_change_register_window), window);

	//gtk_widget_show_all (window->window);
	
	return window;
}
