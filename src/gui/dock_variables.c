/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_variables.h

*/

#include "gui/dock_variables.h"
#include "globals.h"

context_t* context;

dock_variables_t* dock_variables_create_dock(microcontroller_t* microcontroller)
{
	dock_variables_t* dock = (dock_variables_t*) malloc(sizeof(dock_variables_t));
	dock->microcontroller = microcontroller;
	
	dock->dock_item = gdl_dock_item_new("variables", "Variables", GDL_DOCK_ITEM_BEH_CANT_ICONIFY | GDL_DOCK_ITEM_BEH_CANT_CLOSE);
	
	dock->treestore_data = dock_variables_populate_tree(dock->microcontroller);
    
    dock->treeview_data = gtk_tree_view_new();
    
    dock->renderer = gtk_cell_renderer_text_new ();
    dock->pixbuf_renderer = gtk_cell_renderer_pixbuf_new();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "Name", dock->renderer, "text", 0, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "Type", dock->renderer, "text", 1, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "Value", dock->renderer, "text", 2, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "Hexadecimal", dock->renderer, "text", 3, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "", dock->pixbuf_renderer,"pixbuf", 4, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "", dock->renderer, "text", 5, NULL);
    
    gtk_tree_view_set_model(GTK_TREE_VIEW (dock->treeview_data), GTK_TREE_MODEL (dock->treestore_data));
    
    dock->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    gtk_container_add (GTK_CONTAINER (dock->scrolled_window), GTK_WIDGET (dock->treeview_data));
    
    //gtk_container_add (GTK_CONTAINER (dock->dock), dock->scrolled_window);
    gtk_container_add (GTK_CONTAINER (dock->dock_item), dock->scrolled_window);
    
    g_timeout_add(context->refresh_rate, G_CALLBACK (dock_variables_update_dock), dock);
    
    //gtk_widget_show_all (dock->dock);
    
    dock->dock_item_hidden = 0;
    
    return dock;
}

gboolean dock_variables_update_dock(dock_variables_t* dock)
{
	if (dock->microcontroller->resetting)
	    return TRUE;
	    
	debugger_t* debugger = dock->microcontroller->debugger;
	
	GtkTreeIter tree_iter;
    GtkTreeIter tree_iter_child;
    GtkTreePath *tree_path;
	
	int i = 0;
	int j = 0;
	
	gtk_tree_model_get_iter_first(GTK_TREE_MODEL(dock->treestore_data), &tree_iter);
    tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL(dock->treestore_data), &tree_iter);
	
	for (i; i<debugger->n_sources; i++)
	{
		debug_source_t* source = dock->microcontroller->debugger->sources[i];
		
		
		
		if (source->n_variables > 0)
		{
            gtk_tree_model_iter_next(GTK_TREE_MODEL(dock->treestore_data), &tree_iter);
		}
		
		
		for (j=0; j<source->n_variables; j++)
		{
			debug_variable_t* variable = source->variables[j];
			
			gtk_tree_model_get_iter(GTK_TREE_MODEL(dock->treestore_data), &tree_iter, tree_path);
            gtk_tree_model_iter_nth_child(GTK_TREE_MODEL(dock->treestore_data), &tree_iter_child, &tree_iter, j);
            gtk_tree_store_set(GTK_TREE_STORE(dock->treestore_data), &tree_iter_child, 0, variable->name, 1, debug_variable_type_name(variable), 2, debug_variable_value_string(dock->microcontroller, variable), 3, debug_variable_value_hex(dock->microcontroller, variable), -1);
				
			
		}
	}
	
	
	
	for (i; i<debugger->n_sources; i++)
	{
	    debug_source_t* source = debugger->sources[i];
	    for (j=0; j<source->n_variables; j++)
	    {
	        debug_variable_t* variable = source->variables[j];
	        char* val = debug_variable_value_string(dock->microcontroller, variable);
	        //printf("%s val: %s\n", variable->name, val);
	    }
	}
	
	return TRUE;
}

GtkTreeStore* dock_variables_populate_tree(microcontroller_t* microcontroller)
{
	GtkTreeStore *treestore_data;
    GtkListStore *liststore_data;
    GtkTreeIter tree_iter;
    GtkTreeIter tree_iter_child;
    GtkTreeIter tree_iter_grandchild;
    GtkTreePath* tree_path;
    
    int i = 0;
	int j = 0;
	int k = 0;
	
    treestore_data = gtk_tree_store_new(6, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, GDK_TYPE_PIXBUF, G_TYPE_STRING);
    GTK_IS_TREE_STORE(&treestore_data);
    
    for (i; i<microcontroller->debugger->n_sources; i++)
	{
		debug_source_t* source = microcontroller->debugger->sources[i];
		
		if (source->n_variables > 0)
		{
			gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter, NULL);
			tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter);
			gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter, 0, source->name, 1, "", 2, "", -1);
		}
		
		
		for (j=0; j<source->n_variables; j++)
		{
			debug_variable_t* variable = source->variables[j];
				
			gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
			gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
			gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, variable->name, 1, debug_variable_type_name(variable), 2, "0", 3, "0x00", -1);
		}
		
		
		/*
		for (j=0; j<source->n_subprograms; j++)
		{
			debug_subprogram_t* subprogram = source->subprograms[j];
			
			if (subprogram->n_variables > 0)
			{
				gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
				gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
				tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (treestore_data), &tree_iter_child);
				gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, subprogram->name, 1, "func", 2, "0", 3, "0x00", -1);
				
				for (k=0; k<subprogram->n_variables; k++)
				{
					debug_variable_t* variable = subprogram->variables[k];
					
					gtk_tree_model_get_iter(GTK_TREE_MODEL (treestore_data), &tree_iter, tree_path);
					gtk_tree_store_append(GTK_TREE_STORE (treestore_data), &tree_iter_child, &tree_iter);
					gtk_tree_store_set(GTK_TREE_STORE (treestore_data), &tree_iter_child, 0, variable->name, 1, debug_variable_type_name(variable), 2, "0", 3, "0x00", -1);
				}
			}
			
		}
		*/
		
	}
	
	return treestore_data;
}

gboolean dock_variables_treeview_button_pressed(GtkWidget* view, GdkEventButton* event, dock_variables_t* dock)
{
	return FALSE;
}
