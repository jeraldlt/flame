#include "gui/window_new_layout.h"

#include <stdint.h>

#include "context.h"
#include "globals.h"

context_t* context;

void window_new_layout_new_window(window_microcontroller_t* parent)
{
    new_layout_window_t* window = (new_layout_window_t*) malloc(sizeof(new_layout_window_t));
    window->parent = parent;
    
    window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window->window), 300, 100);
	gtk_window_set_title(GTK_WINDOW (window->window), "New Layout");  
    
    window->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    window->hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    
    window->label_name = gtk_label_new("Name:");
    g_object_set (window->label_name, "xalign", 0.05, NULL);
    
    window->entry_name = gtk_entry_new();
    
    window->button_ok = gtk_button_new_with_label("OK");
    g_signal_connect (window->button_ok, "clicked", G_CALLBACK (window_new_layout_button_ok_pressed), window);
    
    gtk_container_add(GTK_CONTAINER (window->vbox), window->label_name);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->entry_name);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->button_ok);
    
    gtk_container_add(GTK_CONTAINER (window->window), window->vbox);
    gtk_widget_show_all (window->window);
    
}

void window_new_layout_button_ok_pressed(GtkButton* button, new_layout_window_t* window)
{
    GtkWidget* menu_item = gtk_radio_menu_item_new_with_label (window->parent->group, "Test");
    window->parent->group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (menu_item));
    gtk_menu_shell_append(GTK_MENU_SHELL(window->parent->menu_layout), menu_item);


    gtk_window_close(window->window);
}
