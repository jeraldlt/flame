/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_new_microcontroller.c

*/

#include "gui/window_new_microcontroller.h"

#include <stdio.h>
#include <libgen.h>

#include "gui/window_microcontroller.h"

#include "globals.h"
#include "context.h"
#include "microcontroller.h"

context_t* context;

void window_new_microcontroller_button_ok_pressed(GtkButton* button, new_microcontroller_window_t* window)
{
    uint8_t clock = 0;
    char* clock_text = gtk_combo_box_text_get_active_text(window->combo_clock);
    char* model_text = gtk_combo_box_text_get_active_text(window->combo_model);
    
    if (strcmp(clock_text, "8 MHz") == 0)
        clock = 8;
    else if (strcmp(clock_text, "12 MHz") == 0)
        clock = 12;
    else if (strcmp(clock_text, "16 MHz") == 0)
        clock = 16;
    else if (strcmp(clock_text, "20 MHz") == 0)
        clock = 20;
    
    
    g_signal_handler_disconnect(window->window, window->exit_signal_id);
    gtk_window_close(window->window);
    
    microcontroller_t* microcontroller = add_mcu(context, model_text, clock);
    
    char* filename = g_convert (gtk_entry_get_text(window->entry_program), -1, "ISO-8859-1", "UTF-8", NULL, NULL, NULL);

    
    if (is_elf_file(filename))
    {
        load_elf(microcontroller, filename);
    }
    else if (is_ihex_file(filename))
    {
        load_hex(microcontroller, filename);
    }

    strcpy(context->options->last_program, filename);
    save_config(context->config_path);
    
    create_microcontroller_window(microcontroller);
    
}

void window_new_microcontroller_button_about_pressed(GtkButton* button)
{
    GdkPixbuf *example_logo = gdk_pixbuf_new_from_file ("../data/splash.png", NULL);
    gchar* authors[1] = {"Jerald Thomas"};
    gtk_show_about_dialog (NULL,
                       "program-name", "",
                       "logo", example_logo,
                       //"title", "About ExampleCode",
                       "license-type", GTK_LICENSE_GPL_3_0,
                       "website", "https://www.bitbucket.com/jeraldlt/glaed.git",
                       "copyright", "© 2018 - 2020 Jerald Thomas",
                       "comments", "FlameLabs AVR is an Atmel/Microchip AVR emulator and debugger",
                       NULL);
}


int window_new_microcontroller_check_program(new_microcontroller_window_t* window)
{
    char* program_file = gtk_entry_get_text(GTK_ENTRY (window->entry_program));
        
    if (access(program_file, F_OK) == -1)
    {
        gtk_label_set_text(GTK_LABEL (window->label_program_type), "Program Type: N/A");
        gtk_label_set_text(GTK_LABEL (window->label_program_debug), "Debug Symbols: No");
        gtk_widget_set_sensitive (window->button_ok, FALSE);
        return FALSE;
    }
    
    if (is_elf_file(program_file))
    {
        gtk_label_set_text(GTK_LABEL (window->label_program_type), "Program Type: ELF");
        
        if (is_debug_file(program_file))
            gtk_label_set_text(GTK_LABEL (window->label_program_debug), "Debug Symbols: Yes");
        else
            gtk_label_set_text(GTK_LABEL (window->label_program_debug), "Debug Symbols: No");
            
        gtk_widget_set_sensitive (window->button_ok, TRUE);
        return TRUE;
    }
    
    if (is_ihex_file(program_file))
    {
        gtk_label_set_text(GTK_LABEL (window->label_program_type), "Program Type: IHEX");
        gtk_label_set_text(GTK_LABEL (window->label_program_debug), "Debug Symbols: No");
        gtk_widget_set_sensitive (window->button_ok, TRUE);
        return TRUE;
    }
    
    gtk_label_set_text(GTK_LABEL (window->label_program_type), "Program Type: N/A");
    gtk_label_set_text(GTK_LABEL (window->label_program_debug), "Debug Symbols: No");
    gtk_widget_set_sensitive (window->button_ok, FALSE);
    return FALSE;
}

void window_new_microcontroller_entry_program_icon_pressed(GtkEntry* entry, GtkEntryIconPosition icon_pos, GdkEvent* event, new_microcontroller_window_t* window)
{
    GtkWidget* dialog;
    int res;
    char* filename;
    
    dialog = gtk_file_chooser_dialog_new ("Open File", NULL, GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
    
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER (dialog), dirname(g_convert (gtk_entry_get_text(window->entry_program), -1, "ISO-8859-1", "UTF-8", NULL, NULL, NULL)));
    
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT)
    {
        
        GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
        filename = gtk_file_chooser_get_filename (chooser);
        gtk_entry_set_text(entry, filename);
    }
    
    gtk_widget_destroy (dialog);
    window_new_microcontroller_check_program(window);
}

void window_new_microcontroller_entry_program_changed(GtkEditable* editable, new_microcontroller_window_t* window)
{
    window_new_microcontroller_check_program(window);
}

void window_new_microcontroller_entry_program_activated(GtkEntry* entry, new_microcontroller_window_t* window)
{
    if (window_new_microcontroller_check_program(window))
        window_new_microcontroller_button_ok_pressed(NULL, window);
}

void window_new_microcontroller_new_microcontroller(const char* mcu, const char* clock, const char* file)
{
    
    new_microcontroller_window_t* window = (new_microcontroller_window_t*) malloc(sizeof(new_microcontroller_window_t));
    
    window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window->window), 400, 708);
	gtk_window_set_title(window->window, "New Microcontroller");  
	window->exit_signal_id = g_signal_connect (window->window, "destroy", G_CALLBACK (gtk_main_quit), NULL); 
    
    window->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    window->hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    
    window->combo_model = gtk_combo_box_text_new();
    window->combo_clock = gtk_combo_box_text_new();
    
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_model), "atmega328p");
    gtk_combo_box_set_active(window->combo_model, 0);
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_clock), "8 MHz");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_clock), "12 MHz");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_clock), "16 MHz");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_clock), "20 MHz");
    gtk_combo_box_set_active(window->combo_clock, 2);

    window->label_model = gtk_label_new("Model:");
    g_object_set (window->label_model, "xalign", 0.05, NULL);
    
    window->label_clock = gtk_label_new("Clock:");
    g_object_set (window->label_clock, "xalign", 0.05, NULL);
    
    window->label_program = gtk_label_new("Program:");
    g_object_set (window->label_program, "xalign", 0.05, NULL);
    
    window->label_program_type = gtk_label_new("Program Type: N/A");
    g_object_set (window->label_program_type, "xalign", 0.1, NULL);
    
    window->label_program_debug = gtk_label_new("Debug Symbols: No");
    g_object_set (window->label_program_debug, "xalign", 0.1, NULL);
    
    window->entry_program = gtk_entry_new();
    gtk_entry_set_placeholder_text (GTK_ENTRY (window->entry_program), "Program File");
    //gtk_entry_set_text(window->entry_program, "tests/blink_timer.elf");
    gtk_entry_set_text(window->entry_program, context->options->last_program);
    gtk_entry_set_icon_from_icon_name(window->entry_program, GTK_ENTRY_ICON_SECONDARY, "document-open");
    g_signal_connect(window->entry_program, "icon-press", G_CALLBACK (window_new_microcontroller_entry_program_icon_pressed), window);
    g_signal_connect(window->entry_program, "changed", G_CALLBACK (window_new_microcontroller_entry_program_changed), window);
    g_signal_connect(window->entry_program, "activate", G_CALLBACK (window_new_microcontroller_entry_program_activated), window);
    
    window->button_ok = gtk_button_new_with_label("OK");
    g_signal_connect (window->button_ok, "clicked", G_CALLBACK (window_new_microcontroller_button_ok_pressed), window);
    
    
    window->button_about = gtk_button_new_with_label("About");
    g_signal_connect (window->button_about, "clicked", G_CALLBACK (window_new_microcontroller_button_about_pressed), window);
    window->button_help = gtk_button_new_with_label("Help");
    g_object_set (window->button_help, "xalign", 1.0, NULL);
    
    
    gtk_container_add (GTK_CONTAINER (window->hbox), window->button_about);
    gtk_container_add (GTK_CONTAINER (window->hbox), window->button_help);
    
    gtk_container_add (GTK_CONTAINER (window->vbox), window->hbox);
    gtk_container_add (GTK_CONTAINER (window->vbox), gtk_image_new_from_file("../data/splash.png")); 
    gtk_container_add (GTK_CONTAINER (window->vbox), window->label_model);
    gtk_container_add (GTK_CONTAINER (window->vbox), window->combo_model);
    gtk_container_add (GTK_CONTAINER (window->vbox), window->label_clock);
    gtk_container_add (GTK_CONTAINER (window->vbox), window->combo_clock);
    gtk_container_add (GTK_CONTAINER (window->vbox), window->label_program);
    gtk_container_add (GTK_CONTAINER (window->vbox), window->entry_program);
    gtk_container_add (GTK_CONTAINER (window->vbox), window->label_program_type);
    gtk_container_add (GTK_CONTAINER (window->vbox), window->label_program_debug);
    gtk_container_add (GTK_CONTAINER (window->vbox), window->button_ok);
    
    gtk_container_add (GTK_CONTAINER (window->window), window->vbox);
    
    window_new_microcontroller_check_program(window);
    
    
    int width, height;
    gtk_window_get_size(window->window, &width, &height);
    
    width = (gdk_screen_width() - width) / 2;
    height = (gdk_screen_height() - height) / 2;
    
    gtk_window_move(window->window, width, height);

    gtk_widget_show_all (window->window);
    gtk_widget_grab_focus (window->button_ok);  
}
