/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-
 *
 * This file is part of GtkSourceView
 *
 * Copyright (C) 2015 - Université Catholique de Louvain
 *
 * GtkSourceView is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * GtkSourceView is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Sébastien Wilmet
 */
 

#define MARK_SP_CURRENT_STACK  "mark_sp_current_stack"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "gui/dock_stack.h"

#include "registers.h"
#include "globals.h"
#include "util.h"

context_t* context;

gboolean dock_stack_detached_timer(dock_stack_t* dock)
{
    GdlDockObject* object;
    GdlDockObject* tmp_object = gdl_dock_object_get_parent_object(dock->dock_item);
    
    //gdl_dock_object_get_toplevel ()
    while (tmp_object != NULL)
    {
    	object = tmp_object;
    	tmp_object = gdl_dock_object_get_parent_object(object);
    }
    
    gboolean floating = FALSE;
    
    g_object_get(object, "floating", &floating, NULL);
    
    printf("Parent: %i, Dock Parent: %i, Floating: %i\n", object, dock->parent, floating);
    
    
    if (floating)
    {
        gdl_dock_item_unbind(dock->dock_item);
        
        GtkWidget* window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	    gtk_window_set_default_size (GTK_WINDOW (window), 300, 200);
	    gtk_window_set_title(GTK_WINDOW (window), "New Dock");

        GdlDock* new_dock = gdl_dock_new();
        gdl_dock_add_item(new_dock, dock->dock_item, GDL_DOCK_BOTTOM);
	    
	    gtk_container_add(GTK_CONTAINER (window), new_dock);
	    
	    gtk_widget_show_all (window);
    }
    
    return FALSE;
}

void dock_stack_dock_detached(GdlDockObject* item, gboolean recursive, dock_stack_t* dock)
{
    
    //g_timeout_add(5, G_CALLBACK (dock_stack_detached_timer), dock);
    
    
    
    //printf("Stack Dock Detaching. Parent: %i, Self: %i\n", gdl_dock_object_get_parent_object(item), item);
}

void dock_stack_query_line_data(GtkSourceGutterRenderer *renderer, GtkTextIter *start, GtkTextIter *end, GtkSourceGutterRendererState state, dock_stack_t* dock)
{
	// Get line of buffer
	int line = dock->microcontroller->sram->size - gtk_text_iter_get_line(start) - 1;
	
	if (line < 0)
	{
		gtk_source_gutter_renderer_text_set_text(renderer, "", -1);
	}	
	else
	{
		// Write the stack address to the gutter
		char buf[8] = {0};
		sprintf(buf, "%02X", line);
		gtk_source_gutter_renderer_text_set_text(renderer, buf, -1);
	}
}


void dock_stack_add_source_mark_attributes (GtkSourceView *view)
{
	// Create a source mark that is 50% alpha yellow
	// This will be used to indicate where the SP is pointing
	
	GtkSourceMarkAttributes *attrs = gtk_source_mark_attributes_new ();

	GdkRGBA color_yellow;	
	color_yellow.red = 0.5;
	color_yellow.green = 0.5;
	color_yellow.blue = 0.0;
	color_yellow.alpha = 0.5;
	
	gtk_source_mark_attributes_set_background(attrs, &color_yellow);
	gtk_source_view_set_mark_attributes(view, MARK_SP_CURRENT_STACK, attrs, 1);
}

void dock_stack_fill_buffer (dock_stack_t* dock)
{
	GtkTextIter iter;
	GtkTextBuffer* buffer = GTK_TEXT_BUFFER (dock->buffer);

	// Clear buffer
	gtk_text_buffer_set_text (buffer, "", 0);

	gtk_text_buffer_get_start_iter (buffer, &iter);
	
	char buf[32];
	
	uint16_t sp = get_sp(dock->microcontroller->registers);
	GSList *mark_list;
	
	int i = dock->microcontroller->sram->size - 1;
	for (i; i>=0; i--)
	{
		sprintf(buf, "0x%04X (0x%04X)", dock->microcontroller->sram->data[i], dock->microcontroller->sram->data[i] * 2);
		
		if (i)
		{
			gtk_text_buffer_insert(buffer, &iter, buf, -1);
			gtk_text_buffer_insert(buffer, &iter, "\n", -1);
			gtk_text_iter_forward_line (&iter);
		}
		else
		{
			gtk_text_buffer_insert(buffer, &iter, buf, -1);
		}
		
		mark_list = gtk_source_buffer_get_source_marks_at_line (dock->buffer,	gtk_text_iter_get_line (&iter), MARK_SP_CURRENT_STACK);
		
		if (mark_list != NULL && (i - 1) != sp)
		{
			gtk_text_buffer_delete_mark (GTK_TEXT_BUFFER (dock->buffer), GTK_TEXT_MARK (mark_list->data));
		}
		else if (mark_list == NULL && (i - 1) == sp)
		{
			gtk_source_buffer_create_source_mark (dock->buffer, NULL, MARK_SP_CURRENT_STACK, &iter);
		}
	}
	
	g_slist_free (mark_list);
	
}

gboolean dock_stack_update_stack_dock(dock_stack_t* dock)
{
	if (dock->microcontroller->resetting)
	    return TRUE;
	
	uint16_t sp = get_sp(dock->microcontroller->registers);
	if (sp != dock->sp_last)
	{
		dock->sp_last = sp;
		dock_stack_fill_buffer (dock);
	}
	
	return TRUE;
}

dock_stack_t* dock_stack_create_stack_dock (microcontroller_t* microcontroller, GdlDockObject* parent)
{
	dock_stack_t* dock = (dock_stack_t*) malloc(sizeof(dock_stack_t));
	dock->parent = parent;
	dock->microcontroller = microcontroller;
	dock->sp_last = 0;
	
	dock->dock_item = gdl_dock_item_new ("stack", "Device Stack", GDL_DOCK_ITEM_BEH_CANT_ICONIFY | GDL_DOCK_ITEM_BEH_CANT_CLOSE);

	dock->view = GTK_SOURCE_VIEW (gtk_source_view_new ());

	g_object_set (dock->view, "expand", TRUE, NULL);

	gtk_text_view_set_monospace (GTK_TEXT_VIEW (dock->view), TRUE);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (dock->view), FALSE);
	gtk_source_view_set_show_line_marks (dock->view, TRUE);

	dock->buffer = GTK_SOURCE_BUFFER (gtk_text_view_get_buffer (GTK_TEXT_VIEW (dock->view)));

	dock->tag = gtk_source_buffer_create_source_tag (dock->buffer, NULL, "draw-spaces", FALSE, NULL);

	dock->gutter = gtk_source_view_get_gutter(dock->view, GTK_TEXT_WINDOW_LEFT);
	dock_stack_fill_buffer (dock);
	GtkSourceGutterRenderer* gutterRenderer = gtk_source_gutter_renderer_text_new();
	gtk_source_gutter_renderer_set_visible(gutterRenderer, TRUE);
	g_object_set (gutterRenderer, "alignment-mode", GTK_SOURCE_GUTTER_RENDERER_ALIGNMENT_MODE_LAST, "yalign", 0.5, "xalign", 0.5, "xpad", 25, NULL);
	gtk_source_gutter_insert(dock->gutter, gutterRenderer, -1);
	g_signal_connect (gutterRenderer, "query-data",  G_CALLBACK (dock_stack_query_line_data), dock);
	
	dock->manager = gtk_source_language_manager_get_default ();

	
	dock_stack_add_source_mark_attributes (dock->view);
	
	g_signal_connect(dock->dock_item, "dock-drag-end", G_CALLBACK (dock_stack_dock_detached), dock);
	
	
	dock->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (dock->scrolled_window), GTK_WIDGET (dock->view));

	gtk_container_add (GTK_CONTAINER (dock->dock_item), dock->scrolled_window);
	
	g_timeout_add(context->refresh_rate, G_CALLBACK (dock_stack_update_stack_dock), dock);

	dock->dock_item_hidden = 0;
	
	return dock;
}
