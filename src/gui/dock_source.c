/* -*- Mode: C; tab-width: 8; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-
 *
 * This file is part of GtkSourceView
 *
 * Copyright (C) 2015 - Université Catholique de Louvain
 *
 * GtkSourceView is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * GtkSourceView is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Sébastien Wilmet
 */
 
#define MARK_BP_CURRENT_SOURCE      "mark_bp_current_source"
#define MARK_BP_AVAILABLE_SOURCE    "mark_bp_available_source"


#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

#include "gui/dock_source.h"

#include "globals.h"
#include "util.h"

context_t* context;

void window_source_mark_tooltip (GtkSourceMarkAttributes *attributes, GtkSourceMark *mark, gpointer user_data)
{
	;
}

void window_source_query_line_data(GtkSourceGutterRenderer *renderer, GtkTextIter *start, GtkTextIter *end, GtkSourceGutterRendererState state, window_source_t* window)
{
	int address_size = 1;
	if (context->options->gui_byte_addressing)
		address_size = 2;
	
	char buf[8] = {0};
	int page = gtk_notebook_get_current_page(GTK_NOTEBOOK (window->notebook));
	int line = gtk_text_iter_get_line(start);
	int address = window->microcontroller->debugger->sources[page]->bp_addresses[line] / 2;
	GSList *mark_list;
	const gchar *mark_type;
	
	
	if (address != 0)
	{
		sprintf(buf, "%04X:%03i", address * address_size,  line);
		int bp = window->microcontroller->debugger->breakpoints[address];
				
		mark_list = gtk_source_buffer_get_source_marks_at_line (window->buffer[page], gtk_text_iter_get_line (start), MARK_BP_CURRENT_SOURCE);
		if (bp && mark_list == NULL)
		{
			gtk_source_buffer_create_source_mark (window->buffer[page], NULL, MARK_BP_CURRENT_SOURCE, start);
		}
		else if (!bp && mark_list != NULL)
		{
			gtk_text_buffer_delete_mark (GTK_TEXT_BUFFER (window->buffer[page]), GTK_TEXT_MARK (mark_list->data));
		}
		
	}
	else
	{
		sprintf(buf, "%03i", line);
	}
	
	gtk_source_gutter_renderer_text_set_text(renderer, buf, -1);
	//g_slist_free (mark_list);
}

void window_source_load_cb (GtkSourceFileLoader *loader, GAsyncResult *result, int source_index)
{
	GtkTextIter iter;
	GFile *location;
	GtkSourceLanguage *language = NULL;
	GError *error = NULL;
	GtkSourceBuffer* buffer;
	
	buffer = gtk_source_file_loader_get_buffer(loader);

	gtk_source_file_loader_load_finish (loader, result, &error);

	if (error != NULL)
	{
		g_warning ("Error while loading the file: %s", error->message);
		g_clear_error (&error);
		g_object_unref(loader);
		return;
	}

	/* move cursor to the beginning */
	gtk_text_buffer_get_start_iter (GTK_TEXT_BUFFER (buffer), &iter);
	gtk_text_buffer_place_cursor (GTK_TEXT_BUFFER (buffer), &iter);
	window_source_add_empty_bp_marks(buffer, source_index);
	//printf("here\n");

	g_object_unref (loader);
}

void window_source_open_file (GtkSourceBuffer *buffer, const gchar *filename, int source_index)
{
	GFile *location;
	GtkSourceFileLoader *loader;
	GAsyncResult *result;
	GtkSourceFile *file;

	file = gtk_source_file_new ();

	location = g_file_new_for_path (filename);
	gtk_source_file_set_location (file, location);
	g_object_unref (location);

	loader = gtk_source_file_loader_new (buffer, file);

	//remove_all_marks (buffer);

	gtk_source_file_loader_load_async (loader, G_PRIORITY_DEFAULT, NULL, NULL, NULL, NULL, (GAsyncReadyCallback) window_source_load_cb, source_index);
	//gtk_source_file_loader_load_async (loader, G_PRIORITY_DEFAULT, NULL, NULL, NULL, NULL, NULL, NULL);
}

void window_source_line_mark_activated (GtkSourceGutter *gutter, GtkTextIter *iter, GdkEventButton *event, window_source_t* window)
{
	
	if (event->button != 1)
		return;
	
	int line = gtk_text_iter_get_line (iter);
	int page = gtk_notebook_get_current_page(GTK_NOTEBOOK (window->notebook));
	int address = window->microcontroller->debugger->sources[page]->bp_addresses[line];
	if (address != 0)
	{
		window->microcontroller->debugger->breakpoints[address/2] ^= 1;	
	}
}

void window_source_add_source_mark_attributes (GtkSourceView *view)
{
	GdkRGBA color;
	GtkSourceMarkAttributes *attrs;

	attrs = gtk_source_mark_attributes_new ();
	
	color.red = 1.0;
	color.green = 0.0;
	color.blue = 0.0;
	color.alpha = 0.5;

	//gdk_rgba_parse (&color, rgba);
	gtk_source_mark_attributes_set_background (attrs, &color);

	GtkImage* icon_bp_active = gtk_image_new_from_file("../data/icon_bp_active.png");
	GdkPixbuf* pixbuf_bp_active = gtk_image_get_pixbuf(icon_bp_active);
	
	gtk_source_mark_attributes_set_pixbuf(attrs, pixbuf_bp_active);
	//gtk_source_mark_attributes_set_icon_name (attrs, "user-busy");

	gtk_source_view_set_mark_attributes (view, MARK_BP_CURRENT_SOURCE, attrs, 1);
	g_object_unref (attrs);


	attrs = gtk_source_mark_attributes_new ();

	GtkImage* icon_bp_inactive = gtk_image_new_from_file("../data/icon_bp_inactive.png");
	GdkPixbuf* pixbuf_bp_inactive = gtk_image_get_pixbuf(icon_bp_inactive);
	
	gtk_source_mark_attributes_set_pixbuf(attrs, pixbuf_bp_inactive);
	//gtk_source_mark_attributes_set_icon_name (attrs, "user-invisible");

	gtk_source_view_set_mark_attributes (view, MARK_BP_AVAILABLE_SOURCE, attrs, 2);
	g_object_unref (attrs);
}

void window_source_add_empty_bp_marks (GtkSourceBuffer *buffer, int source_index)
{
	GtkTextIter iter;
	
	int i = 0;
	for (i; i<context->mcus[0]->debugger->sources[source_index]->n_lines; i++)
	{
	    if (context->mcus[0]->debugger->sources[source_index]->bp_addresses[i]/2 != 0)
	    {
		gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER(buffer), &iter, i);
		gtk_source_buffer_create_source_mark (buffer, NULL, MARK_BP_AVAILABLE_SOURCE, &iter);
	    }
	}
}

void window_source_populate_notebook(window_source_t* window)
{
    window->notebook = gtk_notebook_new();
    gtk_notebook_set_tab_pos (GTK_NOTEBOOK (window->notebook), GTK_POS_TOP);
    
    int i = 0;
    for (i; i<window->microcontroller->debugger->n_sources; i++)
    {
	char* file_path = window->microcontroller->debugger->sources[i]->file_path;
	if (access(file_path, F_OK) == -1)
	{
	    notice("Source file %s cannot be found.", file_path);
	    continue;
	}    
	
	window->view[i] = GTK_SOURCE_VIEW (gtk_source_view_new ());
	g_object_set (window->view[i], "expand", TRUE, NULL);
	
	gtk_text_view_set_monospace (GTK_TEXT_VIEW (window->view[i]), TRUE);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (window->view[i]), FALSE);
	gtk_source_view_set_show_line_marks (window->view[i], TRUE);
	
	window->buffer[i] = GTK_SOURCE_BUFFER (gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->view[i])));
	gtk_source_buffer_set_language (window->buffer[i], window->language);
	gtk_source_buffer_set_highlight_syntax (window->buffer[i], TRUE);
	window_source_add_source_mark_attributes(window->view[i]);
	//window_source_add_empty_bp_marks(window->buffer[i], i);
	g_signal_connect (window->view[i], "line-mark-activated", G_CALLBACK (window_source_line_mark_activated), window);
	
	window_source_open_file(window->buffer[i], file_path, i);
	
	window->gutter[i] = gtk_source_view_get_gutter(window->view[i], GTK_TEXT_WINDOW_LEFT);
	window->gutterRenderer[i] = gtk_source_gutter_renderer_text_new();
	gtk_source_gutter_renderer_set_visible(window->gutterRenderer[i], TRUE);
	g_object_set (window->gutterRenderer[i],
	      "alignment-mode", GTK_SOURCE_GUTTER_RENDERER_ALIGNMENT_MODE_LAST,
	      "yalign", 0.5,
	      "xalign", 1.0,
	      "xpad", 5,
	      "size", 75,
	      NULL);
	gtk_source_gutter_insert(window->gutter[i], window->gutterRenderer[i], -1);
	g_signal_connect (window->gutterRenderer[i], "query-data",  G_CALLBACK (window_source_query_line_data), window);
	
	window->scrolled_window[i] = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (window->scrolled_window[i]), GTK_WIDGET (window->view[i]));
	
	gtk_notebook_append_page(GTK_NOTEBOOK (window->notebook), GTK_WIDGET (window->scrolled_window[i]), gtk_label_new(window->microcontroller->debugger->sources[i]->name));
    }

    
}

window_source_t* window_source_create_source_window (microcontroller_t* microcontroller)
{
	window_source_t* window = (window_source_t*) malloc(sizeof(window_source_t));
	window->microcontroller = microcontroller;
	
	window->dock_item = gdl_dock_item_new ("sources", "Source Files", GDL_DOCK_ITEM_BEH_CANT_ICONIFY | GDL_DOCK_ITEM_BEH_CANT_CLOSE);

	//window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	//gtk_window_set_default_size (GTK_WINDOW (window->window), 600, 600);
	//gtk_window_set_title(window->window, "Sources");

	window->manager = gtk_source_language_manager_get_default ();
	window->language = gtk_source_language_manager_get_language (window->manager, "c");

	window_source_populate_notebook(window);
	

	gtk_container_add (GTK_CONTAINER (window->dock_item), window->notebook);
	//gtk_container_add (GTK_CONTAINER (window->window), window->notebook);

	//gtk_widget_show_all (window->window);
	window->dock_item_hidden = 0;
	
	return window;
}
