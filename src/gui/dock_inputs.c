#include "gui/dock_inputs.h"

#include "globals.h"
#include "context.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

context_t* context;

int get_button_bit(GtkWidget* button)
{
    if (strcmp(gtk_button_get_label(GTK_BUTTON (button)), "0") == 0)
        return 0;
    else if (strcmp(gtk_button_get_label(GTK_BUTTON (button)), "1") == 0)
        return 1;
    else if (strcmp(gtk_button_get_label(GTK_BUTTON (button)), "2") == 0)
        return 2;
    else if (strcmp(gtk_button_get_label(GTK_BUTTON (button)), "3") == 0)
        return 3;
    else if (strcmp(gtk_button_get_label(GTK_BUTTON (button)), "4") == 0)
        return 4;
    else if (strcmp(gtk_button_get_label(GTK_BUTTON (button)), "5") == 0)
        return 5;
    else if (strcmp(gtk_button_get_label(GTK_BUTTON (button)), "6") == 0)
        return 6;
    else if (strcmp(gtk_button_get_label(GTK_BUTTON (button)), "7") == 0)
        return 7;
        
    return 0;
}        

gboolean pinx_button_unset(input_toggler_t* toggler)
{
    gtk_toggle_button_set_active(toggler->button, FALSE);
    *(toggler->reg) &= ~(1 << toggler->bit);
    toggler = NULL;
    free(toggler);
    return FALSE;
}

void pinx_button_toggled(GtkWidget* button, GdkEventButton* event, uint8_t* reg)
{
    
    int bit = get_button_bit(button);
        
    if (event->type == GDK_BUTTON_PRESS  &&  event->button == 1) // Left mouse click
    {
        if (!gtk_toggle_button_get_active(button))
        {
            gtk_toggle_button_set_active(button, TRUE);
            input_toggler_t* toggler = (input_toggler_t*) malloc(sizeof(input_toggler_t));
            toggler->button = button;
            toggler->reg = reg;
            toggler->bit = bit;
            
            *reg |= (1 << bit);
            g_timeout_add(150, G_CALLBACK (pinx_button_unset), toggler);
        }
        else
        {
            gtk_toggle_button_set_active(button, FALSE);
            *reg &= ~(1 << bit);
        }
    }
    else if (event->type == GDK_BUTTON_PRESS  &&  event->button == 3) // Right mouse click
    {
        if (!gtk_toggle_button_get_active(button))
        {
            gtk_toggle_button_set_active(button, TRUE);
            *reg |= (1 << bit);
        }
        else
        {
            gtk_toggle_button_set_active(button, FALSE);
            *reg &= ~(1 << bit);
        }
    }    
}

void adcx_scale_changed(GtkRange* range, uint16_t* adc)
{
    double val = gtk_range_get_value(range);
    *adc = (uint16_t) val;
}

window_inputs_t* window_inputs_create_inputs_window (microcontroller_t* microcontroller)
{
	
	GtkWidget* vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
	GtkWidget* hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	
	window_inputs_t* window = (window_inputs_t*) malloc(sizeof(window_inputs_t));
	window->microcontroller = microcontroller;

    window->dock_item = gdl_dock_item_new ("inputs", "Inputs", GDL_DOCK_ITEM_BEH_CANT_ICONIFY | GDL_DOCK_ITEM_BEH_CANT_CLOSE);
	

    window->expander_pinb = gtk_expander_new("PINB");
    
    GtkWidget* button_pb7 = gtk_toggle_button_new_with_label("7");
    g_signal_connect (button_pb7, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINB);
    gtk_container_add(GTK_CONTAINER (hbox), button_pb7);
    gtk_widget_add_events(button_pb7, GDK_BUTTON_PRESS_MASK);
    
    GtkWidget* button_pb6 = gtk_toggle_button_new_with_label("6");
    g_signal_connect (button_pb6, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINB);
    gtk_container_add(GTK_CONTAINER (hbox), button_pb6);
    
    GtkWidget* button_pb5 = gtk_toggle_button_new_with_label("5");
    g_signal_connect (button_pb5, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINB);
    gtk_container_add(GTK_CONTAINER (hbox), button_pb5);

    GtkWidget* button_pb4 = gtk_toggle_button_new_with_label("4");
    g_signal_connect (button_pb4, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINB);
    gtk_container_add(GTK_CONTAINER (hbox), button_pb4);

    GtkWidget* button_pb3 = gtk_toggle_button_new_with_label("3");
    g_signal_connect (button_pb3, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINB);
    gtk_container_add(GTK_CONTAINER (hbox), button_pb3);

    GtkWidget* button_pb2 = gtk_toggle_button_new_with_label("2");
    g_signal_connect (button_pb2, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINB);
    gtk_container_add(GTK_CONTAINER (hbox), button_pb2);

    GtkWidget* button_pb1 = gtk_toggle_button_new_with_label("1");
    g_signal_connect (button_pb1, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINB);
    gtk_container_add(GTK_CONTAINER (hbox), button_pb1);

    GtkWidget* button_pb0 = gtk_toggle_button_new_with_label("0");
    g_signal_connect (button_pb0, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINB);
    gtk_container_add(GTK_CONTAINER (hbox), button_pb0);    
    
    gtk_container_add(GTK_CONTAINER (window->expander_pinb), hbox);
    hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    
    window->expander_pinc = gtk_expander_new("PINC");
    
    GtkWidget* button_pc7 = gtk_toggle_button_new_with_label("7");
    g_signal_connect (button_pc7, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINC);
    gtk_container_add(GTK_CONTAINER (hbox), button_pc7);
    gtk_widget_add_events(button_pc7, GDK_BUTTON_PRESS_MASK);
    
    GtkWidget* button_pc6 = gtk_toggle_button_new_with_label("6");
    g_signal_connect (button_pc6, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINC);
    gtk_container_add(GTK_CONTAINER (hbox), button_pc6);
    
    GtkWidget* button_pc5 = gtk_toggle_button_new_with_label("5");
    g_signal_connect (button_pc5, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINC);
    gtk_container_add(GTK_CONTAINER (hbox), button_pc5);

    GtkWidget* button_pc4 = gtk_toggle_button_new_with_label("4");
    g_signal_connect (button_pc4, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINC);
    gtk_container_add(GTK_CONTAINER (hbox), button_pc4);

    GtkWidget* button_pc3 = gtk_toggle_button_new_with_label("3");
    g_signal_connect (button_pc3, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINC);
    gtk_container_add(GTK_CONTAINER (hbox), button_pc3);

    GtkWidget* button_pc2 = gtk_toggle_button_new_with_label("2");
    g_signal_connect (button_pc2, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINC);
    gtk_container_add(GTK_CONTAINER (hbox), button_pc2);

    GtkWidget* button_pc1 = gtk_toggle_button_new_with_label("1");
    g_signal_connect (button_pc1, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINC);
    gtk_container_add(GTK_CONTAINER (hbox), button_pc1);

    GtkWidget* button_pc0 = gtk_toggle_button_new_with_label("0");
    g_signal_connect (button_pc0, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PINC);
    gtk_container_add(GTK_CONTAINER (hbox), button_pc0);    
    
    gtk_container_add(GTK_CONTAINER (window->expander_pinc), hbox);
    hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    
    window->expander_pind = gtk_expander_new("PIND");
    
    GtkWidget* button_pd7 = gtk_toggle_button_new_with_label("7");
    g_signal_connect (button_pd7, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PIND);
    gtk_container_add(GTK_CONTAINER (hbox), button_pd7);
    gtk_widget_add_events(button_pd7, GDK_BUTTON_PRESS_MASK);
    
    GtkWidget* button_pd6 = gtk_toggle_button_new_with_label("6");
    g_signal_connect (button_pd6, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PIND);
    gtk_container_add(GTK_CONTAINER (hbox), button_pd6);
    
    GtkWidget* button_pd5 = gtk_toggle_button_new_with_label("5");
    g_signal_connect (button_pd5, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PIND);
    gtk_container_add(GTK_CONTAINER (hbox), button_pd5);

    GtkWidget* button_pd4 = gtk_toggle_button_new_with_label("4");
    g_signal_connect (button_pd4, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PIND);
    gtk_container_add(GTK_CONTAINER (hbox), button_pd4);

    GtkWidget* button_pd3 = gtk_toggle_button_new_with_label("3");
    g_signal_connect (button_pd3, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PIND);
    gtk_container_add(GTK_CONTAINER (hbox), button_pd3);

    GtkWidget* button_pd2 = gtk_toggle_button_new_with_label("2");
    g_signal_connect (button_pd2, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PIND);
    gtk_container_add(GTK_CONTAINER (hbox), button_pd2);

    GtkWidget* button_pd1 = gtk_toggle_button_new_with_label("1");
    g_signal_connect (button_pd1, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PIND);
    gtk_container_add(GTK_CONTAINER (hbox), button_pd1);

    GtkWidget* button_pd0 = gtk_toggle_button_new_with_label("0");
    g_signal_connect (button_pd0, "button-press-event", G_CALLBACK (pinx_button_toggled), microcontroller->registers->PIND);
    gtk_container_add(GTK_CONTAINER (hbox), button_pd0);    
    
    gtk_container_add(GTK_CONTAINER (window->expander_pind), hbox);
    
    
    window->expander_adc0 = gtk_expander_new("ADC0");
    GtkWidget* scale_adc0 = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1023, 1);
    gtk_container_add(GTK_CONTAINER (window->expander_adc0), scale_adc0);
    g_signal_connect (scale_adc0, "value-changed", G_CALLBACK (adcx_scale_changed), &(microcontroller->adc0));
    
    window->expander_adc1 = gtk_expander_new("ADC1");
    GtkWidget* scale_adc1 = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1023, 1);
    gtk_container_add(GTK_CONTAINER (window->expander_adc1), scale_adc1);
    g_signal_connect (scale_adc1, "value-changed", G_CALLBACK (adcx_scale_changed), &(microcontroller->adc1));
    
    window->expander_adc2 = gtk_expander_new("ADC2");
    GtkWidget* scale_adc2 = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1023, 1);
    gtk_container_add(GTK_CONTAINER (window->expander_adc2), scale_adc2);
    g_signal_connect (scale_adc2, "value-changed", G_CALLBACK (adcx_scale_changed), &(microcontroller->adc2));
    
    window->expander_adc3 = gtk_expander_new("ADC3");
    GtkWidget* scale_adc3 = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1023, 1);
    gtk_container_add(GTK_CONTAINER (window->expander_adc3), scale_adc3);
    g_signal_connect (scale_adc3, "value-changed", G_CALLBACK (adcx_scale_changed), &(microcontroller->adc3));
    
    window->expander_adc4 = gtk_expander_new("ADC4");
    GtkWidget* scale_adc4 = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1023, 1);
    gtk_container_add(GTK_CONTAINER (window->expander_adc4), scale_adc4);
    g_signal_connect (scale_adc4, "value-changed", G_CALLBACK (adcx_scale_changed), &(microcontroller->adc4));
    
    window->expander_adc5 = gtk_expander_new("ADC5");
    GtkWidget* scale_adc5 = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1023, 1);
    gtk_container_add(GTK_CONTAINER (window->expander_adc5), scale_adc5);
    g_signal_connect (scale_adc5, "value-changed", G_CALLBACK (adcx_scale_changed), &(microcontroller->adc5));
    
    window->expander_adc6 = gtk_expander_new("ADC6");
    GtkWidget* scale_adc6 = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1023, 1);
    gtk_container_add(GTK_CONTAINER (window->expander_adc6), scale_adc6);
    g_signal_connect (scale_adc6, "value-changed", G_CALLBACK (adcx_scale_changed), &(microcontroller->adc6));
    
    window->expander_adc7 = gtk_expander_new("ADC7");
    GtkWidget* scale_adc7 = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 0, 1023, 1);
    gtk_container_add(GTK_CONTAINER (window->expander_adc7), scale_adc7);
    g_signal_connect (scale_adc7, "value-changed", G_CALLBACK (adcx_scale_changed), &(microcontroller->adc7));

    gtk_container_add(GTK_CONTAINER (vbox), window->expander_pinb);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_pinc);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_pind);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_adc0);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_adc1);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_adc2);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_adc3);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_adc4);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_adc5);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_adc6);
    gtk_container_add(GTK_CONTAINER (vbox), window->expander_adc7);
    
    window->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (window->scrolled_window), GTK_WIDGET (vbox));
    
    //gtk_container_add (GTK_CONTAINER (window->window), window->scrolled_window);
    gtk_container_add (GTK_CONTAINER (window->dock_item), window->scrolled_window);
    
    //g_timeout_add(context->refreshRate, G_CALLBACK (window_inputs_update_window), window);
    
    //gtk_widget_show_all (window->window);
    

	return window;
}
