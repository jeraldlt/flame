#include "gui/dock_vcd.h"

#include <stdio.h>

#include "gui/window_new_vcd.h"
#include "context.h"
#include "globals.h"

context_t* context;


void dock_vcd_button_new_pressed(GtkButton* button, dock_vcd_t* dock)
{
    window_new_vcd_new_window(dock->microcontroller);
}

void dock_vcd_button_save_pressed(GtkButton* button, dock_vcd_t* dock)
{
    save_vcd(context, g_convert (gtk_entry_get_text(GTK_ENTRY (dock->entry_file)), -1, "ISO-8859-1", "UTF-8", NULL, NULL, NULL));
}

gboolean dock_vcd_update_dock(dock_vcd_t* dock)
{
    if (dock->microcontroller->resetting)
	    return TRUE;
	    
    if (dock->microcontroller->vcd_vars_count > 0)
    {
        GtkTreeIter tree_iter;
        gtk_tree_model_get_iter_first(GTK_TREE_MODEL (dock->treestore_data), &tree_iter);
        
        char bit[2] = {0};
        sprintf(bit, "%c", (char) (dock->microcontroller->vcd_vars[0]->bit + 48));
        gtk_tree_store_set(GTK_TREE_STORE (dock->treestore_data), &tree_iter, 0, dock->microcontroller->vcd_vars[0]->register_name, 1, bit, 2, dock->microcontroller->vcd_vars[0]->name, -1);
        int i = 1;
        for (i; i<dock->microcontroller->vcd_vars_count; i++)
        {
            if (!gtk_tree_model_iter_next(GTK_TREE_MODEL (dock->treestore_data), &tree_iter))
            {
                gtk_tree_store_append(GTK_TREE_STORE (dock->treestore_data), &tree_iter, NULL);
            }
            char bit[2] = {0};
            sprintf(bit, "%c", (char) (dock->microcontroller->vcd_vars[i]->bit + 48));
            gtk_tree_store_set(GTK_TREE_STORE (dock->treestore_data), &tree_iter, 0, dock->microcontroller->vcd_vars[i]->register_name, 1, bit, 2, dock->microcontroller->vcd_vars[i]->name, -1);
            
        }
    }
    
    return TRUE;
}

void dock_vcd_file_icon_pressed(GtkEntry* entry, GtkEntryIconPosition icon_pos, GdkEvent* event, dock_vcd_t* dock)
{
    GtkWidget* dialog;
    int res;
    char *filename;
    
    dialog = gtk_file_chooser_dialog_new ("Open File", NULL, GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
    
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT)
    {
        
        GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
        filename = gtk_file_chooser_get_filename (chooser);
        gtk_entry_set_text(entry, filename);
    }
    
    gtk_widget_destroy (dialog);
}

dock_vcd_t* dock_vcd_create_dock(microcontroller_t* microcontroller)
{
    dock_vcd_t* dock = (dock_vcd_t*) malloc(sizeof(dock_vcd_t));
    dock->microcontroller = microcontroller;
    
    dock->dock_item = gdl_dock_item_new ("vcd", "VCD", GDL_DOCK_ITEM_BEH_CANT_ICONIFY | GDL_DOCK_ITEM_BEH_CANT_CLOSE);
	
	dock->treestore_data = gtk_tree_store_new(3, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
	GtkTreeIter tree_iter;
	GtkTreePath* tree_path;
    gtk_tree_store_append(GTK_TREE_STORE (dock->treestore_data), &tree_iter, NULL);
	tree_path = gtk_tree_model_get_path(GTK_TREE_MODEL (dock->treestore_data), &tree_iter);
	gtk_tree_store_set(GTK_TREE_STORE (dock->treestore_data), &tree_iter, 0, "", 1, "", 2, "", -1);
	
	dock->treeview_data = gtk_tree_view_new();
	
	dock->renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "Register", dock->renderer, "text", 0, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "Bit", dock->renderer, "text", 1, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dock->treeview_data), -1, "Name", dock->renderer, "text", 2, NULL);
    
    gtk_tree_view_set_model(GTK_TREE_VIEW (dock->treeview_data), GTK_TREE_MODEL (dock->treestore_data));
    //g_signal_connect(window->treeview_data, "row-activated", G_CALLBACK (window_registers_row_activated), window);
    
    
    dock->label_vars = gtk_label_new("VCD Variables");
    g_object_set(dock->label_vars, "xalign", 0.05, NULL);
    
    dock->label_file = gtk_label_new("VCD File");
    g_object_set(dock->label_file, "xalign", 0.05, NULL);
    
    dock->button_new = gtk_button_new_with_label("New VCD Variable");
    g_signal_connect(dock->button_new, "clicked", G_CALLBACK (dock_vcd_button_new_pressed), dock);
    dock->button_save = gtk_button_new_with_label("Save");
    g_signal_connect(dock->button_save, "clicked", G_CALLBACK (dock_vcd_button_save_pressed), dock);
    
    dock->entry_file = gtk_entry_new();
    gtk_entry_set_placeholder_text (GTK_ENTRY (dock->entry_file), "VCD File");
    gtk_entry_set_text(dock->entry_file, "out.vcd");
    gtk_entry_set_icon_from_icon_name(dock->entry_file, GTK_ENTRY_ICON_SECONDARY, "document-open");
    g_signal_connect(dock->entry_file, "icon-press", G_CALLBACK (dock_vcd_file_icon_pressed), dock);
    
    dock->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    
    dock->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
    
    gtk_container_add(GTK_CONTAINER (dock->vbox), dock->button_new);
    gtk_container_add(GTK_CONTAINER (dock->vbox), dock->label_vars);
    gtk_container_add(GTK_CONTAINER (dock->vbox), GTK_WIDGET (dock->treeview_data));
    gtk_container_add(GTK_CONTAINER (dock->vbox), dock->label_file);
    gtk_container_add(GTK_CONTAINER (dock->vbox), dock->entry_file);
    gtk_container_add(GTK_CONTAINER (dock->vbox), dock->button_save);
    
    gtk_container_add(GTK_CONTAINER (dock->scrolled_window), dock->vbox);
    
    gtk_container_add(GTK_CONTAINER (dock->dock_item), dock->scrolled_window);
    
    dock->dock_item_hidden = 1;
    
    g_timeout_add(context->refresh_rate, G_CALLBACK (dock_vcd_update_dock), dock);
    
    return dock;

}
