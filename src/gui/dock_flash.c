/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_flash.c

*/

#define MARK_BP_CURRENT_FLASH     "mark_bp_current_flash" 
#define MARK_BP_ACTIVE_FLASH      "mark_bp_active_flash"
#define MARK_BP_INACTIVE_FLASH    "mark_bp_inactive_flash"
#define MARK_PC_CURRENT_FLASH     "mark_pc_current_flash"


#include "gui/dock_flash.h"

#include "print_instructions.h"
#include "globals.h"
#include "util.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

context_t* context;

void window_flash_query_line_data(GtkSourceGutterRenderer *renderer, GtkTextIter *start, GtkTextIter *end, GtkSourceGutterRendererState state, window_flash_t* window)
{
    // Whether to display address in word or byte addressing
	int address_size = 1;
	if (context->options->gui_byte_addressing)
		address_size = 2;
	
	// Create address text for gutter
	char buf[8] = {0};
	sprintf(buf, "%04X", gtk_text_iter_get_line(start) * address_size);
	gtk_source_gutter_renderer_text_set_text(renderer, buf, -1);
	
	
	window_flash_set_line_mark(start, window);
}

void window_flash_set_line_mark(GtkTextIter* start, window_flash_t* window)
{
    GSList *mark_list;
	const gchar *mark_type;
	int location = gtk_text_iter_get_line (start);
	int bp = window->microcontroller->debugger->breakpoints[location];
	
	mark_type = MARK_BP_ACTIVE_FLASH;
	mark_list = gtk_source_buffer_get_source_marks_at_line (window->buffer,	location, MARK_BP_INACTIVE_FLASH);
	
	if (mark_list != NULL)
	{
		mark_list = gtk_source_buffer_get_source_marks_at_line (window->buffer, location, MARK_BP_ACTIVE_FLASH);
		if (bp && mark_list == NULL)
		{
		    gtk_source_buffer_create_source_mark (window->buffer, NULL, mark_type, start);
		}
		else if (!bp && mark_list != NULL)
		{
			gtk_text_buffer_delete_mark (GTK_TEXT_BUFFER (window->buffer), GTK_TEXT_MARK (mark_list->data));
		}
	}
	
	mark_list = gtk_source_buffer_get_source_marks_at_line (window->buffer,	location, MARK_PC_CURRENT_FLASH);
	if (mark_list != NULL)
		gtk_text_buffer_delete_mark (GTK_TEXT_BUFFER (window->buffer), GTK_TEXT_MARK (mark_list->data));
	
	if (location == *(window->microcontroller->registers->PC))
		gtk_source_buffer_create_source_mark (window->buffer, NULL, MARK_PC_CURRENT_FLASH, start);
	
	

	g_slist_free (mark_list);
}

void window_flash_line_mark_activated (GtkSourceGutter *gutter, GtkTextIter *iter, GdkEventButton *event, microcontroller_t* microcontroller)
{
	
	//mark_type = event->button == 1 ? MARK_BP_CURRENT : MARK_BP_AVAILABLE;
	if (event->button != 1)
		return;
	
	int location = gtk_text_iter_get_line (iter);
	microcontroller->debugger->breakpoints[location] ^= 1;
	
	
}

gboolean window_flash_update_current_bp(window_flash_t* window)
{

    GtkSourceMarkAttributes *attrs;
	attrs = gtk_source_mark_attributes_new ();

    if (context->state != STATE_INDEBUG && context->state != STATE_STEPPING_DEBUG)
    {
        gtk_source_mark_attributes_set_background(attrs, window->color_yellow);
        gtk_source_view_set_mark_attributes (window->view, MARK_PC_CURRENT_FLASH, attrs, 1);
    	g_object_unref (attrs);

        window->bp_flash_on = 1;
                
        g_timeout_add(context->refresh_rate, G_CALLBACK (window_flash_update_flash_window), window);
        return FALSE;
    }
    
    if (window->bp_flash_on)
    {
        gtk_source_mark_attributes_set_background(attrs, window->color_yellow);
    }
    
    else
    {
        gtk_source_mark_attributes_set_background(attrs, window->color_red);
    }
    
    gtk_source_view_set_mark_attributes (window->view, MARK_PC_CURRENT_FLASH, attrs, 1);
	g_object_unref (attrs);
    
    window->bp_flash_on = !(window->bp_flash_on);
    
    GtkTextIter iter;
    gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER (window->buffer), &iter, window->microcontroller->debugger->cur_breakpoint);
    
    window_flash_set_line_mark(&iter, window);
    
    gtk_widget_queue_draw(GTK_WIDGET (window->view));
    
    return TRUE;
}

void window_flash_add_source_mark_attributes (window_flash_t* window)
{
	GtkSourceMarkAttributes *attrs;

	attrs = gtk_source_mark_attributes_new ();
	
	gtk_source_mark_attributes_set_background (attrs, window->color_red);

	GtkImage* icon_bp_active = gtk_image_new_from_file("../data/icon_bp_active.png");
	GdkPixbuf* pixbuf_bp_active = gtk_image_get_pixbuf(icon_bp_active);
	
	gtk_source_mark_attributes_set_pixbuf(attrs, pixbuf_bp_active);

	gtk_source_view_set_mark_attributes (window->view, MARK_BP_ACTIVE_FLASH, attrs, 1);
	g_object_unref (attrs);


	attrs = gtk_source_mark_attributes_new ();

	GtkImage* icon_bp_inactive = gtk_image_new_from_file("../data/icon_bp_inactive.png");
	GdkPixbuf* pixbuf_bp_inactive = gtk_image_get_pixbuf(icon_bp_inactive);
	
	gtk_source_mark_attributes_set_pixbuf(attrs, pixbuf_bp_inactive);

	gtk_source_view_set_mark_attributes (window->view, MARK_BP_INACTIVE_FLASH, attrs, 2);
	g_object_unref (attrs);
	
	
	attrs = gtk_source_mark_attributes_new();

	
	gtk_source_mark_attributes_set_background(attrs, window->color_yellow);
	gtk_source_view_set_mark_attributes(window->view, MARK_PC_CURRENT_FLASH, attrs, 3);
}

void window_flash_add_buffer_line(GtkTextBuffer *buffer, GtkTextIter *iter, gchar *text, int last_line)
{
	gtk_source_buffer_create_source_mark (GTK_SOURCE_BUFFER (buffer), NULL, MARK_BP_INACTIVE_FLASH, iter);
	gtk_text_buffer_insert(buffer, iter, text, -1);
	if (!last_line)
	{
		gtk_text_buffer_insert(buffer, iter, "\n", -1);
		gtk_text_iter_forward_line (iter);
	}
}

void window_flash_fill_buffer (window_flash_t* window)
{
	
	int state = context->state;
	uint16_t pc = *(window->microcontroller->registers->PC);
	
	context->state = STATE_STOPPED;
	*(window->microcontroller->registers->PC) = 0;
	
	GtkTextIter iter;

	GtkTextBuffer* buffer = GTK_TEXT_BUFFER (window->buffer);

	gtk_text_buffer_set_text (buffer, "", 0);

	gtk_text_buffer_get_start_iter (buffer, &iter);
	
	int address = window->microcontroller->flash->start;
	int address_high = window->microcontroller->flash->end;
	uint16_t instruction = 0;
	char buf[32];
	int last_line = FALSE;
	
	for(address; address<address_high; address++)
	{
		if (address == address_high-1)
			last_line = TRUE;
			
		int i = 0;
		for (i; i<32; i++)
		{
			buf[i] = 0;
		}
		instruction = swapBytes(window->microcontroller->flash->data[address]);
		if (print_opcodes[instruction] > 0)
		{
		    (*print_opcodes[instruction])(window->microcontroller, buf);
		    
		    window_flash_add_buffer_line(buffer, &iter, buf, last_line);
		}
		else
		{
			window_flash_add_buffer_line(buffer, &iter, "...", last_line);
		}
		
		(*(window->microcontroller->registers->PC))++;
	}
	
	*(window->microcontroller->registers->PC) = 0;
	
	GtkSourceGutterRenderer* gutterRenderer = gtk_source_gutter_renderer_text_new();
	gtk_source_gutter_renderer_set_visible(gutterRenderer, TRUE);
	g_object_set (gutterRenderer, "alignment-mode", GTK_SOURCE_GUTTER_RENDERER_ALIGNMENT_MODE_LAST, "yalign", 0.5, "xalign", 0.5, "xpad", 25, NULL);
	gtk_source_gutter_insert(window->gutter, gutterRenderer, -1);
	g_signal_connect (gutterRenderer, "query-data",  G_CALLBACK (window_flash_query_line_data), window);
	
	*(window->microcontroller->registers->PC) = pc;
	context->state = state;
}

gboolean window_flash_update_flash_window(window_flash_t* window)
{
    if (window->microcontroller->resetting)
	    return TRUE;
	    
    if (context->state == STATE_INDEBUG)
    {
        g_timeout_add(1000, G_CALLBACK (window_flash_update_current_bp), window);
        return FALSE;
    }
    
    return TRUE;
}

window_flash_t* window_flash_create_flash_window (microcontroller_t* microcontroller)
{
	window_flash_t* window = (window_flash_t*) malloc(sizeof(window_flash_t));
	window->microcontroller = microcontroller;
	window->bp_flash_on = 1;
	
	window->color_yellow = (GdkRGBA*) malloc(sizeof(GdkRGBA));
	window->color_yellow->red = 0.5;
	window->color_yellow->green = 0.5;
	window->color_yellow->blue = 0.0;
	window->color_yellow->alpha = 0.5;
	
	window->color_red = (GdkRGBA*) malloc(sizeof(GdkRGBA));
	window->color_red->red = 1.0;
	window->color_red->green = 0.0;
	window->color_red->blue = 0.0;
	window->color_red->alpha = 0.5;
	
	window->dock_item = gdl_dock_item_new ("flash", "Device Flash", GDL_DOCK_ITEM_BEH_CANT_ICONIFY | GDL_DOCK_ITEM_BEH_CANT_CLOSE);

	window->view = GTK_SOURCE_VIEW (gtk_source_view_new ());

	g_object_set (window->view, "expand", TRUE, NULL);

	gtk_text_view_set_monospace (GTK_TEXT_VIEW (window->view), TRUE);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (window->view), FALSE);
	gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW (window->view), FALSE);
	gtk_source_view_set_show_line_marks (window->view, TRUE);

	window->buffer = GTK_SOURCE_BUFFER (gtk_text_view_get_buffer (GTK_TEXT_VIEW (window->view)));

	window->tag = gtk_source_buffer_create_source_tag (window->buffer, NULL, "draw-spaces", FALSE, NULL);

	window->gutter = gtk_source_view_get_gutter(window->view, GTK_TEXT_WINDOW_LEFT);
	window_flash_fill_buffer (window);
	
	window->manager = gtk_source_language_manager_get_default ();
	
	window->language = gtk_source_language_manager_get_language (window->manager, "c");
	gtk_source_buffer_set_language (window->buffer, window->language);
	gtk_source_buffer_set_highlight_syntax (window->buffer, TRUE);

	
	g_timeout_add(context->refresh_rate, G_CALLBACK (window_flash_update_flash_window), window);
	
	window_flash_add_source_mark_attributes (window);
	g_signal_connect (window->view, "line-mark-activated", G_CALLBACK (window_flash_line_mark_activated), window->microcontroller);
	
	
	window->scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (window->scrolled_window), GTK_WIDGET (window->view));

	gtk_container_add (GTK_CONTAINER (window->dock_item), window->scrolled_window);

	//gtk_widget_show_all (window->window);
	
	window->dock_item_hidden = 0;
	
	return window;
}
