/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_microcontroller.c

*/

#include <stdlib.h>
#include <libgen.h>
#include <unistd.h>

#include "globals.h"
#include "registers.h"

#include "gui/window_microcontroller.h"

#include "debug.h"
#include "plugin.h"


context_t* context;

gboolean on_key_press (GtkWidget *widget, GdkEventKey *event, window_microcontroller_t* window) 
{
    if (event->keyval == GDK_KEY_F1)
    {
        button_start_stop_pressed(GTK_BUTTON (window->button_start_stop), window);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_F2)
    {
        button_debugging_on_off_pressed(GTK_BUTTON (window->button_debugging_on_off), window);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_F3)
    {
        button_step_pressed(GTK_BUTTON (window->button_step), window);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_F4 && context->state == STATE_INDEBUG)
    {
        button_continue_pressed(GTK_BUTTON (window->button_continue), window);
        return TRUE;
    }
    else if (event->keyval == GDK_KEY_F5)
    {
        button_reset_pressed(GTK_BUTTON (window->button_reset), window);
        return TRUE;
    }
    
    else if (event->keyval == GDK_KEY_F12)
    {
        debugger_t* debugger = window->microcontroller->debugger;
        int i = 0;
        
        for (i; i<debugger->sources[0]->subprograms[0]->n_variables; i++)
        {
            if (strcmp("var_int", debugger->sources[0]->subprograms[0]->variables[i]->name) == 0)
            {
                int val = 0;
                
                uint8_t* address = debug_variable_location(window->microcontroller, debugger->sources[0]->subprograms[0]->variables[i]);
                
                if (address == 0)
                {
                    printf("invalid address\n");
                }
                else
                {
                    memcpy(&val, address, 2);
                    printf("val: %i, 0x%X\n", val, (unsigned) val);
                }
            }
        }
        return TRUE;
    }
    return FALSE;
}

void expander_info_activated(GtkExpander* expander, window_microcontroller_t* window)
{
    printf("Expander activated...\n");
    if (gtk_expander_get_expanded(expander))
    {
        gtk_label_set_text(GTK_LABEL( gtk_expander_get_label_widget(expander) ), "");
    }
    else
    {
        gtk_label_set_text(GTK_LABEL( gtk_expander_get_label_widget(expander) ), "MCU Info");
    }
}



//------------------------------------------------------------------------------
//                                                   CONTROL BUTTONS CALLBACKS
//==============================================================================

void button_start_stop_pressed(GtkButton* button, window_microcontroller_t* window)
{
    if (context->state == STATE_STOPPED)
    {
        context->state = STATE_RUNNING;
        GtkWidget* icon_stop = gtk_image_new_from_file("../data/icon_stop.png");
    	gtk_button_set_image(GTK_BUTTON (window->button_start_stop), icon_stop);
        gtk_widget_set_tooltip_text(window->button_start_stop, "Stop (F1)"); 
    }
    else
    {
        context->state = STATE_STOPPED;
        GtkWidget* icon_start = gtk_image_new_from_file("../data/icon_start.png");
    	gtk_button_set_image(GTK_BUTTON (window->button_start_stop), icon_start);
        gtk_widget_set_tooltip_text(window->button_start_stop, "Start (F1)");
        
    }
}

void button_debugging_on_off_pressed(GtkButton* button, window_microcontroller_t* window)
{
    if (context->debugging_enabled)
    {
        context->debugging_enabled = 0;
        GtkWidget* icon_debugging_on = gtk_image_new_from_file("../data/icon_bp_inactive.png");
        gtk_button_set_image(GTK_BUTTON (window->button_debugging_on_off), icon_debugging_on);
        gtk_widget_set_tooltip_text(window->button_debugging_on_off, "Turn Debugging On (F2)");
    }
    else
    {
        context->debugging_enabled = 1;
        GtkWidget* icon_debugging_off = gtk_image_new_from_file("../data/icon_bp_active.png");
        gtk_button_set_image(GTK_BUTTON (window->button_debugging_on_off), icon_debugging_off);
        gtk_widget_set_tooltip_text(window->button_debugging_on_off, "Turn Debugging Off (F2)");
    }
}

void button_step_pressed(GtkButton* button, window_microcontroller_t* window)
{
    if (context->state == STATE_STOPPED)
    {
        context->state = STATE_STEPPING;
        GtkTextIter iter;
        gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER (window->window_flash->buffer), &iter, *(window->microcontroller->registers->PC));
        gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW (window->window_flash->view), &iter, 0.1, TRUE, 0.0, 0.5);     
    }
    else if (context->state == STATE_INDEBUG)
    {
        context->state = STATE_STEPPING_DEBUG;
        GtkTextIter iter;
        gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER (window->window_flash->buffer), &iter, *(window->microcontroller->registers->PC));
        gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW (window->window_flash->view), &iter, 0.1, TRUE, 0.0, 0.5);
    }
}

void button_continue_pressed(GtkButton* button, window_microcontroller_t* window)
{
    context->state = STATE_RUNNING;
}

void button_reset_pressed(GtkButton* button, window_microcontroller_t* window)
{
    if (context->state != STATE_STOPPED)
    {
        button_start_stop_pressed(window->button_start_stop, window);
    }

    window->microcontroller->resetting = 1;
    usleep(context->refresh_rate * 2 * 1000);
    reset_microcontroller(window->microcontroller);

}

void button_program_pressed(GtkButton* button, window_microcontroller_t* window)
{
    /*
    GtkWidget* dialog;
    int res;
    char *filename;
    
    dialog = gtk_file_chooser_dialog_new ("Open File", window, GTK_FILE_CHOOSER_ACTION_OPEN, "_Cancel", GTK_RESPONSE_CANCEL, "_Open", GTK_RESPONSE_ACCEPT, NULL);
    
    res = gtk_dialog_run (GTK_DIALOG (dialog));
    if (res == GTK_RESPONSE_ACCEPT)
    {
        
        GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
        filename = gtk_file_chooser_get_filename (chooser);
    
        FILE* fp = fopen(filename, "r");
        loadElf(fp, basename(filename), window->microcontroller);
        fclose(fp);
    }
    
    gtk_widget_destroy (dialog);
    
    window_flash_fill_buffer(window->window_flash);
    
    gdl_dock_item_unbind(window->window_source->dock_item);
    window_source_populate_notebook(window->window_source);
    window->window_source->dock_item = gdl_dock_item_new ("sources", "Source Files", GDL_DOCK_ITEM_BEH_NORMAL);
    gtk_container_add (GTK_CONTAINER (window->window_source->dock_item), window->window_source->notebook);
    gdl_dock_add_item(window->dock, window->window_source->dock_item, GDL_DOCK_RIGHT);
    
    resetMicrocontroller(window->microcontroller);
    */
    
}

void button_settings_pressed(GtkButton* button, window_microcontroller_t* window)
{
    window_settings_create_settings_window(window);
}


//------------------------------------------------------------------------------
//                                                                 UPDATE LOOP
//==============================================================================

gboolean update_microcontroller_window(window_microcontroller_t* window)
{
    if (window->microcontroller->resetting)
	    return TRUE;
	    
    GtkTreeIter tree_iter;
    
    char program_size[32] = {0};
    sprintf(program_size, "%i bytes", ((window->microcontroller->flash->end - window->microcontroller->flash->start) * 2));
    
    char clock[8] = {0};
    sprintf(clock, "%iMHz", window->microcontroller->clock_mhz);
    
    char ticks[32] = {0};
    sprintf(ticks, "%lu", window->microcontroller->ticks);
    
    char elapsed_time[32] = {0};
    sprintf(elapsed_time, "%fs", (1.0 / (window->microcontroller->clock_mhz * 1000000)) * window->microcontroller->ticks);
    
    char avg_tick_time[32] = {0};
    sprintf(avg_tick_time, "%uns", (uint64_t)(window->microcontroller->tick_time / (window->microcontroller->ticks + 1)));
    
    char pc[8] = {0};
    sprintf(pc, "0x%04X", *(window->microcontroller->registers->PC) * 2);
    
    char sp[8] = {0};
    sprintf(sp, "0x%04X", get_sp(window->microcontroller->registers));

    
    gtk_tree_model_get_iter_first(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Model: ", 1, window->microcontroller->model, -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Clock: ", 1, clock, -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Program: ", 1, window->microcontroller->flash->name, -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Program Size: ", 1, program_size, -1);
	
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	if (window->microcontroller->debug_symbols)
    	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Debug Symbols: ", 1, "Yes", -1);
    else
        gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Debug Symbols: ", 1, "No", -1);
	
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "", 1, "", -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	
	if (context->state == STATE_STOPPED)
	    gtk_list_store_set(window->liststore_info, &tree_iter, 0, "State: ", 1, "Stopped", -1);
	
	else if (context->state == STATE_RUNNING)
	    gtk_list_store_set(window->liststore_info, &tree_iter, 0, "State: ", 1, "Running", -1);

    else if (context->state == STATE_INDEBUG)
	    gtk_list_store_set(window->liststore_info, &tree_iter, 0, "State: ", 1, "In Debug", -1);


	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Total Ticks: ", 1, ticks, -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Avg. Tick Time: ", 1, avg_tick_time, -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Elapsed Time: ", 1, elapsed_time, -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Program Counter: ", 1, pc, -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Stack Pointer: ", 1, sp, -1);
	
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "", 1, "", -1);
	gtk_tree_model_iter_next(GTK_TREE_MODEL (window->liststore_info), &tree_iter);
	
	
	char buf[8];
	char* bit;
	uint8_t sreg = *(window->microcontroller->registers->SREG);
	
	sprintf(buf, "0x%02X", sreg);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "SREG: ", 1, buf, -1);
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	
	bit = "0";
	if (sreg & 0x80)
		bit = "1";
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "I - Global Interrupt: ", 1, bit, -1);
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	
	bit = "0";
	if (sreg & 0x40)
		bit = "1";
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "T - Transfer Bit: ", 1, bit, -1);
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	
	bit = "0";
	if (sreg & 0x20)
		bit = "1";
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "H - Half Carry: ", 1, bit, -1);
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	
	bit = "0";
	if (sreg & 0x10)
		bit = "1";
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "S - N xor V: ", 1, bit, -1);
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	
	bit = "0";
	if (sreg & 0x08)
		bit = "1";
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "V - 2's Compl. Overflow: ", 1, bit, -1);
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	
	bit = "0";
	if (sreg & 0x04)
		bit = "1";
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "N - Negative: ", 1, bit, -1);
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	
	bit = "0";
	if (sreg & 0x02)
		bit = "1";
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Z - Zero: ", 1, bit, -1);
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	
	bit = "0";
	if (sreg & 0x01)
		bit = "1";
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "C - Carry: ", 1, bit, -1);
	
	gtk_tree_model_iter_next(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "", 1, "", -1);
	
	
	if (context->state == STATE_INDEBUG)
	    gtk_widget_set_sensitive (window->button_continue, TRUE);
	else
	    gtk_widget_set_sensitive (window->button_continue, FALSE);

	
	if (context->breakpoint_triggered)
	{
	    GtkTextIter iter;
        gtk_text_buffer_get_iter_at_line(GTK_TEXT_BUFFER (window->window_flash->buffer), &iter, *(window->microcontroller->registers->PC));
        gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW (window->window_flash->view), &iter, 0.1, TRUE, 0.0, 0.5);
        printf("%lu\n", *(window->microcontroller->registers->PC));
        context->breakpoint_triggered = 0;
	}
	
	context->state_last = context->state;
	
	while (window->n_messages < window->microcontroller->n_messages)
	{
	    gtk_statusbar_push(GTK_STATUSBAR(window->statusbar), 0, g_convert (window->microcontroller->messages[window->n_messages], -1, "ISO-8859-1", "UTF-8", NULL, NULL, NULL));
	    
	    window->n_messages++;
	}
	
	
	return TRUE;
}


//------------------------------------------------------------------------------
//                                                         MENU ITEM CALLBACKS
//==============================================================================

void menu_item_layout_save_activated(GtkMenuItem* item, window_microcontroller_t* window)
{
    char layout[256];
    sprintf(layout, "%s/.flame/layouts/%s", getenv("HOME"), window->active_layout);
    gdl_dock_layout_save_to_file(window->layout, layout);
}

void menu_item_layout_save_as_activated(GtkMenuItem* item, window_microcontroller_t* window)
{
    window_new_layout_new_window(window->microcontroller);
}

void menu_item_layout_changed(GtkMenuItem* item, window_microcontroller_t* window)
{
    char layout[256];
    sprintf(layout, "%s/.flame/layouts/%s", getenv("HOME"), gtk_menu_item_get_label(item));
    gdl_dock_layout_load_from_file(window->layout, layout);
    gdl_dock_layout_load_layout(window->layout, NULL);
    window->active_layout = gtk_menu_item_get_label(item);
}

void menu_item_plugin_toggled(GtkCheckMenuItem* item, plugin_container_t* container)
{
    if (gtk_check_menu_item_get_active(item))
    {
        gdl_dock_item_show_item(container->plugin->dock_item);
    }
    else
    {
        gdl_dock_item_hide_item(container->plugin->dock_item);
    }
}

void menu_item_flash_toggled(GtkCheckMenuItem* item, window_microcontroller_t* window)
{
    if (gtk_check_menu_item_get_active(item))
    {
        gdl_dock_item_show_item(window->window_flash->dock_item);
    }
    else
    {
        gdl_dock_item_hide_item(window->window_flash->dock_item);
    }
}

void menu_item_sources_toggled(GtkCheckMenuItem* item, window_microcontroller_t* window)
{
    if (gtk_check_menu_item_get_active(item))
    {
        gdl_dock_item_show_item(window->window_source->dock_item);
    }
    else
    {
        gdl_dock_item_hide_item(window->window_source->dock_item);
    }
}

void menu_item_registers_toggled(GtkCheckMenuItem* item, window_microcontroller_t* window)
{
    if (gtk_check_menu_item_get_active(item))
    {
        gdl_dock_item_show_item(window->window_registers->dock_item);
    }
    else
    {
        gdl_dock_item_hide_item(window->window_registers->dock_item);
    }
}

void menu_item_inputs_toggled(GtkCheckMenuItem* item, window_microcontroller_t* window)
{
    if (gtk_check_menu_item_get_active(item))
    {
        gdl_dock_item_show_item(window->window_inputs->dock_item);
    }
    else
    {
        gdl_dock_item_hide_item(window->window_inputs->dock_item);
    }
}

void menu_item_stack_toggled(GtkCheckMenuItem* item, window_microcontroller_t* window)
{
    if (gtk_check_menu_item_get_active(item))
    {
        gdl_dock_item_show_item(window->dock_stack->dock_item);
    }
    else
    {
        gdl_dock_item_hide_item(window->dock_stack->dock_item);
    }
}

void menu_item_vcd_toggled(GtkCheckMenuItem* item, window_microcontroller_t* window)
{
    if (gtk_check_menu_item_get_active(item))
    {
        gdl_dock_item_show_item(window->dock_vcd->dock_item);
    }
    else
    {
        gdl_dock_item_hide_item(window->dock_vcd->dock_item);
    }
}

void menu_item_variables_toggled(GtkCheckMenuItem* item, window_microcontroller_t* window)
{
    if (gtk_check_menu_item_get_active(item))
    {
        gdl_dock_item_show_item(window->dock_variables->dock_item);
    }
    else
    {
        gdl_dock_item_hide_item(window->dock_variables->dock_item);
    }
}


void populate_layout_menu(window_microcontroller_t* window)
{
    char layout_dir[256];
	sprintf(layout_dir, "%s/.flame/layouts/", getenv("HOME"));
    
    
    struct dirent *de;   
    DIR *dr = opendir(layout_dir); 
  
    if (dr == NULL) 
    { 
        printf("Could not open layout directory\n"); 
        return 0; 
    }
    
    window->group = NULL; 

    while ((de = readdir(dr)) != NULL)
    {
        char name[128];
        
        if (strcmp(de->d_name, ".") != 0 && strcmp(de->d_name, "..") != 0)
        {     
            GtkWidget* menu_item = gtk_radio_menu_item_new_with_label (window->group, de->d_name);
            window->group = gtk_radio_menu_item_get_group (GTK_RADIO_MENU_ITEM (menu_item));
            gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_layout), menu_item);
            
            if (strcmp(de->d_name, context->options->gui_last_layout) == 0)
            {
                gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (menu_item), TRUE);
                window->active_layout = de->d_name;
                menu_item_layout_changed(menu_item, window);
            }
            
            g_signal_connect(menu_item, "activate", G_CALLBACK (menu_item_layout_changed), window);
        }
    }
  
    closedir(dr);
}


window_microcontroller_t* create_microcontroller_window(microcontroller_t* microcontroller)
{
    
    window_microcontroller_t* window = (window_microcontroller_t*) malloc(sizeof(window_microcontroller_t));
    
    window->microcontroller = microcontroller;
    window->n_messages = 0;
    window->active_layout = NULL;
    
    window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	g_signal_connect (window->window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
	gtk_window_set_title(window->window, "Flame");
	
	window->dock = gdl_dock_new();
	g_object_set (window->dock, "expand", TRUE, NULL);
	window->layout = gdl_dock_layout_new(window->dock);
	
	GtkTreeIter tree_iter;
	window->listbox_info = gtk_list_box_new();
	window->liststore_info = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Model: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Clock: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Program: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Program Size: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Debug Symbols: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "", 1, "", -1);
	
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "State: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Total Ticks: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Avg. Tick Time: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Elapsed Time: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Program Counter: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Stack Pointer: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "", 1, "", -1);
	
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "SREG: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "I - Global Interrupt: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "T - Transfer Bit: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "H - Half Carry: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "S - N xor V: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "V - 2's Comp. Overflow: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "N - Negative: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "Z - Zero: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "C - Carry: ", 1, "", -1);
	gtk_list_store_append(window->liststore_info, &tree_iter);
	gtk_list_store_set(window->liststore_info, &tree_iter, 0, "", 1, "", -1);
	
	window->treeview_info = gtk_tree_view_new();
	
	window->renderer = gtk_cell_renderer_text_new ();
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeview_info), -1, "Name", window->renderer, "text", 0, NULL);
    gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (window->treeview_info), -1, "Name1", window->renderer, "text", 1, NULL);
    
    gtk_tree_view_set_headers_visible(window->treeview_info, FALSE);
	
	gtk_tree_view_set_model(window->treeview_info, GTK_TREE_MODEL (window->liststore_info));
	
	window->menu_bar = gtk_menu_bar_new();
	
	window->menu_dialogs = gtk_menu_new();
	window->menu_item_dialogs = gtk_menu_item_new_with_label("Dialogs");
	
	window->menu_item_dialogs_flash = gtk_check_menu_item_new_with_label("Flash");
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (window->menu_item_dialogs_flash), TRUE);
	g_signal_connect (window->menu_item_dialogs_flash, "toggled", G_CALLBACK (menu_item_flash_toggled), window);
	
	window->menu_item_dialogs_registers = gtk_check_menu_item_new_with_label("Registers");
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (window->menu_item_dialogs_registers), TRUE);
	g_signal_connect (window->menu_item_dialogs_registers, "toggled", G_CALLBACK (menu_item_registers_toggled), window);
	
	window->menu_item_dialogs_sources = gtk_check_menu_item_new_with_label("Sources");
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (window->menu_item_dialogs_sources), TRUE);
	g_signal_connect (window->menu_item_dialogs_sources, "toggled", G_CALLBACK (menu_item_sources_toggled), window);
	
	window->menu_item_dialogs_inputs = gtk_check_menu_item_new_with_label("Inputs");
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (window->menu_item_dialogs_inputs), TRUE);
	g_signal_connect (window->menu_item_dialogs_inputs, "toggled", G_CALLBACK (menu_item_inputs_toggled), window);
	
	window->menu_item_dialogs_stack = gtk_check_menu_item_new_with_label("Stack");
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (window->menu_item_dialogs_stack), TRUE);
	g_signal_connect (window->menu_item_dialogs_stack, "toggled", G_CALLBACK (menu_item_stack_toggled), window);
	
	window->menu_item_dialogs_vcd = gtk_check_menu_item_new_with_label("VCD");
	//gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (window->menu_item_dialogs_vcd), TRUE);
	g_signal_connect (window->menu_item_dialogs_vcd, "toggled", G_CALLBACK (menu_item_vcd_toggled), window);
	
	window->menu_item_dialogs_variables = gtk_check_menu_item_new_with_label("Variables");
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM (window->menu_item_dialogs_variables), TRUE);
	g_signal_connect (window->menu_item_dialogs_variables, "toggled", G_CALLBACK (menu_item_variables_toggled), window);
	
	window->menu_dialogs_plugins = gtk_menu_new();
	window->menu_item_dialogs_plugins = gtk_menu_item_new_with_label("Plugins");
	
	window->menu_debugging = gtk_menu_new();
	window->menu_item_debugging = gtk_menu_item_new_with_label("Debugging");
	window->menu_layout = gtk_menu_new();
	window->menu_item_layout = gtk_menu_item_new_with_label("Layout");
	window->menu_item_layout_save = gtk_menu_item_new_with_label("Save Layout");
	g_signal_connect(window->menu_item_layout_save, "activate", G_CALLBACK (menu_item_layout_save_activated), window);
	window->menu_item_layout_save_as = gtk_menu_item_new_with_label("Save Layout As");
	g_signal_connect(window->menu_item_layout_save_as, "activate", G_CALLBACK (menu_item_layout_save_as_activated), window);
	window->menu_help = gtk_menu_new();
	window->menu_item_help = gtk_menu_item_new_with_label("Help");

	
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(window->menu_item_dialogs), window->menu_dialogs);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs), window->menu_item_dialogs_flash);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs), window->menu_item_dialogs_registers);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs), window->menu_item_dialogs_sources);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs), window->menu_item_dialogs_inputs);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs), window->menu_item_dialogs_stack);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs), window->menu_item_dialogs_vcd);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs), window->menu_item_dialogs_variables);
	
	gtk_menu_item_set_submenu(GTK_MENU_ITEM (window->menu_item_dialogs_plugins), window->menu_dialogs_plugins);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs), window->menu_item_dialogs_plugins);
	
	gtk_menu_item_set_submenu(GTK_MENU_ITEM (window->menu_item_layout), window->menu_layout);
	populate_layout_menu(window);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_layout), gtk_separator_menu_item_new());
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_layout), window->menu_item_layout_save);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_layout), window->menu_item_layout_save_as);
	
	
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_bar), window->menu_item_dialogs);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_bar), window->menu_item_debugging);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_bar), window->menu_item_layout);
	gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_bar), window->menu_item_help);
	
	
	// Buttons
	window->button_start_stop = gtk_button_new();
	GtkWidget* icon_start = gtk_image_new_from_file("../data/icon_start.png");
	gtk_button_set_image(GTK_BUTTON (window->button_start_stop), icon_start);
	gtk_widget_set_tooltip_text(window->button_start_stop, "Start (F1)");
	g_signal_connect (window->button_start_stop, "clicked", G_CALLBACK (button_start_stop_pressed), window);

	
	window->button_debugging_on_off = gtk_button_new();//_from_icon_name("user-invisible", GTK_ICON_SIZE_LARGE_TOOLBAR);
	GtkWidget* icon_debugging_on = gtk_image_new_from_file("../data/icon_bp_inactive.png");
	gtk_button_set_image(GTK_BUTTON (window->button_debugging_on_off), icon_debugging_on);
	gtk_widget_set_tooltip_text(window->button_debugging_on_off, "Turn Debugging On (F2)");
    g_signal_connect (window->button_debugging_on_off, "clicked", G_CALLBACK (button_debugging_on_off_pressed), window);
	
	window->button_step = gtk_button_new();
	GtkWidget* icon_step = gtk_image_new_from_file("../data/icon_step.png");
	gtk_button_set_image(GTK_BUTTON (window->button_step), icon_step);
	gtk_widget_set_tooltip_text(window->button_step, "Step (F3)");
	g_signal_connect (window->button_step, "clicked", G_CALLBACK (button_step_pressed), window);
	
	window->button_continue = gtk_button_new();
	GtkWidget* icon_continue = gtk_image_new_from_file("../data/icon_continue.png");
    gtk_button_set_image(GTK_BUTTON (window->button_continue), icon_continue);
	gtk_widget_set_tooltip_text(window->button_continue, "Continue (F4)");
	gtk_widget_set_sensitive (window->button_continue, FALSE);
	g_signal_connect (window->button_continue, "clicked", G_CALLBACK (button_continue_pressed), window);
	
	window->button_reset = gtk_button_new_from_icon_name("view-refresh", GTK_ICON_SIZE_LARGE_TOOLBAR);
	gtk_widget_set_tooltip_text(window->button_reset, "Reset (F5)");
	g_signal_connect (window->button_reset, "clicked", G_CALLBACK (button_reset_pressed), window);
	
	window->button_program = gtk_button_new_from_icon_name("insert-object", GTK_ICON_SIZE_LARGE_TOOLBAR);
	gtk_widget_set_tooltip_text(window->button_program, "Program");
	g_signal_connect (window->button_program, "clicked", G_CALLBACK (button_program_pressed), window);
	
	window->button_settings = gtk_button_new_from_icon_name("emblem-system", GTK_ICON_SIZE_LARGE_TOOLBAR);
	gtk_widget_set_tooltip_text(window->button_settings, "Settings");
	g_signal_connect (window->button_settings, "clicked", G_CALLBACK (button_settings_pressed), window);
	
	window->button_views = gtk_menu_button_new();
	//g_object_set (button_views, "icon-name", "window-new", NULL);
	
	window->button_box = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_button_box_set_layout(GTK_BUTTON_BOX (window->button_box), GTK_BUTTONBOX_CENTER);
	
	gtk_container_add (GTK_CONTAINER (window->button_box), window->button_start_stop);
	gtk_container_add (GTK_CONTAINER (window->button_box), window->button_debugging_on_off);
	gtk_container_add (GTK_CONTAINER (window->button_box), window->button_step);
	gtk_container_add (GTK_CONTAINER (window->button_box), window->button_continue);
	gtk_container_add (GTK_CONTAINER (window->button_box), window->button_reset);
	gtk_container_add (GTK_CONTAINER (window->button_box), window->button_program);
	gtk_container_add (GTK_CONTAINER (window->button_box), window->button_settings);






	
	window->expander = gtk_expander_new("MCU Information");
	gtk_expander_set_expanded(window->expander, TRUE);
	g_signal_connect(window->expander, "activate", G_CALLBACK (expander_info_activated), window);
	
	gtk_container_add(GTK_CONTAINER (window->expander), window->treeview_info);
	
	
	
	
	
	
	window->hpaned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
	gtk_paned_add1(GTK_PANED (window->hpaned), window->expander);
	gtk_paned_add2(GTK_PANED (window->hpaned), window->dock);


    window->dock_stack = dock_stack_create_stack_dock(window->microcontroller, window->dock);
    gdl_dock_add_item(window->dock, window->dock_stack->dock_item, GDL_DOCK_LEFT);	
	
    window->window_flash = window_flash_create_flash_window(window->microcontroller);
    gdl_dock_add_item(window->dock, window->window_flash->dock_item, GDL_DOCK_CENTER);
    
    
    window->window_source = window_source_create_source_window(window->microcontroller);
    gdl_dock_add_item(window->dock, window->window_source->dock_item, GDL_DOCK_RIGHT);

    
    window->window_registers = window_registers_create_registers_window(window->microcontroller);
    gdl_dock_add_item(window->dock, window->window_registers->dock_item, GDL_DOCK_LEFT);
    
    
    window->window_inputs = window_inputs_create_inputs_window(window->microcontroller);
    gdl_dock_add_item(window->dock, window->window_inputs->dock_item, GDL_DOCK_BOTTOM);
    
    window->dock_vcd = dock_vcd_create_dock(window->microcontroller);
    gdl_dock_add_item(window->dock, window->dock_vcd->dock_item, GDL_DOCK_CENTER);
    
    window->dock_variables = dock_variables_create_dock(window->microcontroller);
    gdl_dock_add_item(window->dock, window->dock_variables->dock_item, GDL_DOCK_LEFT);
    
    
    
    gtk_widget_add_events(window->window, GDK_KEY_PRESS_MASK);
    g_signal_connect (G_OBJECT (window->window), "key_press_event", G_CALLBACK (on_key_press), window);
    
    
    window->statusbar = gtk_statusbar_new();
    
	//window->vpaned = gtk_paned_new(GTK_ORIENTATION_VERTICAL);
	//gtk_paned_add1(GTK_PANED (window->vpaned), window->hpaned);
	//gtk_paned_add2(GTK_PANED (window->vpaned), window->menu_bar);
	
	window->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER (window->vbox), window->menu_bar);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->button_box);
	gtk_container_add(GTK_CONTAINER (window->vbox), window->hpaned);
	gtk_container_add(GTK_CONTAINER (window->vbox), window->statusbar);
		
	gtk_container_add(GTK_CONTAINER (window->window), window->vbox);
	
	
	g_timeout_add(context->refresh_rate, G_CALLBACK (update_microcontroller_window), window);
	
	gtk_window_set_icon_from_file(window->window, "../data/flame_icon.png", NULL);
	
	gtk_window_maximize(window->window);
	
	gtk_widget_show_all (window->window);
	
	gdl_dock_item_hide_item(window->dock_vcd->dock_item);
	
	//gtk_paned_set_position(GTK_PANED (window->vpaned), gdk_screen_height() - 100);
	
	
	int i = 0;
	for(i; i<microcontroller->plugin_manager->n_plugins; i++)
	{
	    plugin_container_t* container = microcontroller->plugin_manager->plugins[i];
	    
	    container->button = gtk_check_menu_item_new_with_label(container->plugin->name);
	    g_signal_connect(container->button, "toggled", G_CALLBACK (menu_item_plugin_toggled), container);
	    
	    gtk_widget_show_all(GTK_WIDGET (container->plugin->dock_item));
	    
	    gdl_dock_add_item(window->dock, container->plugin->dock_item, GDL_DOCK_CENTER);
        gdl_dock_item_hide_item(container->plugin->dock_item);
        container->dock_item_hidden = 1;
	    
	    if (container->is_registered)
	    {
	        container->plugin->on_register(container->plugin);
	        gtk_widget_show(container->button);
	    }
	    
	    
        container->parent_window = window;
        gtk_menu_shell_append(GTK_MENU_SHELL(window->menu_dialogs_plugins), container->button);
        
	}
	
	return window;
}
