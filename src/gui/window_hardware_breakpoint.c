/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_hardware_breakpoint.c

*/


#include "gui/window_hardware_breakpoint.h"

#include "registers.h"
#include "debug.h"
#include "globals.h"
#include "util.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

context_t* context;


void window_hardware_breakpoint_button_ok_pressed(GtkButton* button, window_hardware_breakpoint_t* window)
{
    uint8_t val = (uint8_t) strtol(gtk_entry_get_text(GTK_ENTRY (window->entry_comparison_value)), 0, 16);
    
    add_hardware_breakpoint(window->microcontroller->debugger, window->reg, window->reg_name, gtk_combo_box_get_active(GTK_COMBO_BOX (window->combo_comparison)), val);
    
    GtkTreeModel* model = GTK_TREE_MODEL (window->tree_store);
    GtkTreeIter tree_iter;
    gtk_tree_model_get_iter(model, &tree_iter, window->tree_path);
    
    GtkImage* icon_bp_active = gtk_image_new_from_file("../data/icon_bp_active.png");
	GdkPixbuf* pixbuf_bp_active = gtk_image_get_pixbuf(icon_bp_active);
	
	GtkImage* icon_bp_inactive = gtk_image_new_from_file("../data/icon_bp_inactive.png");
	GdkPixbuf* pixbuf_bp_inactive = gtk_image_get_pixbuf(icon_bp_inactive);
    
    char bin[9];
	char hex[8];
	char comparison[16];
	char* comparison_types[6] = {"<", "<=", ">", ">=", "==", "~"};
	
    
    decToBin(*(window->reg), bin, 8);
	sprintf(hex, "0x%02X", *(window->reg));
	sprintf(comparison, "%s 0x%02X", comparison_types[gtk_combo_box_get_active(GTK_COMBO_BOX (window->combo_comparison))], val);
	
    gtk_tree_store_set(GTK_TREE_STORE(window->tree_store), &tree_iter, 0, window->reg_name, 1, bin, 2, hex, 3, pixbuf_bp_active, 4, comparison, -1);
    
    //gtk_tree_store_set_value(window->tree_store, &tree_iter, 2, "test");
    
    gtk_window_close(GTK_WINDOW (window->window));
}

window_hardware_breakpoint_t* window_hardware_breakpoint_create_register_window(uint8_t* reg, char* reg_name, microcontroller_t* microcontroller, GtkTreeStore* tree_store, GtkTreePath* tree_path)
{
    window_hardware_breakpoint_t* window = (window_hardware_breakpoint_t*) malloc(sizeof(window_hardware_breakpoint_t));
    window->reg = reg;
    window->reg_name = reg_name;
    window->microcontroller = microcontroller;
    window->tree_store = tree_store;
    window->tree_path = tree_path;

    window->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size (GTK_WINDOW (window->window), 300, 100);
    char title[64];
    sprintf(title, "Hardware Breakpoint %s", reg_name);
    gtk_window_set_title(GTK_WINDOW (window->window), title);

    window->vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);	

    window->label_comparison = gtk_label_new("Comparison Type:");
    g_object_set (window->label_comparison, "xalign", 0.05, NULL);

    window->label_comparison_value = gtk_label_new("Comparison Value:");
    g_object_set (window->label_comparison_value, "xalign", 0.05, NULL);

    window->combo_comparison = gtk_combo_box_text_new();
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_comparison), "<  - Less Than");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_comparison), "<= - Less Than or Equal To");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_comparison), ">  - Greater Than");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_comparison), ">= - Greater Than or Equal To");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_comparison), "== - Equal To");
    gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (window->combo_comparison), "~  - Changed");
    gtk_combo_box_set_active(window->combo_comparison, 0);

    window->entry_comparison_value = gtk_entry_new();
    gtk_entry_set_placeholder_text (GTK_ENTRY (window->entry_comparison_value), "Compare Against");
    
    //window->check_button_breakpoint_active = gtk_check_button_new_with_label("Breakpoint Active");

    window->button_ok = gtk_button_new_with_label("OK");
    g_signal_connect (window->button_ok, "clicked", G_CALLBACK (window_hardware_breakpoint_button_ok_pressed), window);

    gtk_container_add(GTK_CONTAINER (window->vbox), window->label_comparison);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->combo_comparison);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->label_comparison_value);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->entry_comparison_value);
    gtk_container_add(GTK_CONTAINER (window->vbox), window->button_ok);

    gtk_container_add(GTK_CONTAINER (window->window), window->vbox);

    int width, height;
	    gtk_window_get_size(window->window, &width, &height);

    width = (gdk_screen_width() - width) / 2;
    height = (gdk_screen_height() - height) / 2;

    gtk_window_move(window->window, width, height);

    gtk_widget_show_all (window->window);  

    //g_timeout_add(context->refreshRate, G_CALLBACK (window_hardware_breakpoint_update_change_register_window), window);

    //gtk_widget_show_all (window->window);

    return window;
}
