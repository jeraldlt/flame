#include <signal.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "microcontroller.h"
#include "instructions.h"
#include "print_instructions.h"
#include "util.h"

#include "mcus/atmega328p.h"

// Custom assert function
static int passed = 0;
static int failed = 0;
#define STR(x) #x
#define ASSERT(x) if (!(x)) { printf("... %s \x1B[31mFailed\x1B[0m (%s:%d)\n", STR(x), __PRETTY_FUNCTION__, __LINE__); failed++;} else { printf("... %s \x1B[32mSucceded!\x1B[0m\n", STR(x)); passed++; }

// Number of tests in tests array
#define N_TESTS 11

void test_reset_microcontroller(microcontroller_t* microcontroller)
{
	if (strcmp(microcontroller->model, "atmega328p") == 0)
	{
		atmega328p_populate_microcontroller(microcontroller, 1);
	}
}

void test_step_microcontroller(microcontroller_t* microcontroller)
{
	char buf[256];

	uint16_t instruction = swapBytes(microcontroller->flash->data[*(microcontroller->registers->PC)]);

	if (print_opcodes[instruction] > 0)
	{
		(*print_opcodes[instruction])(microcontroller, buf);
		printf("\nExecuting %s\n", buf);
	}

	else if (*opcodes[instruction] == 0)
	{
		printf("Trying to execute 0; PC=0x%X\n", *(microcontroller->registers->PC) * 2);
	}
	(*opcodes[instruction])(microcontroller);
}

void test_and(microcontroller_t* microcontroller)
{
    microcontroller->flash->data[0] = 0x6721; // and d=22, r=7
    microcontroller->registers->r[7] = 7;
    microcontroller->registers->r[22] = 22;
    
	test_step_microcontroller(microcontroller);
    
    ASSERT(microcontroller->registers->r[22] == (7 & 22));
	ASSERT(*(microcontroller->registers->SREG) == 0x00);
}

void test_bclr(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0xA894; // Instruction and arguments

	*(microcontroller->registers->SREG) = 0x82;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0x80);
}

void test_cli(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0xF894; // cli

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0x7F);
}

void test_clc(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x8894; // clc

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0xFE);
}

void test_clh(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0xD894; // clh

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0xDF);
}

void test_cln(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0xA894; // cln

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0xFB);
}

void test_cls(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0xC894; // cls

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0xEF);
}

void test_clt(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0xE894; // clt

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0xBF);
}

void test_clv(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0xB894; // Instruction and arguments

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0xF7);
}

void test_clz(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x9894; // Instruction and arguments

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0xFD);
}

void test_sei(microcontroller_t* microcontroller)
{
    microcontroller->flash->data[0] = 0x7894; // sei
    
	test_step_microcontroller(microcontroller);
    
    ASSERT(*(microcontroller->registers->SREG) == 0x80);
}

void (*tests[N_TESTS])(microcontroller_t* microcontroller) = {
	test_and,
	test_bclr,
	test_cli,
	test_clt,
	test_clh,
	test_cls,
	test_clv,
	test_cln,
	test_clz,
	test_clc,
	test_sei,
};

int run_tests(microcontroller_t* microcontroller)
{
	

	int i = 0;
	for (i; i < N_TESTS; i++)
	{
		(*tests[i])(microcontroller);
		test_reset_microcontroller(microcontroller);
	}
    
    printf("\n[%s] \x1B[32m%i\x1B[0m instruction tests passed; \x1B[31m%i\x1B[0m instruction tests failed\n", microcontroller->model, passed, failed);

	if (failed > 0)
		return 1;
	return 0;
}

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		printf("usage: ./%s [microcontroller] [clock]\n   example: ./%s atmega328p 16\n", argv[0], argv[0]);
		return 1;
	}

	int clock = atoi(argv[2]);
	if (clock == 0)
	{
		printf("usage: ./%s [microcontroller] [clock]\n   example: ./%s atmega328p 16\n", argv[0], argv[0]);
		return 1;
	}

	microcontroller_t* microcontroller = (microcontroller_t*)malloc(sizeof(microcontroller_t));
	microcontroller->clock_mhz = clock;
	microcontroller->n_messages = 0;

	if (strcmp(argv[1], "atmega328p") == 0)
	{
		atmega328p_populate_microcontroller(microcontroller, 0);
	}
	else
	{
		printf("usage: ./%s [microcontroller] [clock]\n   example: ./%s atmega328p 16\n", argv[0], argv[0]);
		return 1;
	}

	return run_tests(microcontroller);
}

//----------------------------------------------------------------------------------------------------------
//                                                                    Instruction tests to be implemented
//==========================================================================================================

void test_adc(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_add(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_adiw(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_andi(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_asr(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_bld(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brbc(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brbs(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brcc(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brcs(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_break(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_breq(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brge(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brhc(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brhs(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brid(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brie(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brlo(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brlt(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brmi(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brne(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brpl(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brsh(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brtc(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brts(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brvc(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_brvs(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_bset(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_bst(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_call(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_clr(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments

	*(microcontroller->registers->SREG) = 0xFF;

	test_step_microcontroller(microcontroller);

	ASSERT(*(microcontroller->registers->SREG) == 0x7F);
}

void test_cbi(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_cbr(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_com(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_cp(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_cpc(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_cpi(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}

void test_cpse(microcontroller_t* microcontroller)
{
	microcontroller->flash->data[0] = 0x0000; // Instruction and arguments



	test_step_microcontroller(microcontroller);

	ASSERT(0 == 0);
}