## Plugins:

This is a list of plugins that will be developed and maintained by the core Flame development team.

---

#### Motor with encoder feedback

This plugin will provide a dock with a visual representation of a motor. The simulated motor will be able to hook up to the microcontroller's PWM and PCINT pins in order to drive the motor and provide encoder feedback.

#### Virtual serial terminal

This plugin will provide a system serial device attached to the emulator (ie. ACMx, TTY_ACMx, COMx) that a system terminal can connect to. 