# Flame Roadmap

This is the current roadmap for Flame out to version 1.0. The goal of version 1.0 is to be fully featured emulator for all of the ATMega and ATTiny microcontrollers with support for Linux, Windows, and Mac OSX. I would like version 1.0 to be able to run in real time at an emulated 16 to 20 MHz (which from current testing is possible without intensive optomizaiton). 

Each version below has a version number, a main purpose of the version, and a list of features.

---
### Version 0.1 - Proof of concept version
- Mostly implemented instruction set
- Supported Microcontrollers:
    - ATMega328p
- Timer counter modes:
    - Normal
    - CTC
    - Fast PWM
- ADC modes:
    - Single ended
- Debugging:
    - Firmware breakpoints
    - Hardware breakpoints
- VCD
    - Single bit recording
- Docks:
    - Flash
    - Stack
    - Sources
    - Registers
    - Inputs
    - VCD
- Emulation modes:
    - Interpreted

### Version 0.2 - Add plugin capability
- Plugin architecture

### Version 0.3 - Flush out debugging
- Debugging:
    - Data breakpoint
    - Backstepping
- Docks:
    - Variables

### Version 0.4
- Supported microcontrollers:
    - ATMega32U4
    - ATTiny85
- Hardware communication modes:
    - SPI
    - TWI

### Version 0.5 - Multiple microcontroller support
- Multiple microcontrollers

### Version 0.6 - Flush out timer/counters and analog
- Analog comparator
- Analog
    - ADC Voltage reference selection
    - ADC Differential mode
    - ADC gain stage
    - DAC
    - Temperature sensor
- Timer counter modes
    - Phase correct PWM
    - Phase and frequency correct PWM
    - Input capture

### Version 0.7 - Finish instruction set
- Instruction set
    - Watchdog instructions
    - Self programming instructions
    - Sleep instructions
- Sleep mode
- Watchdog

### Version 0.8 - Finish communications
- Hardware communication modes:
    - UART
    - USART
    - USB

### Version 0.9 - Add more supported microcontrollers
- Supported Microcontrollers:
    - All ATMega microcontrollers
    - All ATTiny microcontrollers

### Version 1.0 - Cross platform support
- Cross platform support:
    - Windows
    - MacOS
    
---

## Future Plans

By version 2.0 I would like flame to be able to run in real time at much higher emulated clock rates. This means that a lot of optimization will need to happen. Likely, this alone will not be enough and recompilation will need to be implemented. I would also like to add support for the ATMegaX family of microcontrollers.

## Far Future Plans

I would eventually like add support for other types of microcontrollers, namely PICs and ARMs.
