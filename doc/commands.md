# GLAED Commands

## Microcontroller Control

### addmcu [str:model] [int:speedMhz]
`example: addmcu atmega328p 16`

Add a mcu of type model and clockspeed clockMhz

### program [int:mcu] [str:filePath]
`example: program 0 blink.elf`

Programs the corresponding mcu with the given file. The file can be either ihex or elf format. If the file an elf file then dwarf debugging symbols will be loaded if present.

### reset ([int:mcu])
`example: reset 0`

Resets all volatile memory for the corresponding mcu. If mcu is not provided all mcus will be reset.

### mcu [int:mcu]
`example: mcu 0`

Switch forcus to the corresponding mcu

### setreg [str:reg] [hex:value]
`example: setreg pinb 0x01`

Sets the provided register to the provided value. All register names are all lowercase.

### input [str:key] [int:mcu][char:port][int:bit] _I am going to change this_
```
example: input f5 0b0
example: input sf7 0c7
```

---

## GUI Control

### show [left|right] [str:window] ([str:sourceFile])
```
example: show left flash
example: show right source Blink.c
```

Displays the provided window on the provided panel. If the window is "source" then a third argument (sourceName) must be provided and the source must be in the loaded debug symbols.

Possible windows: help, about, flash, stack, data, sources, source.

### help
Shows the help window in the left panel.

### about
Shows the about panel in the left panel.

---

## Emulation Control

### load [str:script]
`example: load test.glaed`

Runs the commands in the corresponding script line by line

### save [str:filePath]
`example: save test.glaed`

Saves the session command history to the provided file path.

### run
Runs all mcus.

### stop
Stops all mcus.

### quit
Exits this program. Ctrl+q will do the same.

---

## Debugging

### debugging [on|off]
`example: debugging on`

Turns debugging either on or off. If off the emulator will not break at a breakpoint.

### addbp [str:sourceFile] [int:lineNum]
`example: addbp Blink.c 24`

Adds a breakpoint to the memory location coresponding with the source file and line number. Debug symbols must be loaded.

### addbp [hex:location]
`example: addbp 0xF4`

Adds a breakpoint to the memory location in flash.

### clearallbp
Clears all breakpoints.

### step
Execute next instruction.

### continue
Continue running after breakpoint.

---

## VCD (Value Change Dump)

### vcd record [int:mcu] [str:reg] [int:bit]
`example: vcd record 0 portb 7`

The system will record any changes of the corresponding bit of the corresponding register of the corresponding mcu. All register names are all lowercase.

### vcd save [str:filePath]
`example: vcd save out.vcd`

Save all recorded bit changes to a vcd file.








