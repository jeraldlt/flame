## Warning: Flame is still under heavy active devolpment. It is probably safe to assume that it is just always broken :)

# Flame (Free Linux Avr Microcontroller Emulator)

Flame is a free and open source emulated testing environment for AVR microcontrollers. Designed for the Linux operating system (though developed with porting in mind), flame is not a replacement for your AVR development toolchain, but rather a link to add to your current toolchain.

# Why did you develop flame?

To put it simply, I wanted to learn how to design and implement an emulator. I work with AVRs on a semi-regular basis and wanted a better development cycle, so they were the obvious choice. Also, being 8 bit microcontrollers, they seemed to be the easiest to build an emulator for first.

# What is an emulator?

According to the Wikipedia page on emulators (<https://en.wikipedia.org/wiki/Emulator>):

> In computing, an emulator is hardware or software that enables one computer system (called the host) to behave like another computer system (called the guest). An emulator typically enables the host system to run software or use peripheral devices designed for the guest system.

Essentially, an emulator replicates all aspects of a guest system (CPU, memory, etc.). This allows software compiled for the guest system (AVR microcontrollers) to run on the host system (flame). A simple example would be the game Pac-Man. If you want to play Pac-Man on a PC you have two options. The first is to create a game that looks and behaves like the game Pac-Man, only written for the PC architecture. This would be simulation. The second is to write software that can read the Pac-Man program from an arcade cabinet or an Atari game, run the program, display the graphics from the program, and feed user input to the program. This would be emulation.


# Why use an emulator instead of a simulator?

Simulators seek to replicate the behavior or a target system, but not necessarily the the process to achieve that behavior. 

1. Simulators don't emulate system memory. With an emulator you can pause at any time and see exactly what is in the emulated memory, which is very helpful for debugging. 

2. Simulators require additional steps in the compilation/linking process. Simulators essentilly replace the AVR libraries with x86 libraries that have the same API, and compile an executable that runs on x86 architecture. Emulators take the AVR executable and either interpret or recompile it to run on the x86 system. In other words the file that you use to program your AVR (.ihex or .elf) is the exact same one that you give to the emulator.

3. Simulators can't simulate AVR timing. Because simulators compile to a different instruction set, bits of code may take more or less cycles than the compiled AVR code.

4. Simulators make it difficult to interact with the simulated system in a real-time manner. 


# How do I build flame?

*First*, you need a Linux development environment (hopefully soon flame will be ported to OSX and Windows). Second you need all of the prerequisite libraries and development files installed:

1. git
2. CMake
3. GTK+ 3.0
4. GTK sourceview 4.0

For a Debian based platform, installing these dependencies may look like this:
```
sudo apt-get update
sudo apt-get install git cmake libgtk-3-0
```
Unfortunately, GTK sourceview 4.0 is not in the Ubuntu repositories (last I checked). GTK sourceview 3.0 is, but I have not tested that to make sure that it works. You can either download the gtksourceview 4.0 source and build it or try the 3.0 version.
```
sudo apt-get install libgtksourceview-3.0-dev
```

For an Arch based platform, installing these dependencies may look like this:
```
sudo pacman -S git cmake gtk3 gtksourceview4
```

*Second*, clone this repository:

```
git clone https://bitbucket.org/jeraldlt/flame.git
```

*Third*, build the emulator:

```
cd flame
mkdir build
cd build
cmake ..
make
```

# How do I use flame?

Flame has two main functions: interacting with the emulated microcontroller, and debugging firmware on the emulated microcontroller.

## Interacting with the emulated microcontroller

Add basic description of how to use flame.


## Debugging with the emulated microcontroller

The major feature that Flame implements for debugging is breakpoints, of which there are three types (two currently implemented).

### Firmware Breakpoints

Firmware breakpoints are the most similar to the kind of breakpoints that most people are familiar with. A firmware breakpoint will pause execution when the program counter reaches a specfic value (when a specific address in the flash is about to be executed). To activate a firmware breakpoint, press the grey square to the left of the address in the flash dock (an active breakpoint will have a red center). If the firmware has embedded debug symbols, you can also activate firmware breakpoint from the sources dock.

### Hardware Breakpoints

Hardware breakpoints pause execution when a hardware register meets some condition. For example, a hardware breakpoint can be set to trigger whenever TCNT0 (timer/counter 0 count register) is greater than some value. To set a hardware breakpoint, right click on the register in the registers dock and choose "Hardware Breakpoint", and choose a comparison and comparison value. The first five comparisons (<, <=, >, >=, ==) compare the register value directly with the comparison value, and trigger when the comparison is true. The last comparison (~) uses the comparison value as a bit mask and triggers when any of the un-masked bits of the register change value.

### Data Breakpoints (not yet implemented)

Data breakpoints pause execution when a source code variable meets some condition.

# Docks

Add basic description of the docks.

---

# How do I contribute?

If you would like to contribute by programming, shoot me a message and we can work something out. My plan is to get flame up to version 1.0 (see roadmap) before soliciting for contribution, but if you want to help out I am happy to make that work. There are other ways to contribute as well, such as testing, documentation, and packaging.

Another possible way to contribute is to write plugins. I am currently working on a plugin architecture and hope to have it implemented soon. If you have a feature idea that is neat, but may not warrant being implemented in the core framework, it would probably work great as a plugin.

If you have a cool or interesting feature idea, I would love to hear that too. I am interested in hearing the community feedback regarding the future of flame. The short term roadmap can be found [here](doc/roadmap.md). Nothing here is solid, it is just what I have been working off of.
