/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

memory.h
    
*/


#ifndef FLAME_MEMORY_H
#define FLAME_MEMORY_H

#include <stdint.h>

#include "types.h"

struct flash_t
{
    char* name;    // name of file used to program flash
    uint32_t size;
    int start;
    int end;
    uint16_t* data;
};

struct sram_t
{
    uint32_t size;
    uint8_t* data;
};

struct eeprom_t
{
    uint32_t size;
    uint8_t* data;
};

//
// Initializes a flash pointer and returns it.
//
flash_t* init_flash(const uint32_t size);

//
// Recursively frees the flash pointer
//
void free_flash(flash_t* flash);

//
// Initializes a sram pointer and returns it.
//
sram_t* init_sram(const uint32_t size);

//
// Recursively frees the sram pointer
//
void free_sram(sram_t* sram);

//
// Initializes a eeprom pointer and returns it.
//
eeprom_t* init_eeprom(const uint32_t size);

//
// Recursively frees the eeprom pointer
//
void free_eeprom(eeprom_t* eeprom);


#endif
