/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

adc.h

*/


#ifndef FLAME_ADC_H
#define FLAME_ADC_H

#include "microcontroller.h"

struct adc_channel_t
{
    uint8_t channel_num;
    uint16_t value;
    
};
void handle_adc_single_ended_only(microcontroller_t* microcontroller);

#endif
