/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

interrupts.h
    
*/

#ifndef FLAME_INTERRUPTS_H
#define FLAME_INTERRUPTS_H

#include "types.h"
#include "microcontroller.h"
#include "timers.h"

#include <stdint.h>

struct interrupt_t
{
    void (*handler) (interrupt_t* interrupt, microcontroller_t* microcontroller);
    
    // pin change interrupt variables
    uint8_t* pcmsk;
    uint8_t* pin;
    uint8_t pin_last;
    uint8_t flag_mask;
    uint16_t pin_change_vector;
    
    // timers interrupt variables
    timer_counter_t* timer;
    uint16_t compare_a_vector;
    uint16_t compare_b_vector;
    uint16_t overflow_vector;
    uint16_t capture_vector;
    
};

interrupt_t* init_interrupt();

interrupt_t* init_pin_change_interrupt(uint8_t* pcmsk, uint8_t* pin, uint8_t flag_mask, uint16_t vector);

interrupt_t* init_timer_interrupt(timer_counter_t* timer, uint16_t compare_a, uint16_t compare_b, uint16_t overflow, uint16_t capture);

void free_interrupt();

void pin_change_interrupt_handler(interrupt_t* interrupt, microcontroller_t* microcontroller);

void timer_interrupt_handler(interrupt_t* interrupt, microcontroller_t* microcontroller);

#endif
