/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

atmega328p.h

*/


#ifndef FLAME_ATMEGA328P_H
#define FLAME_ATMEGA328P_H

#include <stdint.h>
#include "types.h"


void atmega328p_populate_microcontroller(microcontroller_t* microcontroller, uint8_t reset);

void atmega328p_populate_registers(registers_t* registers, sram_t* sram);

void atmega328p_populate_vectors(microcontroller_t* microcontroller);

void atmega328p_handle_timers(microcontroller_t* microcontroller);

void atmega328p_handle_adc(microcontroller_t* microcontroller);

void atmega328p_handle_interrupts(microcontroller_t* microcontroller);

#endif
