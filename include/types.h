/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

types.h

This file contains definitions for all data structure types.
*/

#ifndef FLAME_TYPES_H
#define FLAME_TYPES_H

typedef struct context_t context_t;

typedef struct connection_t connection_t;

typedef struct microcontroller_t microcontroller_t;

typedef struct registers_t registers_t;

typedef struct debugger_t debugger_t;

typedef struct firmware_breakpoint_t firmware_breakpoint_t;

typedef struct hardware_breakpoint_t hardware_breakpoint_t;

typedef struct data_breakpoint_t data_breakpoint_t;

typedef struct debug_type_t debug_type_t;

typedef struct debug_line_t debug_line_t;

typedef struct debug_location_t debug_location_t;

typedef struct debug_variable_t debug_variable_t;

typedef struct debug_subprogram_t debug_subprogram_t;

typedef struct debug_source_t debug_source_t;

typedef struct flash_t flash_t;

typedef struct sram_t sram_t;

typedef struct eeprom_t eeprom_t;

typedef struct options_t options_t;

typedef struct timer_counter_t timer_counter_t;

typedef struct interrupt_t interrupt_t;

typedef struct adc_channel_t adc_channel_t;

typedef struct vcd_variable_t vcd_variable_t;

typedef struct plugin_t plugin_t;

typedef struct plugin_manager_t plugin_manager_t;

typedef struct plugin_container_t plugin_container_t;

#endif
