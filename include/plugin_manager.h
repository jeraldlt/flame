/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

plugin_manager.h
    
*/

#ifndef FLAME_PLUGIN_MANAGER_H
#define FLAME_PLUGIN_MANAGER_H

#include "types.h"
#include "globals.h"
#include "context.h"
#include "microcontroller.h"

#include "gui/types.h"

#include <gtk/gtk.h>

context_t* context;

struct plugin_container_t
{
	plugin_t* plugin;
	uint8_t dock_item_hidden;
	uint8_t is_registered;
	window_microcontroller_t* parent_window;
	GtkWidget* button;
	char config_path[128];
	
};

struct plugin_manager_t
{
	uint8_t n_plugins;
	plugin_container_t* plugins[64];
};

plugin_manager_t* init_plugin_manager(microcontroller_t* microcontroller, context_t* context);

void plugin_manager_populate();

void plugin_manager_on_step();

void plugin_manager_on_quit();

plugin_container_t* load_plugin(plugin_manager_t* manager, const char* path, microcontroller_t* microcontroller, context_t* context);

void generate_plugin_config(const char* path, plugin_container_t* plugin);

void load_plugin_config(const char* path, plugin_container_t* plugin);

void save_plugin_config(const char* path, plugin_container_t* plugin);

#endif
