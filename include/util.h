/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

util.h
    
*/


#ifndef FLAME_UTIL_H
#define FLAME_UTIL_H

#include <string.h>
#include <stdint.h>
#include <stdarg.h>

void sliceStr(const char* str, char* buffer, size_t start, size_t end);

uint32_t hexToDec(const char* hex, uint8_t size);

uint8_t hexCharToDec(const char hex);

void decToBin(uint32_t dec, char* bin, uint8_t nbits);

uint8_t bit(uint32_t val, uint8_t place, uint8_t negate);

void hexToBin(const char* hex, char* bin, uint8_t size);

char* hexCharToBin(const char hex);

char decToHexChar(const int dec);

uint16_t swapBytes(const uint16_t word);

#endif
