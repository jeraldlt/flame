/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

plugin.h
    
*/


#ifndef FLAME_PLUGIN_H
#define FLAME_PLUGIN_H

#include <gtk/gtk.h>
#include <gdl/gdl.h>
#include <stdint.h>

#include "types.h"
#include "context.h"
#include "microcontroller.h"
#include "gui/types.h"
#include "gui/window_microcontroller.h"

struct plugin_t
{
    char* name;
    char* version;
    char* author;
    char* website;
    char* description;
   
    context_t* context;
    microcontroller_t* microcontroller;
   
    void (*on_register)(plugin_t* plugin);
    void (*on_unregister)(plugin_t* plugin);
    void (*on_step)(plugin_t* plugin);
    void (*on_quit)(plugin_t* plugin);
    void (*on_generate_config)(plugin_t* plugin, GKeyFile* file);
    void (*on_load_config)(plugin_t* plugin, GKeyFile* file);
    void (*on_save_config)(plugin_t* plugin, GKeyFile* file);
   
    GtkWidget* dock_item;
   
    void* plugin_data;
   
};

plugin_t* init_plugin(context_t* context, microcontroller_t* microcontroller);

#endif
