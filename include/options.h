/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

options.h

*/


#ifndef FLAME_OPTIONS_H
#define FLAME_OPTIONS_H

#include <stdint.h>
#include <glib.h>
#include "types.h"

struct options_t
{
    // history
    char last_program[256];
    
    // gui
    uint8_t gui_byte_addressing;
    uint8_t gui_theme;
    char* gui_default_theme;
    char gui_last_layout[256];
};

//
// Initializes an options pointer and returns it.
//
options_t* init_options();

//
// Generates a config file at path.
//
void generate_config(char* path);

//
// Saves the config to path.
//
void save_config(char* path);

//
// Loads the config from path
//
void load_config(char* path);

//
// Sets the options struct from config file
//
void set_options(GKeyFile* file);


// Options defines
#define THEME_SYSTEM_DEFAULT 0
#define THEME_LIGHT          1
#define THEME_DARK           2
#define THEME_LIGHT_MONO     3
#define THEME_DARK_MONO      4


#endif
