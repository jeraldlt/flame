/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

context.h

This file pair contains logic to control the context. The context is a signleton
object that contains all of the data for the emulator. Everything that happens
above the microcontroller layer is controlled by the context.
    
*/


#ifndef CONTEXT_H
#define CONTEXT_H

#include "microcontroller.h"
#include "plugin_manager.h"
#include "options.h"
#include "types.h"

struct connection_t
{
    uint8_t* port_out;
    uint8_t  bit_out;
    uint8_t* port_in;
    uint8_t  bit_in;
};

struct context_t
{
    uint8_t n_mcus;                  // Number of MCUs active in context 
    //int curMcu;
    microcontroller_t* mcus[4];      // Array of MCUs
    uint8_t n_connections;           // Number of connections
    connection_t* connections[100];  // Array of connections
    
    uint8_t state;                   // Current state of the system
    uint8_t state_last;              // State of the system last frame
    //uint8_t running;
    uint8_t debugging_enabled;
    //uint8_t quit;
    //uint8_t mcuBreakpoint;
    //uint8_t inBreakpoint;
    //uint8_t continueBreakpoint;
    uint8_t breakpoint_triggered;
    
    //uint8_t interruptRefresh;
    uint32_t refresh_rate;
    //uint32_t curRefresh;
    
    uint8_t vcd_vars_count;           // Number of VCD variables
    
    char config_path[256];
    
    options_t* options;               // System options object
    
};

//
// Initializes a context pointer and returns it.
//
context_t* init_context();

//
// Recursively frees the context pointer.
//
void free_context(context_t* context);

//
// Adds a MCU to the context.
//
microcontroller_t* add_mcu(context_t* context, char* model, uint8_t clock_mhz);

//
// Progresses the context one frame.
// Adds one clock step to each microcontroller.
//
int step(context_t* context);

//
// Writes all of the VCD variables to a VCD file.
//
void save_vcd(context_t* context, char* filepath);

//
// Adds a VCD variable.
//
void add_vcd_variable(context_t* context, uint8_t mcu, uint8_t* byte, char* register_name, uint8_t bit, char* name);

// System state defines
#define STATE_STOPPED        0
#define STATE_RUNNING        1
#define STATE_INDEBUG        2
#define STATE_STEPPING       3
#define STATE_STEPPING_DEBUG 4

#endif
