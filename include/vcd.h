/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

vcd.c
    
*/


#ifndef FLAME_VCD_H
#define FLAME_VCD_H

#include <stdint.h>
#include "types.h"

struct vcd_variable_t
{
    char* name;
    char identifier;
    char* register_name;
    uint8_t* byte;
    uint8_t bit;
    uint8_t last_val;
    uint64_t change_count;
    uint64_t* change_ticks;
    uint8_t* change_vals;
    
};

vcd_variable_t* init_vcd_variable(char* name, char identifier, char* register_name, uint8_t* byte, uint8_t bit);

void vcd_change(vcd_variable_t* vcd, uint64_t ticks);

extern char vcd_identifiers[10];

#endif
