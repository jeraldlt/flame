/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

instructions.h
    
*/


#ifndef FLAME_INSTRUCTIONS_H
#define FLAME_INSTRUCTIONS_H
#include "microcontroller.h"


void _and(microcontroller_t* microcontroller);
void _adc(microcontroller_t* microcontroller);
void _add(microcontroller_t* microcontroller);
void _ori(microcontroller_t* microcontroller);
void _clr(microcontroller_t* microcontroller);
void _rol(microcontroller_t* microcontroller);
void _lsl(microcontroller_t* microcontroller);
void _tst(microcontroller_t* microcontroller);
void _andi(microcontroller_t* microcontroller);
void _asr(microcontroller_t* microcontroller);
void _bclr(microcontroller_t* microcontroller);
void _bld(microcontroller_t* microcontroller);
void _brbc(microcontroller_t* microcontroller);
void _brbs(microcontroller_t* microcontroller);
void _brsh(microcontroller_t* microcontroller);
void _brlo(microcontroller_t* microcontroller);
void _breq(microcontroller_t* microcontroller);
void _brge(microcontroller_t* microcontroller);
void _brhc(microcontroller_t* microcontroller);
void _brhs(microcontroller_t* microcontroller);
void _brid(microcontroller_t* microcontroller);
void _brie(microcontroller_t* microcontroller);
void _brlt(microcontroller_t* microcontroller);
void _brmi(microcontroller_t* microcontroller);
void _brne(microcontroller_t* microcontroller);
void _brpl(microcontroller_t* microcontroller);
void _brtc(microcontroller_t* microcontroller);
void _brts(microcontroller_t* microcontroller);
void _brvc(microcontroller_t* microcontroller);
void _brvs(microcontroller_t* microcontroller);
void _bset(microcontroller_t* microcontroller);
void _bst(microcontroller_t* microcontroller);
void _cbi(microcontroller_t* microcontroller);
void _clc(microcontroller_t* microcontroller);
void _clh(microcontroller_t* microcontroller);
void _cli(microcontroller_t* microcontroller);
void _cln(microcontroller_t* microcontroller);
void _eor(microcontroller_t* microcontroller);
void _cls(microcontroller_t* microcontroller);
void _clt(microcontroller_t* microcontroller);
void _clv(microcontroller_t* microcontroller);
void _clz(microcontroller_t* microcontroller);
void _com(microcontroller_t* microcontroller);
void _cp(microcontroller_t* microcontroller);
void _cpc(microcontroller_t* microcontroller);
void _cpi(microcontroller_t* microcontroller);
void _cpse(microcontroller_t* microcontroller);
void _dec(microcontroller_t* microcontroller);
void _in(microcontroller_t* microcontroller);
void _inc(microcontroller_t* microcontroller);
void _ldx1(microcontroller_t* microcontroller);
void _ldx2(microcontroller_t* microcontroller);
void _ldx3(microcontroller_t* microcontroller);
void _ldy1(microcontroller_t* microcontroller);
void _ldy2(microcontroller_t* microcontroller);
void _ldy3(microcontroller_t* microcontroller);
void _ldy4(microcontroller_t* microcontroller);
void _ldz1(microcontroller_t* microcontroller);
void _ldz2(microcontroller_t* microcontroller);
void _ldz3(microcontroller_t* microcontroller);
void _ldz4(microcontroller_t* microcontroller);
void _ldi(microcontroller_t* microcontroller);
void _lpm1(microcontroller_t* microcontroller);
void _lpm2(microcontroller_t* microcontroller);
void _lpm3(microcontroller_t* microcontroller);
void _lsr(microcontroller_t* microcontroller);
void _mov(microcontroller_t* microcontroller);
void _neg(microcontroller_t* microcontroller);
void _nop(microcontroller_t* microcontroller);
void _or(microcontroller_t* microcontroller);
void _sbr(microcontroller_t* microcontroller);
void _out(microcontroller_t* microcontroller);
void _rcall(microcontroller_t* microcontroller);
void _ret(microcontroller_t* microcontroller);
void _reti(microcontroller_t* microcontroller);
void _rjmp(microcontroller_t* microcontroller);
void _ror(microcontroller_t* microcontroller);
void _sbc(microcontroller_t* microcontroller);
void _sbci(microcontroller_t* microcontroller);
void _sbi(microcontroller_t* microcontroller);
void _sbic(microcontroller_t* microcontroller);
void _sbis(microcontroller_t* microcontroller);
void _sbrc(microcontroller_t* microcontroller);
void _sbrs(microcontroller_t* microcontroller);
void _sec(microcontroller_t* microcontroller);
void _seh(microcontroller_t* microcontroller);
void _sei(microcontroller_t* microcontroller);
void _sen(microcontroller_t* microcontroller);
void _ser(microcontroller_t* microcontroller);
void _ses(microcontroller_t* microcontroller);
void _set(microcontroller_t* microcontroller);
void _sev(microcontroller_t* microcontroller);
void _sez(microcontroller_t* microcontroller);
void _sleep(microcontroller_t* microcontroller);
void _stx1(microcontroller_t* microcontroller);
void _stx2(microcontroller_t* microcontroller);
void _stx3(microcontroller_t* microcontroller);
void _sty1(microcontroller_t* microcontroller);
void _sty2(microcontroller_t* microcontroller);
void _sty3(microcontroller_t* microcontroller);
void _sty4(microcontroller_t* microcontroller);
void _stz1(microcontroller_t* microcontroller);
void _stz2(microcontroller_t* microcontroller);
void _stz3(microcontroller_t* microcontroller);
void _stz4(microcontroller_t* microcontroller);
void _sub(microcontroller_t* microcontroller);
void _subi(microcontroller_t* microcontroller);
void _swap(microcontroller_t* microcontroller);
void _wdr(microcontroller_t* microcontroller);
void _adiw(microcontroller_t* microcontroller);
void _icall(microcontroller_t* microcontroller);
void _ijmp(microcontroller_t* microcontroller);
void _lds32(microcontroller_t* microcontroller);
void _lds16(microcontroller_t* microcontroller);
void _pop(microcontroller_t* microcontroller);
void _push(microcontroller_t* microcontroller);
void _sbiw(microcontroller_t* microcontroller);
void _sts32(microcontroller_t* microcontroller);
void _sts16(microcontroller_t* microcontroller);
void _movw(microcontroller_t* microcontroller);
void _call(microcontroller_t* microcontroller);
void _jmp(microcontroller_t* microcontroller);
void _elpmz1(microcontroller_t* microcontroller);
void _elpmz2(microcontroller_t* microcontroller);
void _elpmz3(microcontroller_t* microcontroller);
void _fmul(microcontroller_t* microcontroller);
void _fmuls(microcontroller_t* microcontroller);
void _fmulsu(microcontroller_t* microcontroller);
void _mul(microcontroller_t* microcontroller);
void _muls(microcontroller_t* microcontroller);
void _mulsu(microcontroller_t* microcontroller);
void _spm(microcontroller_t* microcontroller);
void _break(microcontroller_t* microcontroller);
void _elpmx(microcontroller_t* microcontroller);

extern void (*opcodes[65527]) (microcontroller_t*);

#endif
