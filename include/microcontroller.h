/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

microcontroller.h

This file pair contains the logic to control individual microcontrollers.
*/


#ifndef MICROCONTROLLER_H
#define MICROCONTROLLER_H

#include "memory.h"
#include "registers.h"
#include "debug.h"
#include "vcd.h"
#include "timers.h"
#include "types.h"
#include "interrupts.h"

struct microcontroller_t
{
    char* model;
    flash_t* flash;
    sram_t* sram;
    eeprom_t* eeprom;
    registers_t* registers;
    debugger_t* debugger;

    uint64_t ticks;
    uint64_t prev_ticks;
    uint64_t tick_time;
    uint64_t counter0;
    uint64_t counter1;
    uint8_t clock_mhz;
    
    int debug_symbols;
    
    uint8_t prev_PINB;
    uint8_t prev_PINC;
    uint8_t prev_PIND;
    uint8_t prev_PINE;
    uint8_t prev_PINF;
    uint8_t prev_PING;
    
    uint8_t prev_TCNT0;
    uint16_t prev_TCNT1;
    uint8_t prev_TCNT2;
    uint16_t prev_TCNT3;
    
    uint8_t adc_start_set;
    uint64_t adc_target_ticks;
    uint8_t adc_mode;
    uint8_t adc_channel;
    uint16_t adc0;
    uint16_t adc1;
    uint16_t adc2;
    uint16_t adc3;
    uint16_t adc4;
    uint16_t adc5;
    uint16_t adc6;
    uint16_t adc7;
    
    uint8_t vcd_vars_count;
    vcd_variable_t* vcd_vars[128];
    
    uint16_t RESET_vect;
    uint16_t INT0_vect;
    uint16_t INT1_vec;
    uint16_t PCINT0_vect;
    uint16_t PCINT1_vect;
    uint16_t PCINT2_vect;
    uint16_t WDT_vect;
    uint16_t TIMER2_COMPA_vect;
    uint16_t TIMER2_COMPB_vect;
    uint16_t TIMER2_OFV_vect;
    uint16_t TIMER1_CAPT_vect;
    uint16_t TIMER1_COMPA_vect;
    uint16_t TIMER1_COMPB_vect;
    uint16_t TIMER1_OFV_vect;
    uint16_t TIMER0_COMPA_vect;
    uint16_t TIMER0_COMPB_vect;
    uint16_t TIMER0_OFV_vect;
    uint16_t SPI_STC_vect;
    uint16_t USART_RX_vect;
    uint16_t USART_UDRE_vect;
    uint16_t USART_TX_vect;
    uint16_t ADC_vect;
    uint16_t EE_READY_vect;
    uint16_t ANALOG_COMP_vect;
    uint16_t TWI_vect;
    uint16_t SPM_READY_vect;
    
    uint8_t n_timers;
    timer_counter_t* timers[8];
    
    uint8_t n_interrupts;
    interrupt_t* interrupts[32];
    
    void (*handle_adc)(microcontroller_t*);
    
    uint8_t n_messages;
    char messages[256][256];
    
    plugin_manager_t* plugin_manager;
    
    uint8_t resetting;
};

//
// Initializes a microcontroller pointer and returns it.
//
microcontroller_t* init_microcontroller(char* mcu, uint8_t clock_mhz);

//
// Recursively frees the microcontroller pointer
//
void free_microcontroller(microcontroller_t* microcontroller);

//
// Resets the microcontroller.
// Emulates bringing the reset pin high.
//
void reset_microcontroller(microcontroller_t* microcontroller);

//
// Checks to see if the file is an Intel hex file.
//
int is_ihex_file(char* filepath);

//
// Checks to see if the file is a Linux elf file.
//
int is_elf_file(char* filepath);

//
// Checks to see if the file has debug symbols.
//
int is_debug_file(char* filepath);

//
// Loads an Intel hex file to the microcontroller's flash memory.
//
int load_hex(microcontroller_t* microcontroller, char* filepath);

//
// Loads a Linux elf file to the microcontroller's flash memory.
// Also loads debug symbols if available.
//
int load_elf(microcontroller_t* microcontroller, char* filepath);

//
// Calls an interrupt with the vector location "location".
//
void call_interrupt(microcontroller_t* microcontroller, const uint32_t location);

//
// Per fram handeling of timers pre interrupts.
//
void handle_timers_pre(microcontroller_t* microcontroller);

//
// Per fram handeling of timers post interrupts.
//
void handle_timers_post(microcontroller_t* microcontroller);

//
// Per frame handeling of adc.
//
void handle_adc(microcontroller_t* microcontroller);

//
// Per fram ehandeling of interrupts
//
void handle_interrupts(microcontroller_t* microcontroller);


void notice(microcontroller_t* microcontroller, const char* fmt, ...);

#define ADC_MODE_SINGLE_ENDED 0

#endif
