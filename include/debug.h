/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

debug.h

Very helpful documents:
http://www.dwarfstd.org/doc/DWARF5.pdf
ftp://ftp.software.ibm.com/software/os390/czos/dwarf/libdwarf2.1.pdf
    
*/


#ifndef FLAME_DEBUG_H
#define FLAME_DEBUG_H

#include <stdint.h>
#include <libelf.h>
#include <libdwarf.h>
#include <dwarf.h>

#include "types.h"
#include "microcontroller.h"

struct debug_type_t
{
	char* name;
	uint8_t avr_type;
	uint8_t size;
	uint8_t encoding;
	uint64_t offset;
	uint64_t typedef_offsets[128];
	uint8_t n_typedef_offsets;
};

struct debug_line_t
{
    uint16_t line_number;
    uint16_t address;
};

struct debug_location_t
{
    uint16_t low_pc;
    uint16_t high_pc;
    uint8_t* value;
};

struct debug_variable_t
{
    char* name;
    uint16_t low_pc;
    uint16_t high_pc;
    uint64_t offset;
    uint64_t type_offset;
    debug_type_t* type;
    
    uint16_t n_locations;
    Dwarf_Locdesc* locations;
};

struct debug_subprogram_t
{
    char* name;
    uint16_t low_pc;
    uint16_t high_pc;
    
    uint16_t n_variables;
    debug_variable_t* variables[32];
};

struct debug_source_t
{
    char* name;
    char* comp_dir;
    char file_path[128];
    char* producer;
    uint16_t low_pc;
    uint16_t high_pc;
    uint16_t source_num;
    
    uint16_t n_types;
    debug_type_t* types[128];
    
	uint16_t n_variables;
    debug_variable_t* variables[32];
    
	uint16_t n_subprograms;
    debug_subprogram_t* subprograms[32];
    
    uint16_t n_lines;
    debug_line_t* lines[128];
    
    uint16_t bp_addresses[1024];
};

struct firmware_breakpoint_t
{
    uint16_t address;
};

struct hardware_breakpoint_t
{
    uint8_t* reg;
    char* reg_name;
    uint8_t prev_reg;
    uint8_t condition;
    uint8_t compare_value;
    uint8_t active;
    uint8_t cur_breakpoint;
    
};

struct data_breakpoint_t
{

};

struct debugger_t
{
    uint8_t* breakpoints;
    int cur_breakpoint;

    debug_source_t** sources;
    uint16_t n_sources;

	debug_type_t* types[128];
	uint16_t n_types;
        
    uint8_t n_hardware_breakpoints;
    hardware_breakpoint_t* hardware_breakpoints[128];
};

debugger_t* init_debugger(const uint32_t size);

void free_debugger(debugger_t* debugger);

void add_breakpoint(debugger_t* debugger, const uint32_t location);

void add_breakpointSource(debugger_t* debugger, const char* filename, const uint16_t line);

void clear_breakpoint(debugger_t* debugger, const uint32_t location);

void toggle_breakpoint(debugger_t* debugger, const uint32_t location);

void add_hardware_breakpoint(debugger_t* debugger, uint8_t* reg, char* reg_name, uint8_t condition, uint8_t compare_value);

int hardware_breakpoint_condition_met(hardware_breakpoint_t* breakpoint);

void populate_sources(microcontroller_t* microcontroller, Elf* elf);

void crawl_dwarf_tree(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die cu_die, void (*callback)(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die));

void populate_variable(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die); 

void populate_abstract_variable(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die); 

void populate_type(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die);

void populate_typedef(debugger_t* debugger, Dwarf_Debug dbg, Dwarf_Die die);

void populate_source_variable(microcontroller_t* microcontroller, Dwarf_Debug dbg, Dwarf_Die die, debug_source_t* source);

void populate_subprogram_variable(microcontroller_t* microcontroller, Dwarf_Debug dbg, Dwarf_Die die, debug_subprogram_t* subprogram);

void populate_subprogram(microcontroller_t* microcontroller, Dwarf_Debug dbg, Dwarf_Die die, debug_source_t* source);

uint8_t* debug_variable_location(microcontroller_t* microcontroller, debug_variable_t* variable);

char* debug_variable_type_name(debug_variable_t* variable);

char* debug_variable_value_hex(microcontroller_t* microcontroller, debug_variable_t* variable);

char* debug_variable_value_string(microcontroller_t* microcontroller, debug_variable_t* variable);

debug_source_t* init_debug_source();

debug_variable_t* init_debug_variable();

debug_subprogram_t* init_debug_subprogram();

debug_line_t* init_debug_line();

debug_location_t* init_debug_location();

debug_type_t* init_debug_type();

#define HARDWARE_BREAKPOINT_CONDITION_LESSTHAN          0
#define HARDWARE_BREAKPOINT_CONDITION_LESSTHAN_EQUAL    1
#define HARDWARE_BREAKPOINT_CONDITION_GREATERTHAN       2
#define HARDWARE_BREAKPOINT_CONDITION_GREATERTHAN_EQUAL 3
#define HARDWARE_BREAKPOINT_CONDITION_EQUAL             4
#define HARDWARE_BREAKPOINT_CONDITION_CHANGED           5

#define AVR_TYPE_CHAR      0
#define AVR_TYPE_SHORT     1
#define AVR_TYPE_INT       2
#define AVR_TYPE_LONG      3
#define AVR_TYPE_LONG_LONG 4
#define AVR_TYPE_SIZE_T    5
#define AVR_TYPE_PTRDIFF_T 6
#define AVR_TYPE_VOIDP     7
#define AVR_TYPE_FLOAT     8
#define AVR_TYPE_DOUBLE    9
#define AVR_TYPE_WCHAR_T  10

#endif
