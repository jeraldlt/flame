/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

print_instructions.h
    
*/


#ifndef FLAME_PRINT_INSTRUCTIONS_H
#define FLAME_PRINT_INSTRUCTIONS_H


#include "microcontroller.h"


void print_and(microcontroller_t* microcontroller, char* str);
void print_adc(microcontroller_t* microcontroller, char* str);
void print_add(microcontroller_t* microcontroller, char* str);
void print_ori(microcontroller_t* microcontroller, char* str);
void print_clr(microcontroller_t* microcontroller, char* str);
void print_rol(microcontroller_t* microcontroller, char* str);
void print_lsl(microcontroller_t* microcontroller, char* str);
void print_tst(microcontroller_t* microcontroller, char* str);
void print_andi(microcontroller_t* microcontroller, char* str);
void print_asr(microcontroller_t* microcontroller, char* str);
void print_bclr(microcontroller_t* microcontroller, char* str);
void print_bld(microcontroller_t* microcontroller, char* str);
void print_brbc(microcontroller_t* microcontroller, char* str);
void print_brbs(microcontroller_t* microcontroller, char* str);
void print_brsh(microcontroller_t* microcontroller, char* str);
void print_brlo(microcontroller_t* microcontroller, char* str);
void print_breq(microcontroller_t* microcontroller, char* str);
void print_brge(microcontroller_t* microcontroller, char* str);
void print_brhc(microcontroller_t* microcontroller, char* str);
void print_brhs(microcontroller_t* microcontroller, char* str);
void print_brid(microcontroller_t* microcontroller, char* str);
void print_brie(microcontroller_t* microcontroller, char* str);
void print_brlt(microcontroller_t* microcontroller, char* str);
void print_brmi(microcontroller_t* microcontroller, char* str);
void print_brne(microcontroller_t* microcontroller, char* str);
void print_brpl(microcontroller_t* microcontroller, char* str);
void print_brtc(microcontroller_t* microcontroller, char* str);
void print_brts(microcontroller_t* microcontroller, char* str);
void print_brvc(microcontroller_t* microcontroller, char* str);
void print_brvs(microcontroller_t* microcontroller, char* str);
void print_bset(microcontroller_t* microcontroller, char* str);
void print_bst(microcontroller_t* microcontroller, char* str);
void print_cbi(microcontroller_t* microcontroller, char* str);
void print_clc(microcontroller_t* microcontroller, char* str);
void print_clh(microcontroller_t* microcontroller, char* str);
void print_cli(microcontroller_t* microcontroller, char* str);
void print_cln(microcontroller_t* microcontroller, char* str);
void print_eor(microcontroller_t* microcontroller, char* str);
void print_cls(microcontroller_t* microcontroller, char* str);
void print_clt(microcontroller_t* microcontroller, char* str);
void print_clv(microcontroller_t* microcontroller, char* str);
void print_clz(microcontroller_t* microcontroller, char* str);
void print_com(microcontroller_t* microcontroller, char* str);
void print_cp(microcontroller_t* microcontroller, char* str);
void print_cpc(microcontroller_t* microcontroller, char* str);
void print_cpi(microcontroller_t* microcontroller, char* str);
void print_cpse(microcontroller_t* microcontroller, char* str);
void print_dec(microcontroller_t* microcontroller, char* str);
void print_in(microcontroller_t* microcontroller, char* str);
void print_inc(microcontroller_t* microcontroller, char* str);
void print_ldx1(microcontroller_t* microcontroller, char* str);
void print_ldx2(microcontroller_t* microcontroller, char* str);
void print_ldx3(microcontroller_t* microcontroller, char* str);
void print_ldy1(microcontroller_t* microcontroller, char* str);
void print_ldy2(microcontroller_t* microcontroller, char* str);
void print_ldy3(microcontroller_t* microcontroller, char* str);
void print_ldy4(microcontroller_t* microcontroller, char* str);
void print_ldz1(microcontroller_t* microcontroller, char* str);
void print_ldz2(microcontroller_t* microcontroller, char* str);
void print_ldz3(microcontroller_t* microcontroller, char* str);
void print_ldz4(microcontroller_t* microcontroller, char* str);
void print_ldi(microcontroller_t* microcontroller, char* str);
void print_lpm1(microcontroller_t* microcontroller, char* str);
void print_lpm2(microcontroller_t* microcontroller, char* str);
void print_lpm3(microcontroller_t* microcontroller, char* str);
void print_lsr(microcontroller_t* microcontroller, char* str);
void print_mov(microcontroller_t* microcontroller, char* str);
void print_neg(microcontroller_t* microcontroller, char* str);
void print_nop(microcontroller_t* microcontroller, char* str);
void print_or(microcontroller_t* microcontroller, char* str);
void print_sbr(microcontroller_t* microcontroller, char* str);
void print_out(microcontroller_t* microcontroller, char* str);
void print_rcall(microcontroller_t* microcontroller, char* str);
void print_ret(microcontroller_t* microcontroller, char* str);
void print_reti(microcontroller_t* microcontroller, char* str);
void print_rjmp(microcontroller_t* microcontroller, char* str);
void print_ror(microcontroller_t* microcontroller, char* str);
void print_sbc(microcontroller_t* microcontroller, char* str);
void print_sbci(microcontroller_t* microcontroller, char* str);
void print_sbi(microcontroller_t* microcontroller, char* str);
void print_sbic(microcontroller_t* microcontroller, char* str);
void print_sbis(microcontroller_t* microcontroller, char* str);
void print_sbrc(microcontroller_t* microcontroller, char* str);
void print_sbrs(microcontroller_t* microcontroller, char* str);
void print_sec(microcontroller_t* microcontroller, char* str);
void print_seh(microcontroller_t* microcontroller, char* str);
void print_sei(microcontroller_t* microcontroller, char* str);
void print_sen(microcontroller_t* microcontroller, char* str);
void print_ser(microcontroller_t* microcontroller, char* str);
void print_ses(microcontroller_t* microcontroller, char* str);
void print_set(microcontroller_t* microcontroller, char* str);
void print_sev(microcontroller_t* microcontroller, char* str);
void print_sez(microcontroller_t* microcontroller, char* str);
void print_sleep(microcontroller_t* microcontroller, char* str);
void print_stx1(microcontroller_t* microcontroller, char* str);
void print_stx2(microcontroller_t* microcontroller, char* str);
void print_stx3(microcontroller_t* microcontroller, char* str);
void print_sty1(microcontroller_t* microcontroller, char* str);
void print_sty2(microcontroller_t* microcontroller, char* str);
void print_sty3(microcontroller_t* microcontroller, char* str);
void print_sty4(microcontroller_t* microcontroller, char* str);
void print_stz1(microcontroller_t* microcontroller, char* str);
void print_stz2(microcontroller_t* microcontroller, char* str);
void print_stz3(microcontroller_t* microcontroller, char* str);
void print_stz4(microcontroller_t* microcontroller, char* str);
void print_sub(microcontroller_t* microcontroller, char* str);
void print_subi(microcontroller_t* microcontroller, char* str);
void print_swap(microcontroller_t* microcontroller, char* str);
void print_wdr(microcontroller_t* microcontroller, char* str);
void print_adiw(microcontroller_t* microcontroller, char* str);
void print_icall(microcontroller_t* microcontroller, char* str);
void print_ijmp(microcontroller_t* microcontroller, char* str);
void print_lds32(microcontroller_t* microcontroller, char* str);
void print_lds16(microcontroller_t* microcontroller, char* str);
void print_pop(microcontroller_t* microcontroller, char* str);
void print_push(microcontroller_t* microcontroller, char* str);
void print_sbiw(microcontroller_t* microcontroller, char* str);
void print_sts32(microcontroller_t* microcontroller, char* str);
void print_sts16(microcontroller_t* microcontroller, char* str);
void print_movw(microcontroller_t* microcontroller, char* str);
void print_call(microcontroller_t* microcontroller, char* str);
void print_jmp(microcontroller_t* microcontroller, char* str);
void print_elpmz1(microcontroller_t* microcontroller, char* str);
void print_elpmz2(microcontroller_t* microcontroller, char* str);
void print_elpmz3(microcontroller_t* microcontroller, char* str);
void print_fmul(microcontroller_t* microcontroller, char* str);
void print_fmuls(microcontroller_t* microcontroller, char* str);
void print_fmulsu(microcontroller_t* microcontroller, char* str);
void print_mul(microcontroller_t* microcontroller, char* str);
void print_muls(microcontroller_t* microcontroller, char* str);
void print_mulsu(microcontroller_t* microcontroller, char* str);
void print_spm(microcontroller_t* microcontroller, char* str);
void print_break(microcontroller_t* microcontroller, char* str);
void print_elpmx(microcontroller_t* microcontroller, char* str);


void (*print_opcodes[65527]) (microcontroller_t*, char*);

#endif
