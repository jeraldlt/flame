/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_hardware_breakpoint.h

*/


#ifndef FLAME_WINDOW_HARDWARE_BREAKPOINT
#define FLAME_WINDOW_HARDWARE_BREAKPOINT

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include <stdint.h>

#include "microcontroller.h"
#include "context.h"

typedef struct window_hardware_breakpoint_t
{
    GtkWidget* window;
    GtkWidget* vbox;
    
    GtkWidget* label_comparison;
    GtkWidget* label_comparison_value;
    
    GtkWidget* combo_comparison;
    GtkWidget* entry_comparison_value;
    
    GtkWidget* check_button_breakpoint_active;
    GtkWidget* button_ok;
    
    
    uint8_t* reg;
    char* reg_name;
    microcontroller_t* microcontroller;
    GtkTreeStore* tree_store;
    GtkTreePath* tree_path;

} window_hardware_breakpoint_t;


window_hardware_breakpoint_t* window_hardware_breakpoint_create_hardware_breakpoint_window(uint8_t* reg, char* reg_name, microcontroller_t* microcontroller, GtkTreeStore* tree_store, GtkTreePath* tree_path);

void window_hardware_breakpoint_button_ok_pressed(GtkButton* button, window_hardware_breakpoint_t* window);

#endif
