/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_inputs.h

*/


#ifndef FLAME_WINDOW_INPUTS_H
#define FLAME_WINDOW_INPUTS_H

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include "microcontroller.h"

typedef struct window_inputs_t
{
    GtkWidget *window;
    GtkWidget *dock_item;
    int dock_item_hidden;
	GtkWidget *scrolled_window;
	GtkWidget* expander_pinb;
	GtkWidget* expander_pinc;
	GtkWidget* expander_pind;
	GtkWidget* expander_adc0;
	GtkWidget* expander_adc1;
	GtkWidget* expander_adc2;
	GtkWidget* expander_adc3;
	GtkWidget* expander_adc4;
	GtkWidget* expander_adc5;
	GtkWidget* expander_adc6;
	GtkWidget* expander_adc7;
	
	microcontroller_t* microcontroller;
	
} window_inputs_t;

typedef struct input_toggler_t
{
    GtkButton* button;
    uint8_t* reg;
    uint8_t bit;
} input_toggler_t;


window_inputs_t* window_inputs_create_inputs_window(microcontroller_t* microcontroller);

void window_inputs_update_window(window_inputs_t* window);

GtkTreeStore* window_inputs_populate_tree();

#endif
