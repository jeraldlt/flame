/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_flash.h

*/


#ifndef FLAME_WINDOW_FLASH_H
#define FLAME_WINDOW_FLASH_H

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include "microcontroller.h"
#include "context.h"

typedef struct window_flash_t
{
    GtkWidget *window;
    GtkWidget *dock_item;
    int dock_item_hidden;
	GtkWidget *scrolled_window;
	GtkSourceLanguageManager *manager;
	GtkSourceLanguage *language;
	GtkSourceView *view;
	GtkSourceBuffer *buffer;
	GtkTextTag *tag;
	GtkSourceSpaceDrawer *space_drawer;
	GtkSourceGutter *gutter;
	
	GdkRGBA* color_red;
	GdkRGBA* color_yellow;
	
	uint8_t bp_flash_on;
	
	microcontroller_t* microcontroller;
} window_flash_t;

void window_flash_set_line_mark(GtkTextIter* start, window_flash_t* window);

void window_flash_line_mark_activated(GtkSourceGutter *gutter, GtkTextIter *iter, GdkEventButton *event, microcontroller_t* microcontroller);

void window_flash_add_source_mark_attributes(window_flash_t* window);

void window_flash_add_buffer_line(GtkTextBuffer *buffer, GtkTextIter *iter, gchar *text, int last_line);

void window_flash_fill_buffer(window_flash_t* window);

window_flash_t* window_flash_create_flash_window(microcontroller_t* microcontroller);

gboolean window_flash_update_flash_window(window_flash_t* window);

void window_flash_query_line_data(GtkSourceGutterRenderer *renderer, GtkTextIter *start, GtkTextIter *end, GtkSourceGutterRendererState state, window_flash_t* window);

#endif
