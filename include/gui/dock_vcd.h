/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_vcd.h

*/


#ifndef FLAME_DOCK_VCD_H
#define FLAME_DOCK_VCD_H

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include <stdint.h>

#include "microcontroller.h"

typedef struct dock_vcd_t
{
    GtkWidget* dock_item;
    int dock_item_hidden;
	GtkWidget* scrolled_window;
	GtkWidget* vbox;
    GtkWidget* treeview_data;
	GtkTreeStore* treestore_data;
	GtkCellRenderer* renderer;
	GtkWidget* button_new;
	GtkWidget* entry_file;
	GtkWidget* button_save;
	GtkWidget* label_vars;
	GtkWidget* label_file;
	
	microcontroller_t* microcontroller;
} dock_vcd_t;

dock_vcd_t* dock_vcd_create_dock(microcontroller_t* microcontroller);

gboolean dock_vcd_update_dock(dock_vcd_t* dock);

void dock_vcd_button_new_pressed(GtkButton* button, dock_vcd_t* dock);

void dock_vcd_button_save_pressed(GtkButton* button, dock_vcd_t* dock);

void dock_vcd_file_icon_pressed(GtkEntry* entry, GtkEntryIconPosition icon_pos, GdkEvent* event, dock_vcd_t* dock);

#endif
