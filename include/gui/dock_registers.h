/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_registers.h

*/


#ifndef FLAME_DOCK_REGISTERS_H
#define FLAME_DOCK_REGISTERS_H

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include "microcontroller.h"
#include "registers.h"

typedef struct window_registers_t
{
    GtkWidget* window;
    GtkWidget* dock_item;
    int dock_item_hidden;
	GtkWidget* scrolled_window;
	GtkWidget* treeview_data;
	GtkTreeStore* treestore_data;
	GtkCellRenderer* renderer;
	GtkCellRenderer* pixbuf_renderer;
	
	GtkTreePath* tree_path_iob;
	GtkTreePath* tree_path_adc;
	
	uint8_t* selected_register;
	char* selected_register_name;
	GtkTreePath* selected_register_path;
	
	microcontroller_t* microcontroller;
	
} window_registers_t;

window_registers_t* window_registers_create_registers_window(microcontroller_t* microcontroller);

gboolean window_registers_update_window(window_registers_t* window);

GtkTreeStore* window_registers_populate_tree();

gboolean treeview_button_pressed(GtkWidget* view, GdkEventButton* event, window_registers_t* window);

#endif
