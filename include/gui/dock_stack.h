/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_stack.h

*/


#ifndef FLAME_DOCK_STACK_H
#define FLAME_DOCK_STACK_H

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include <stdint.h>

#include "microcontroller.h"
#include "context.h"

typedef struct dock_stack_t
{
    GdlDockObject* parent;
    GtkWidget *dock_item;
    int dock_item_hidden;
	GtkWidget *scrolled_window;
	GtkSourceLanguageManager *manager;
	GtkSourceLanguage *language;
	GtkSourceView *view;
	GtkSourceBuffer *buffer;
	GtkTextTag *tag;
	GtkSourceSpaceDrawer *space_drawer;
	GtkSourceGutter *gutter;
	
	uint16_t sp_last;
	
	microcontroller_t* microcontroller;
} dock_stack_t;

void dock_stack_dock_detached(GdlDockObject* item, gboolean recursive, dock_stack_t* dock);

void dock_stack_add_source_mark_attributes(GtkSourceView *view);

void dock_stack_add_buffer_line(GtkTextBuffer *buffer, GtkTextIter *iter, gchar *text);

void dock_stack_fill_buffer(dock_stack_t* dock);

dock_stack_t* dock_stack_create_stack_dock(microcontroller_t* microcontroller, GdlDockObject* parent);

gboolean dock_stack_update_stack_dock(dock_stack_t* dock);

void dock_stack_query_line_data(GtkSourceGutterRenderer *renderer, GtkTextIter *start, GtkTextIter *end, GtkSourceGutterRendererState state, dock_stack_t* dock);

#endif
