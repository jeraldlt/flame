/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_microcontroller.h

*/


#ifndef FLAME_WINDOW_MICROCONTROLLER_H
#define FLAME_WINDOW_MICROCONTROLLER_H

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include <stdint.h>

#include "gui/dock_flash.h"
#include "gui/dock_registers.h"
#include "gui/dock_source.h"
#include "gui/dock_inputs.h"
#include "gui/dock_stack.h"
#include "gui/dock_vcd.h"
#include "gui/dock_variables.h"
#include "gui/window_change_register.h"
#include "gui/window_settings.h"

#include "microcontroller.h"

struct window_microcontroller_t
{
    window_flash_t* window_flash;
    window_source_t* window_source;
    window_registers_t* window_registers;
    window_inputs_t* window_inputs;
    dock_stack_t* dock_stack;
    dock_vcd_t* dock_vcd;
    dock_variables_t* dock_variables;
    
    GtkWidget* vpaned;
    GtkWidget* statusbar;
    GtkWidget* hpaned;
    GtkWidget* expander;
    GtkWidget* dock;
    GtkWidget* main_dock_item;
    GtkWidget* window;
	GtkWidget* vbox;
	GtkWidget* vbox_left;
	GtkWidget* vbox_right;
	GtkWidget* hbox;
	GtkWidget* toolbar;
	GtkWidget* button_box;
	
	GtkWidget* menu_bar;
	GtkWidget* menu_dialogs;
	GtkWidget* menu_item_dialogs;
	GtkWidget* menu_item_dialogs_flash;
	GtkWidget* menu_item_dialogs_registers;
	GtkWidget* menu_item_dialogs_sources;
	GtkWidget* menu_item_dialogs_inputs;
	GtkWidget* menu_item_dialogs_stack;
	GtkWidget* menu_item_dialogs_vcd;
	GtkWidget* menu_item_dialogs_variables;
	GtkWidget* menu_dialogs_plugins;
	GtkWidget* menu_item_dialogs_plugins;
	GtkWidget* menu_debugging;
	GtkWidget* menu_item_debugging;
	GtkWidget* menu_layout;
	GtkWidget* menu_item_layout;
	GtkWidget* menu_item_layout_save;
	GtkWidget* menu_item_layout_save_as;
	GtkWidget* menu_help;
	GtkWidget* menu_item_help;
	char* active_layout;
	GSList* group;
	
	GtkWidget* hbox_model;
	GtkWidget* hbox_clock;
	GtkWidget* hbox_program;
	
	GtkWidget* listbox_info;
	GtkWidget* listbox_row_model;
	GtkWidget* listbox_row_clock;
	GtkWidget* listbox_row_program;
	
	GtkListStore* liststore_info;
	GtkWidget* treeview_info;
	GtkCellRenderer* renderer;
	
	GtkWidget* button_start;
	GtkWidget* button_stop;
	GtkWidget* button_start_stop;
	GtkWidget* button_debugging_on;
	GtkWidget* button_debugging_off;
	GtkWidget* button_debugging_on_off;
	GtkWidget* button_step;
	GtkWidget* button_continue;
	GtkWidget* button_program;
	GtkWidget* button_settings;
	GtkWidget* button_views;

	GtkWidget* button_flash;
	GtkWidget* button_data;
	GtkWidget* button_stack;
	GtkWidget* button_sources;
	GtkWidget* button_inputs;
	GtkWidget* button_vcd;
	GtkWidget* button_reset;
	
	uint8_t n_messages;
	
	GdlDockLayout* layout;
	
	microcontroller_t* microcontroller;
	
};

window_microcontroller_t* create_microcontroller_window(microcontroller_t* microcontroller);

void button_start_stop_pressed(GtkButton* button, window_microcontroller_t* window);

void button_debugging_on_off_pressed(GtkButton* button, window_microcontroller_t* window);

void button_step_pressed(GtkButton* button, window_microcontroller_t* window);

void button_continue_pressed(GtkButton* button, window_microcontroller_t* window);

void button_reset_pressed(GtkButton* button, window_microcontroller_t* window);

void button_program_pressed(GtkButton* button, window_microcontroller_t* window);

void populate_layout_menu(window_microcontroller_t* window);

gboolean update_microcontroller_window(window_microcontroller_t* window);

#endif
