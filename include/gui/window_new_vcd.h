/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_new_vcd.h

*/


#ifndef FLAME_WINDOW_NEW_VCD_H
#define FLAME_WINDOW_NEW_VCD_H

#include <gtksourceview/gtksource.h>

#include "microcontroller.h"

typedef struct new_vcd_window_t
{
    GtkWidget* window;
    GtkWidget* vbox;
    GtkWidget* hbox;
    
    GtkWidget* label_register;
    GtkWidget* label_bit;
    GtkWidget* label_name;
    
    GtkWidget* entry_register;
    GtkWidget* entry_name;
    
    GtkWidget* spin_button_bit;
    
    GtkWidget* button_ok;
    
    microcontroller_t* microcontroller;
} new_vcd_window_t;

void window_new_vcd_new_window(microcontroller_t* microcontroller);

void window_new_vcd_button_ok_pressed(GtkButton* button, new_vcd_window_t* window);

gboolean window_new_vcd_update_window(new_vcd_window_t* window);


#endif
