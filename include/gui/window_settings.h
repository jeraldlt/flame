/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_settings.h

*/


#ifndef FLAME_WINDOW_SETTINGS_H
#define FLAME_WINDOW_SETTINGS_H

#include <gtksourceview/gtksource.h>

#include "microcontroller.h"
#include "plugin.h"
#include "plugin_manager.h"
#include "gui/types.h"
#include "gui/window_microcontroller.h"

typedef struct settings_window_t
{
    window_microcontroller_t* parent;
    
    GtkWidget* window;
    GtkWidget* notebook;
    
    GtkWidget* button_save;
    
    GtkWidget* scrolled_window_gui;
    GtkWidget* scrolled_window_context;
    GtkWidget* scrolled_window_microcontroller;
    GtkWidget* scrolled_window_plugins;
    
    GtkWidget* vbox_gui;
    
    GtkWidget* check_button_byte_addressing;
    GtkWidget* combo_theme;
    
} settings_window_t;

void window_settings_create_settings_window(window_microcontroller_t* parent);

void window_settings_populate_gui_tab(settings_window_t* window);

void window_settings_populate_plugins_tab(settings_window_t* window);

void window_settings_destroy();

void window_settings_check_button_plugin_toggled(GtkToggleButton* button, plugin_container_t* container);

void window_settings_about_button_plugin_pressed(GtkButton* button, plugin_t* plugin);

void window_settings_check_button_byte_addressing_toggled(GtkToggleButton* button);

void window_settings_combo_theme_changed(GtkComboBox* combo);

#endif
