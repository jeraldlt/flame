/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_new_microcontroller.h

*/


#ifndef FLAME_WINDOW_NEW_MICROCONTROLLER_H
#define FLAME_WINDOW_NEW_MICROCONTROLLER_H

#include <stdint.h>
#include <gtksourceview/gtksource.h>

typedef struct new_microcontroller_window_t
{
    GtkWidget* window;
    GtkWidget* vbox;
    GtkWidget* hbox;
    
    GtkWidget* button_about;
    GtkWidget* button_help;
    GtkWidget* button_ok;
    
    GtkWidget* label_model;
    GtkWidget* label_clock;
    GtkWidget* label_program;
    GtkWidget* label_program_type;
    GtkWidget* label_program_debug;

    GtkWidget* combo_model;
    GtkWidget* combo_clock;
    
    GtkWidget* entry_program;
    
    uint64_t exit_signal_id;
} new_microcontroller_window_t;


void window_new_microcontroller_button_ok_pressed(GtkButton* button, new_microcontroller_window_t* window);

void window_new_microcontroller_button_about_pressed(GtkButton* button);

int window_new_microcontroller_check_program(new_microcontroller_window_t* window);

void window_new_microcontroller_entry_program_icon_pressed(GtkEntry* entry, GtkEntryIconPosition icon_pos, GdkEvent* event, new_microcontroller_window_t* window);

void window_new_microcontroller_entry_program_changed(GtkEditable* editable, new_microcontroller_window_t* window);

void window_new_microcontroller_entry_program_activated(GtkEntry* entry, new_microcontroller_window_t* window);

void window_new_microcontroller_new_microcontroller(const char* mcu, const char* clock, const char* file);


#endif
