/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_variables.h

*/


#ifndef FLAME_DOCK_VARIABLES_H
#define FLAME_DOCK_VARIABLES_H

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include "microcontroller.h"
#include "registers.h"
#include "debug.h"

typedef struct dock_variables_t
{
    GtkWidget* dock_item;
    int dock_item_hidden;
	GtkWidget* scrolled_window;
	GtkWidget* treeview_data;
	GtkTreeStore* treestore_data;
	GtkCellRenderer* renderer;
	GtkCellRenderer* pixbuf_renderer;
	
	debug_variable_t* selected_variable;
	
	microcontroller_t* microcontroller;
	
} dock_variables_t;

dock_variables_t* dock_variables_create_dock(microcontroller_t* microcontroller);

gboolean dock_variables_update_dock(dock_variables_t* dock);

GtkTreeStore* dock_variables_populate_tree(microcontroller_t* microcontroller);

gboolean dock_variables_treeview_button_pressed(GtkWidget* view, GdkEventButton* event, dock_variables_t* dock);

#endif
