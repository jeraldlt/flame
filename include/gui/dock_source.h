/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

dock_source.h

*/


#ifndef FLAME_DOCK_SOURCE_H
#define FLAME_DOCK_SOURCE_H

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include "microcontroller.h"
#include "context.h"

typedef struct window_source_t
{
    GtkWidget* window;
    GtkWidget *dock_item;
    int dock_item_hidden;
	GtkWidget* notebook;
	GtkWidget* scrolled_window[100];
	GtkSourceLanguageManager* manager;
	GtkSourceLanguage* language;
	GtkSourceView* view[100];
	GtkSourceBuffer* buffer[100];
	GtkTextTag* tag;
	GtkSourceGutter* gutter[100];
	GtkSourceGutterRenderer* gutterRenderer[100];
	
	microcontroller_t* microcontroller;
} window_source_t;

void window_source_mark_tooltip (GtkSourceMarkAttributes* attributes, GtkSourceMark* mark, gpointer user_data);

void window_source_query_line_data(GtkSourceGutterRenderer* renderer, GtkTextIter* start, GtkTextIter* end, GtkSourceGutterRendererState state, window_source_t* window);

void window_source_open_file (GtkSourceBuffer* buffer, const gchar* filename, int source_index);

void window_source_load_cb (GtkSourceFileLoader* loader, GAsyncResult* result, int source_index);

void window_source_add_empty_bp_marks (GtkSourceBuffer *buffer, int source_index);

void window_source_line_mark_activated (GtkSourceGutter* gutter, GtkTextIter* iter, GdkEventButton* event, window_source_t* window);

void window_source_add_source_mark_attributes (GtkSourceView* view);

window_source_t* window_source_create_source_window (microcontroller_t* microcontroller);

void window_source_populate_notebook(window_source_t* window);

#endif
