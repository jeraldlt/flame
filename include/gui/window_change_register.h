/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

window_change_register.h

*/


#ifndef FLAME_WINDOW_CHANGE_REGISTER
#define FLAME_WINDOW_CHANGE_REGISTER

#include <gtksourceview/gtksource.h>
#include <gdl/gdl.h>

#include <stdint.h>

#include "microcontroller.h"
#include "context.h"

typedef struct window_change_register_t
{
    GtkWidget* window;
    GtkWidget* vbox;
    
    GtkWidget* label_reg_cur;
    GtkWidget* label_reg_new;
    GtkWidget* entry_reg_cur;
    GtkWidget* entry_reg_new;
    
    GtkWidget* check_button_breakpoint_active;
    GtkWidget* label_comparison;
    GtkWidget* combo_comaprison;
    GtkWidget* label_comparison_value;
    GtkWidget* entry_comparison_value;
    GtkWidget* button_ok;
    
    
    uint8_t* reg;
    

} window_change_register_t;


window_change_register_t* window_change_register_create_change_register_window(uint8_t* reg, char* reg_name);

void window_change_register_button_ok_pressed(GtkButton* button, window_change_register_t* window);
gboolean window_change_register_update_change_register_window(window_change_register_t* window);

#endif
