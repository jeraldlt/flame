/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

timers.h
    
*/


#ifndef FLAME_TIMERS_H
#define FLAME_TIMERS_H

#include "microcontroller.h"
#include "types.h"

struct timer_counter_t
{
    uint8_t timer_num;
    void (*handle_timer_pre)(microcontroller_t* microcontroller, timer_counter_t* timer);
    void (*handle_timer_post)(microcontroller_t* microcontroller, timer_counter_t* timer);
    
    uint64_t counter;
    uint16_t prev_tcnt;
    
    uint8_t* timskx;
    
    uint8_t* tccrxa;
    uint8_t* tccrxb;
    uint8_t* tccrxc;
    
    uint8_t* tcntx;
    uint8_t* tcntxh;
    uint8_t* tcntxl;
    
    uint8_t* ocrxa;
    uint8_t* ocrxah;
    uint8_t* ocrxal;
    
    uint8_t* ocrxb;
    uint8_t* ocrxbh;
    uint8_t* ocrxbl;
    
    uint8_t* tifrx;
    
};

timer_counter_t* init_timer(uint8_t timer_num, void (*pre_handler)(), void (*post_handler)());

void timer8(microcontroller_t* microcontroller, timer_counter_t* timer);

void timer16_pre(microcontroller_t* microcontroller, timer_counter_t* timer);

void timer16_post(microcontroller_t* microcontroller, timer_counter_t* timer);

#endif
