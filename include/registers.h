/*
-*- Mode: C; tab-width: 4; indent-tabs-mode: s; c-basic-offset: 4; coding: utf-8 -*-

This file is part of flame.

    flame is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    flame is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flame.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

registers.h
    
*/


#ifndef FLAME_REGISTERS_H
#define FLAME_REGISTERS_H

#include <stdint.h>
#include "memory.h"
#include "types.h"

struct registers_t
{
    uint16_t* PC;
    uint8_t* SPH;
    uint8_t* SPL;
    uint8_t* SREG;
    uint8_t* r;
    
    uint8_t* PORTB;
    uint8_t* DDRB;
    uint8_t* PINB;
    
    uint8_t* PORTC;
    uint8_t* DDRC;
    uint8_t* PINC;
    
    uint8_t* PORTD;
    uint8_t* DDRD;
    uint8_t* PIND;
    
    uint8_t* PCICR;
    uint8_t* PCIFR;
    uint8_t* PCMSK2;
    uint8_t* PCMSK1;
    uint8_t* PCMSK0;
    
    uint8_t* TIFR0;
    uint8_t* GTCCR;
    uint8_t* TCCR0A;
    uint8_t* TCCR0B;
    uint8_t* TCNT0;
    uint8_t* OCR0A;
    uint8_t* OCR0B;
    uint8_t* TIMSK0;
    
    uint8_t* TIFR1;
    uint8_t* TCCR1A;
    uint8_t* TCCR1B;
    uint8_t* TCCR1C;
    uint8_t* TCNT1L;
    uint8_t* TCNT1H;
    uint8_t* ICR1L;
    uint8_t* ICR1H;
    uint8_t* OCR1AL;
    uint8_t* OCR1AH;
    uint8_t* OCR1BL;
    uint8_t* OCR1BH;
    uint8_t* TIMSK1;
    
    uint8_t* ADMUX;
    uint8_t* ADCSRA;
    uint8_t* ADCSRB;
    uint8_t* ADCH;
    uint8_t* ADCL;
    

};

//
// Initializes a registers pointer and returns it.
//
registers_t* init_registers();

//
// Recursevly frees the resiters pointer
//
void free_registers(registers_t* registers);

//
// Returns the concatinated stack pointer
//
uint16_t get_sp(registers_t* registers);

//
// Sets SPH and SPL from a concatinated stack pointer
//
void set_sp(registers_t* registers, uint16_t sp);

//
// Returns a pointer to a register given a name
//
uint8_t* get_register(registers_t* regsiters, char* name);


#endif
