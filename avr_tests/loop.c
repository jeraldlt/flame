#ifndef F_CPU
#define F_CPU 16000000U
#endif

#include <avr/io.h>

#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define output_toggle(port,pin) port ^= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

int main()
{
    int i = 0;
    unsigned int j = 0;
    for (i; i<100; i++)
    {
        j += 2;
    }
}
