#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <avr/interrupt.h>


#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define output_toggle(port,pin) port ^= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

int main()
{

    set_output(DDRB, PB0);
    
    TCCR1A = 0x00;
    //TCCR1B = (1<<CS10) | (1<<CS12); // Timer mode with 1024 prescler
    //TCCR1B |= (1<<WGM12);
    TCCR1B = 0x0D;
    TCCR1C = 0x00;
    OCR1A = 39062;
    TIMSK1 = 0x02;
    //TIMSK1 = (1 << TOIE1);          // Enable timer1 overflow interrupt(TOIE1)
    //TCNT1 = 15625;                  // 1 second at 16MHz
        
    sei();

    while(1)
    {
    }
    
    return 0;
}

ISR(TIMER1_COMPA_vect)
{
    output_toggle(PORTB, PB0);
}


