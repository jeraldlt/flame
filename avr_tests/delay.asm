
delay.elf:     file format elf32-avr


Disassembly of section .text:

00000000 <__vectors>:
   0:	0c 94 34 00 	jmp	0x68	; 0x68 <__ctors_end>
   4:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
   8:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
   c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  10:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  14:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  18:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  1c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  20:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  24:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  28:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  2c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  30:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  34:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  38:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  3c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  40:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  44:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  48:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  4c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  50:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  54:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  58:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  5c:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  60:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>
  64:	0c 94 3e 00 	jmp	0x7c	; 0x7c <__bad_interrupt>

00000068 <__ctors_end>:
  68:	11 24       	eor	r1, r1
  6a:	1f be       	out	0x3f, r1	; 63
  6c:	cf ef       	ldi	r28, 0xFF	; 255
  6e:	d8 e0       	ldi	r29, 0x08	; 8
  70:	de bf       	out	0x3e, r29	; 62
  72:	cd bf       	out	0x3d, r28	; 61
  74:	0e 94 40 00 	call	0x80	; 0x80 <main>
  78:	0c 94 fe 01 	jmp	0x3fc	; 0x3fc <_exit>

0000007c <__bad_interrupt>:
  7c:	0c 94 00 00 	jmp	0	; 0x0 <__vectors>

00000080 <main>:
  80:	cf 93       	push	r28
  82:	df 93       	push	r29
  84:	cd b7       	in	r28, 0x3d	; 61
  86:	de b7       	in	r29, 0x3e	; 62
  88:	a2 97       	sbiw	r28, 0x22	; 34
  8a:	0f b6       	in	r0, 0x3f	; 63
  8c:	f8 94       	cli
  8e:	de bf       	out	0x3e, r29	; 62
  90:	0f be       	out	0x3f, r0	; 63
  92:	cd bf       	out	0x3d, r28	; 61
  94:	80 e0       	ldi	r24, 0x00	; 0
  96:	90 e0       	ldi	r25, 0x00	; 0
  98:	aa e7       	ldi	r26, 0x7A	; 122
  9a:	b4 e4       	ldi	r27, 0x44	; 68
  9c:	89 83       	std	Y+1, r24	; 0x01
  9e:	9a 83       	std	Y+2, r25	; 0x02
  a0:	ab 83       	std	Y+3, r26	; 0x03
  a2:	bc 83       	std	Y+4, r27	; 0x04
  a4:	20 e0       	ldi	r18, 0x00	; 0
  a6:	30 e0       	ldi	r19, 0x00	; 0
  a8:	4a e7       	ldi	r20, 0x7A	; 122
  aa:	55 e4       	ldi	r21, 0x45	; 69
  ac:	69 81       	ldd	r22, Y+1	; 0x01
  ae:	7a 81       	ldd	r23, Y+2	; 0x02
  b0:	8b 81       	ldd	r24, Y+3	; 0x03
  b2:	9c 81       	ldd	r25, Y+4	; 0x04
  b4:	0e 94 69 01 	call	0x2d2	; 0x2d2 <__mulsf3>
  b8:	6b 8b       	std	Y+19, r22	; 0x13
  ba:	7c 8b       	std	Y+20, r23	; 0x14
  bc:	8d 8b       	std	Y+21, r24	; 0x15
  be:	9e 8b       	std	Y+22, r25	; 0x16
  c0:	8b 89       	ldd	r24, Y+19	; 0x13
  c2:	9c 89       	ldd	r25, Y+20	; 0x14
  c4:	ad 89       	ldd	r26, Y+21	; 0x15
  c6:	be 89       	ldd	r27, Y+22	; 0x16
  c8:	8d 83       	std	Y+5, r24	; 0x05
  ca:	9e 83       	std	Y+6, r25	; 0x06
  cc:	af 83       	std	Y+7, r26	; 0x07
  ce:	b8 87       	std	Y+8, r27	; 0x08
  d0:	20 e0       	ldi	r18, 0x00	; 0
  d2:	30 e0       	ldi	r19, 0x00	; 0
  d4:	40 e8       	ldi	r20, 0x80	; 128
  d6:	5f e3       	ldi	r21, 0x3F	; 63
  d8:	6d 81       	ldd	r22, Y+5	; 0x05
  da:	7e 81       	ldd	r23, Y+6	; 0x06
  dc:	8f 81       	ldd	r24, Y+7	; 0x07
  de:	98 85       	ldd	r25, Y+8	; 0x08
  e0:	0e 94 e3 00 	call	0x1c6	; 0x1c6 <__cmpsf2>
  e4:	88 23       	and	r24, r24
  e6:	2c f4       	brge	.+10     	; 0xf2 <main+0x72>
  e8:	81 e0       	ldi	r24, 0x01	; 1
  ea:	90 e0       	ldi	r25, 0x00	; 0
  ec:	9a 87       	std	Y+10, r25	; 0x0a
  ee:	89 87       	std	Y+9, r24	; 0x09
  f0:	54 c0       	rjmp	.+168    	; 0x19a <main+0x11a>
  f2:	20 e0       	ldi	r18, 0x00	; 0
  f4:	3f ef       	ldi	r19, 0xFF	; 255
  f6:	4f e7       	ldi	r20, 0x7F	; 127
  f8:	57 e4       	ldi	r21, 0x47	; 71
  fa:	6d 81       	ldd	r22, Y+5	; 0x05
  fc:	7e 81       	ldd	r23, Y+6	; 0x06
  fe:	8f 81       	ldd	r24, Y+7	; 0x07
 100:	98 85       	ldd	r25, Y+8	; 0x08
 102:	0e 94 64 01 	call	0x2c8	; 0x2c8 <__gesf2>
 106:	18 16       	cp	r1, r24
 108:	d4 f5       	brge	.+116    	; 0x17e <main+0xfe>
 10a:	20 e0       	ldi	r18, 0x00	; 0
 10c:	30 e0       	ldi	r19, 0x00	; 0
 10e:	40 e2       	ldi	r20, 0x20	; 32
 110:	51 e4       	ldi	r21, 0x41	; 65
 112:	69 81       	ldd	r22, Y+1	; 0x01
 114:	7a 81       	ldd	r23, Y+2	; 0x02
 116:	8b 81       	ldd	r24, Y+3	; 0x03
 118:	9c 81       	ldd	r25, Y+4	; 0x04
 11a:	0e 94 69 01 	call	0x2d2	; 0x2d2 <__mulsf3>
 11e:	6f 8b       	std	Y+23, r22	; 0x17
 120:	78 8f       	std	Y+24, r23	; 0x18
 122:	89 8f       	std	Y+25, r24	; 0x19
 124:	9a 8f       	std	Y+26, r25	; 0x1a
 126:	8f 89       	ldd	r24, Y+23	; 0x17
 128:	98 8d       	ldd	r25, Y+24	; 0x18
 12a:	a9 8d       	ldd	r26, Y+25	; 0x19
 12c:	ba 8d       	ldd	r27, Y+26	; 0x1a
 12e:	8f 87       	std	Y+15, r24	; 0x0f
 130:	98 8b       	std	Y+16, r25	; 0x10
 132:	a9 8b       	std	Y+17, r26	; 0x11
 134:	ba 8b       	std	Y+18, r27	; 0x12
 136:	6f 85       	ldd	r22, Y+15	; 0x0f
 138:	78 89       	ldd	r23, Y+16	; 0x10
 13a:	89 89       	ldd	r24, Y+17	; 0x11
 13c:	9a 89       	ldd	r25, Y+18	; 0x12
 13e:	0e 94 e8 00 	call	0x1d0	; 0x1d0 <__fixunssfsi>
 142:	6b 8f       	std	Y+27, r22	; 0x1b
 144:	7c 8f       	std	Y+28, r23	; 0x1c
 146:	8d 8f       	std	Y+29, r24	; 0x1d
 148:	9e 8f       	std	Y+30, r25	; 0x1e
 14a:	ab 8d       	ldd	r26, Y+27	; 0x1b
 14c:	bc 8d       	ldd	r27, Y+28	; 0x1c
 14e:	ba 87       	std	Y+10, r27	; 0x0a
 150:	a9 87       	std	Y+9, r26	; 0x09
 152:	10 c0       	rjmp	.+32     	; 0x174 <main+0xf4>
 154:	80 e9       	ldi	r24, 0x90	; 144
 156:	91 e0       	ldi	r25, 0x01	; 1
 158:	9c 87       	std	Y+12, r25	; 0x0c
 15a:	8b 87       	std	Y+11, r24	; 0x0b
 15c:	8b 85       	ldd	r24, Y+11	; 0x0b
 15e:	9c 85       	ldd	r25, Y+12	; 0x0c
 160:	01 97       	sbiw	r24, 0x01	; 1
 162:	f1 f7       	brne	.-4      	; 0x160 <main+0xe0>
 164:	9c 87       	std	Y+12, r25	; 0x0c
 166:	8b 87       	std	Y+11, r24	; 0x0b
 168:	00 00       	nop
 16a:	89 85       	ldd	r24, Y+9	; 0x09
 16c:	9a 85       	ldd	r25, Y+10	; 0x0a
 16e:	01 97       	sbiw	r24, 0x01	; 1
 170:	9a 87       	std	Y+10, r25	; 0x0a
 172:	89 87       	std	Y+9, r24	; 0x09
 174:	89 85       	ldd	r24, Y+9	; 0x09
 176:	9a 85       	ldd	r25, Y+10	; 0x0a
 178:	89 2b       	or	r24, r25
 17a:	61 f7       	brne	.-40     	; 0x154 <main+0xd4>
 17c:	19 c0       	rjmp	.+50     	; 0x1b0 <main+0x130>
 17e:	6d 81       	ldd	r22, Y+5	; 0x05
 180:	7e 81       	ldd	r23, Y+6	; 0x06
 182:	8f 81       	ldd	r24, Y+7	; 0x07
 184:	98 85       	ldd	r25, Y+8	; 0x08
 186:	0e 94 e8 00 	call	0x1d0	; 0x1d0 <__fixunssfsi>
 18a:	6f 8f       	std	Y+31, r22	; 0x1f
 18c:	78 a3       	std	Y+32, r23	; 0x20
 18e:	89 a3       	std	Y+33, r24	; 0x21
 190:	9a a3       	std	Y+34, r25	; 0x22
 192:	8f 8d       	ldd	r24, Y+31	; 0x1f
 194:	98 a1       	ldd	r25, Y+32	; 0x20
 196:	9a 87       	std	Y+10, r25	; 0x0a
 198:	89 87       	std	Y+9, r24	; 0x09
 19a:	89 85       	ldd	r24, Y+9	; 0x09
 19c:	9a 85       	ldd	r25, Y+10	; 0x0a
 19e:	9e 87       	std	Y+14, r25	; 0x0e
 1a0:	8d 87       	std	Y+13, r24	; 0x0d
 1a2:	8d 85       	ldd	r24, Y+13	; 0x0d
 1a4:	9e 85       	ldd	r25, Y+14	; 0x0e
 1a6:	01 97       	sbiw	r24, 0x01	; 1
 1a8:	f1 f7       	brne	.-4      	; 0x1a6 <main+0x126>
 1aa:	9e 87       	std	Y+14, r25	; 0x0e
 1ac:	8d 87       	std	Y+13, r24	; 0x0d
 1ae:	00 00       	nop
 1b0:	80 e0       	ldi	r24, 0x00	; 0
 1b2:	90 e0       	ldi	r25, 0x00	; 0
 1b4:	a2 96       	adiw	r28, 0x22	; 34
 1b6:	0f b6       	in	r0, 0x3f	; 63
 1b8:	f8 94       	cli
 1ba:	de bf       	out	0x3e, r29	; 62
 1bc:	0f be       	out	0x3f, r0	; 63
 1be:	cd bf       	out	0x3d, r28	; 61
 1c0:	df 91       	pop	r29
 1c2:	cf 91       	pop	r28
 1c4:	08 95       	ret

000001c6 <__cmpsf2>:
 1c6:	0e 94 17 01 	call	0x22e	; 0x22e <__fp_cmp>
 1ca:	08 f4       	brcc	.+2      	; 0x1ce <__cmpsf2+0x8>
 1cc:	81 e0       	ldi	r24, 0x01	; 1
 1ce:	08 95       	ret

000001d0 <__fixunssfsi>:
 1d0:	0e 94 43 01 	call	0x286	; 0x286 <__fp_splitA>
 1d4:	88 f0       	brcs	.+34     	; 0x1f8 <__fixunssfsi+0x28>
 1d6:	9f 57       	subi	r25, 0x7F	; 127
 1d8:	98 f0       	brcs	.+38     	; 0x200 <__fixunssfsi+0x30>
 1da:	b9 2f       	mov	r27, r25
 1dc:	99 27       	eor	r25, r25
 1de:	b7 51       	subi	r27, 0x17	; 23
 1e0:	b0 f0       	brcs	.+44     	; 0x20e <__fixunssfsi+0x3e>
 1e2:	e1 f0       	breq	.+56     	; 0x21c <__fixunssfsi+0x4c>
 1e4:	66 0f       	add	r22, r22
 1e6:	77 1f       	adc	r23, r23
 1e8:	88 1f       	adc	r24, r24
 1ea:	99 1f       	adc	r25, r25
 1ec:	1a f0       	brmi	.+6      	; 0x1f4 <__fixunssfsi+0x24>
 1ee:	ba 95       	dec	r27
 1f0:	c9 f7       	brne	.-14     	; 0x1e4 <__fixunssfsi+0x14>
 1f2:	14 c0       	rjmp	.+40     	; 0x21c <__fixunssfsi+0x4c>
 1f4:	b1 30       	cpi	r27, 0x01	; 1
 1f6:	91 f0       	breq	.+36     	; 0x21c <__fixunssfsi+0x4c>
 1f8:	0e 94 5d 01 	call	0x2ba	; 0x2ba <__fp_zero>
 1fc:	b1 e0       	ldi	r27, 0x01	; 1
 1fe:	08 95       	ret
 200:	0c 94 5d 01 	jmp	0x2ba	; 0x2ba <__fp_zero>
 204:	67 2f       	mov	r22, r23
 206:	78 2f       	mov	r23, r24
 208:	88 27       	eor	r24, r24
 20a:	b8 5f       	subi	r27, 0xF8	; 248
 20c:	39 f0       	breq	.+14     	; 0x21c <__fixunssfsi+0x4c>
 20e:	b9 3f       	cpi	r27, 0xF9	; 249
 210:	cc f3       	brlt	.-14     	; 0x204 <__fixunssfsi+0x34>
 212:	86 95       	lsr	r24
 214:	77 95       	ror	r23
 216:	67 95       	ror	r22
 218:	b3 95       	inc	r27
 21a:	d9 f7       	brne	.-10     	; 0x212 <__fixunssfsi+0x42>
 21c:	3e f4       	brtc	.+14     	; 0x22c <__fixunssfsi+0x5c>
 21e:	90 95       	com	r25
 220:	80 95       	com	r24
 222:	70 95       	com	r23
 224:	61 95       	neg	r22
 226:	7f 4f       	sbci	r23, 0xFF	; 255
 228:	8f 4f       	sbci	r24, 0xFF	; 255
 22a:	9f 4f       	sbci	r25, 0xFF	; 255
 22c:	08 95       	ret

0000022e <__fp_cmp>:
 22e:	99 0f       	add	r25, r25
 230:	00 08       	sbc	r0, r0
 232:	55 0f       	add	r21, r21
 234:	aa 0b       	sbc	r26, r26
 236:	e0 e8       	ldi	r30, 0x80	; 128
 238:	fe ef       	ldi	r31, 0xFE	; 254
 23a:	16 16       	cp	r1, r22
 23c:	17 06       	cpc	r1, r23
 23e:	e8 07       	cpc	r30, r24
 240:	f9 07       	cpc	r31, r25
 242:	c0 f0       	brcs	.+48     	; 0x274 <__fp_cmp+0x46>
 244:	12 16       	cp	r1, r18
 246:	13 06       	cpc	r1, r19
 248:	e4 07       	cpc	r30, r20
 24a:	f5 07       	cpc	r31, r21
 24c:	98 f0       	brcs	.+38     	; 0x274 <__fp_cmp+0x46>
 24e:	62 1b       	sub	r22, r18
 250:	73 0b       	sbc	r23, r19
 252:	84 0b       	sbc	r24, r20
 254:	95 0b       	sbc	r25, r21
 256:	39 f4       	brne	.+14     	; 0x266 <__fp_cmp+0x38>
 258:	0a 26       	eor	r0, r26
 25a:	61 f0       	breq	.+24     	; 0x274 <__fp_cmp+0x46>
 25c:	23 2b       	or	r18, r19
 25e:	24 2b       	or	r18, r20
 260:	25 2b       	or	r18, r21
 262:	21 f4       	brne	.+8      	; 0x26c <__fp_cmp+0x3e>
 264:	08 95       	ret
 266:	0a 26       	eor	r0, r26
 268:	09 f4       	brne	.+2      	; 0x26c <__fp_cmp+0x3e>
 26a:	a1 40       	sbci	r26, 0x01	; 1
 26c:	a6 95       	lsr	r26
 26e:	8f ef       	ldi	r24, 0xFF	; 255
 270:	81 1d       	adc	r24, r1
 272:	81 1d       	adc	r24, r1
 274:	08 95       	ret

00000276 <__fp_split3>:
 276:	57 fd       	sbrc	r21, 7
 278:	90 58       	subi	r25, 0x80	; 128
 27a:	44 0f       	add	r20, r20
 27c:	55 1f       	adc	r21, r21
 27e:	59 f0       	breq	.+22     	; 0x296 <__fp_splitA+0x10>
 280:	5f 3f       	cpi	r21, 0xFF	; 255
 282:	71 f0       	breq	.+28     	; 0x2a0 <__fp_splitA+0x1a>
 284:	47 95       	ror	r20

00000286 <__fp_splitA>:
 286:	88 0f       	add	r24, r24
 288:	97 fb       	bst	r25, 7
 28a:	99 1f       	adc	r25, r25
 28c:	61 f0       	breq	.+24     	; 0x2a6 <__fp_splitA+0x20>
 28e:	9f 3f       	cpi	r25, 0xFF	; 255
 290:	79 f0       	breq	.+30     	; 0x2b0 <__fp_splitA+0x2a>
 292:	87 95       	ror	r24
 294:	08 95       	ret
 296:	12 16       	cp	r1, r18
 298:	13 06       	cpc	r1, r19
 29a:	14 06       	cpc	r1, r20
 29c:	55 1f       	adc	r21, r21
 29e:	f2 cf       	rjmp	.-28     	; 0x284 <__fp_split3+0xe>
 2a0:	46 95       	lsr	r20
 2a2:	f1 df       	rcall	.-30     	; 0x286 <__fp_splitA>
 2a4:	08 c0       	rjmp	.+16     	; 0x2b6 <__fp_splitA+0x30>
 2a6:	16 16       	cp	r1, r22
 2a8:	17 06       	cpc	r1, r23
 2aa:	18 06       	cpc	r1, r24
 2ac:	99 1f       	adc	r25, r25
 2ae:	f1 cf       	rjmp	.-30     	; 0x292 <__fp_splitA+0xc>
 2b0:	86 95       	lsr	r24
 2b2:	71 05       	cpc	r23, r1
 2b4:	61 05       	cpc	r22, r1
 2b6:	08 94       	sec
 2b8:	08 95       	ret

000002ba <__fp_zero>:
 2ba:	e8 94       	clt

000002bc <__fp_szero>:
 2bc:	bb 27       	eor	r27, r27
 2be:	66 27       	eor	r22, r22
 2c0:	77 27       	eor	r23, r23
 2c2:	cb 01       	movw	r24, r22
 2c4:	97 f9       	bld	r25, 7
 2c6:	08 95       	ret

000002c8 <__gesf2>:
 2c8:	0e 94 17 01 	call	0x22e	; 0x22e <__fp_cmp>
 2cc:	08 f4       	brcc	.+2      	; 0x2d0 <__gesf2+0x8>
 2ce:	8f ef       	ldi	r24, 0xFF	; 255
 2d0:	08 95       	ret

000002d2 <__mulsf3>:
 2d2:	0e 94 7c 01 	call	0x2f8	; 0x2f8 <__mulsf3x>
 2d6:	0c 94 ed 01 	jmp	0x3da	; 0x3da <__fp_round>
 2da:	0e 94 df 01 	call	0x3be	; 0x3be <__fp_pscA>
 2de:	38 f0       	brcs	.+14     	; 0x2ee <__mulsf3+0x1c>
 2e0:	0e 94 e6 01 	call	0x3cc	; 0x3cc <__fp_pscB>
 2e4:	20 f0       	brcs	.+8      	; 0x2ee <__mulsf3+0x1c>
 2e6:	95 23       	and	r25, r21
 2e8:	11 f0       	breq	.+4      	; 0x2ee <__mulsf3+0x1c>
 2ea:	0c 94 d6 01 	jmp	0x3ac	; 0x3ac <__fp_inf>
 2ee:	0c 94 dc 01 	jmp	0x3b8	; 0x3b8 <__fp_nan>
 2f2:	11 24       	eor	r1, r1
 2f4:	0c 94 5e 01 	jmp	0x2bc	; 0x2bc <__fp_szero>

000002f8 <__mulsf3x>:
 2f8:	0e 94 3b 01 	call	0x276	; 0x276 <__fp_split3>
 2fc:	70 f3       	brcs	.-36     	; 0x2da <__mulsf3+0x8>

000002fe <__mulsf3_pse>:
 2fe:	95 9f       	mul	r25, r21
 300:	c1 f3       	breq	.-16     	; 0x2f2 <__mulsf3+0x20>
 302:	95 0f       	add	r25, r21
 304:	50 e0       	ldi	r21, 0x00	; 0
 306:	55 1f       	adc	r21, r21
 308:	62 9f       	mul	r22, r18
 30a:	f0 01       	movw	r30, r0
 30c:	72 9f       	mul	r23, r18
 30e:	bb 27       	eor	r27, r27
 310:	f0 0d       	add	r31, r0
 312:	b1 1d       	adc	r27, r1
 314:	63 9f       	mul	r22, r19
 316:	aa 27       	eor	r26, r26
 318:	f0 0d       	add	r31, r0
 31a:	b1 1d       	adc	r27, r1
 31c:	aa 1f       	adc	r26, r26
 31e:	64 9f       	mul	r22, r20
 320:	66 27       	eor	r22, r22
 322:	b0 0d       	add	r27, r0
 324:	a1 1d       	adc	r26, r1
 326:	66 1f       	adc	r22, r22
 328:	82 9f       	mul	r24, r18
 32a:	22 27       	eor	r18, r18
 32c:	b0 0d       	add	r27, r0
 32e:	a1 1d       	adc	r26, r1
 330:	62 1f       	adc	r22, r18
 332:	73 9f       	mul	r23, r19
 334:	b0 0d       	add	r27, r0
 336:	a1 1d       	adc	r26, r1
 338:	62 1f       	adc	r22, r18
 33a:	83 9f       	mul	r24, r19
 33c:	a0 0d       	add	r26, r0
 33e:	61 1d       	adc	r22, r1
 340:	22 1f       	adc	r18, r18
 342:	74 9f       	mul	r23, r20
 344:	33 27       	eor	r19, r19
 346:	a0 0d       	add	r26, r0
 348:	61 1d       	adc	r22, r1
 34a:	23 1f       	adc	r18, r19
 34c:	84 9f       	mul	r24, r20
 34e:	60 0d       	add	r22, r0
 350:	21 1d       	adc	r18, r1
 352:	82 2f       	mov	r24, r18
 354:	76 2f       	mov	r23, r22
 356:	6a 2f       	mov	r22, r26
 358:	11 24       	eor	r1, r1
 35a:	9f 57       	subi	r25, 0x7F	; 127
 35c:	50 40       	sbci	r21, 0x00	; 0
 35e:	9a f0       	brmi	.+38     	; 0x386 <__mulsf3_pse+0x88>
 360:	f1 f0       	breq	.+60     	; 0x39e <__mulsf3_pse+0xa0>
 362:	88 23       	and	r24, r24
 364:	4a f0       	brmi	.+18     	; 0x378 <__mulsf3_pse+0x7a>
 366:	ee 0f       	add	r30, r30
 368:	ff 1f       	adc	r31, r31
 36a:	bb 1f       	adc	r27, r27
 36c:	66 1f       	adc	r22, r22
 36e:	77 1f       	adc	r23, r23
 370:	88 1f       	adc	r24, r24
 372:	91 50       	subi	r25, 0x01	; 1
 374:	50 40       	sbci	r21, 0x00	; 0
 376:	a9 f7       	brne	.-22     	; 0x362 <__mulsf3_pse+0x64>
 378:	9e 3f       	cpi	r25, 0xFE	; 254
 37a:	51 05       	cpc	r21, r1
 37c:	80 f0       	brcs	.+32     	; 0x39e <__mulsf3_pse+0xa0>
 37e:	0c 94 d6 01 	jmp	0x3ac	; 0x3ac <__fp_inf>
 382:	0c 94 5e 01 	jmp	0x2bc	; 0x2bc <__fp_szero>
 386:	5f 3f       	cpi	r21, 0xFF	; 255
 388:	e4 f3       	brlt	.-8      	; 0x382 <__mulsf3_pse+0x84>
 38a:	98 3e       	cpi	r25, 0xE8	; 232
 38c:	d4 f3       	brlt	.-12     	; 0x382 <__mulsf3_pse+0x84>
 38e:	86 95       	lsr	r24
 390:	77 95       	ror	r23
 392:	67 95       	ror	r22
 394:	b7 95       	ror	r27
 396:	f7 95       	ror	r31
 398:	e7 95       	ror	r30
 39a:	9f 5f       	subi	r25, 0xFF	; 255
 39c:	c1 f7       	brne	.-16     	; 0x38e <__mulsf3_pse+0x90>
 39e:	fe 2b       	or	r31, r30
 3a0:	88 0f       	add	r24, r24
 3a2:	91 1d       	adc	r25, r1
 3a4:	96 95       	lsr	r25
 3a6:	87 95       	ror	r24
 3a8:	97 f9       	bld	r25, 7
 3aa:	08 95       	ret

000003ac <__fp_inf>:
 3ac:	97 f9       	bld	r25, 7
 3ae:	9f 67       	ori	r25, 0x7F	; 127
 3b0:	80 e8       	ldi	r24, 0x80	; 128
 3b2:	70 e0       	ldi	r23, 0x00	; 0
 3b4:	60 e0       	ldi	r22, 0x00	; 0
 3b6:	08 95       	ret

000003b8 <__fp_nan>:
 3b8:	9f ef       	ldi	r25, 0xFF	; 255
 3ba:	80 ec       	ldi	r24, 0xC0	; 192
 3bc:	08 95       	ret

000003be <__fp_pscA>:
 3be:	00 24       	eor	r0, r0
 3c0:	0a 94       	dec	r0
 3c2:	16 16       	cp	r1, r22
 3c4:	17 06       	cpc	r1, r23
 3c6:	18 06       	cpc	r1, r24
 3c8:	09 06       	cpc	r0, r25
 3ca:	08 95       	ret

000003cc <__fp_pscB>:
 3cc:	00 24       	eor	r0, r0
 3ce:	0a 94       	dec	r0
 3d0:	12 16       	cp	r1, r18
 3d2:	13 06       	cpc	r1, r19
 3d4:	14 06       	cpc	r1, r20
 3d6:	05 06       	cpc	r0, r21
 3d8:	08 95       	ret

000003da <__fp_round>:
 3da:	09 2e       	mov	r0, r25
 3dc:	03 94       	inc	r0
 3de:	00 0c       	add	r0, r0
 3e0:	11 f4       	brne	.+4      	; 0x3e6 <__fp_round+0xc>
 3e2:	88 23       	and	r24, r24
 3e4:	52 f0       	brmi	.+20     	; 0x3fa <__fp_round+0x20>
 3e6:	bb 0f       	add	r27, r27
 3e8:	40 f4       	brcc	.+16     	; 0x3fa <__fp_round+0x20>
 3ea:	bf 2b       	or	r27, r31
 3ec:	11 f4       	brne	.+4      	; 0x3f2 <__fp_round+0x18>
 3ee:	60 ff       	sbrs	r22, 0
 3f0:	04 c0       	rjmp	.+8      	; 0x3fa <__fp_round+0x20>
 3f2:	6f 5f       	subi	r22, 0xFF	; 255
 3f4:	7f 4f       	sbci	r23, 0xFF	; 255
 3f6:	8f 4f       	sbci	r24, 0xFF	; 255
 3f8:	9f 4f       	sbci	r25, 0xFF	; 255
 3fa:	08 95       	ret

000003fc <_exit>:
 3fc:	f8 94       	cli

000003fe <__stop_program>:
 3fe:	ff cf       	rjmp	.-2      	; 0x3fe <__stop_program>
