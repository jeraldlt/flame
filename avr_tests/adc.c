#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <avr/interrupt.h>


#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define output_toggle(port,pin) port ^= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

volatile uint16_t adc_val;

uint16_t read_adc()
{
    ADCSRA |= (1<<ADSC);

    // wait for conversion to complete
    // ADSC becomes '0' again
    // till then, run loop continuously
    while(ADCSRA & (1<<ADSC));

    //return (ADC);
}

int main()
{

	set_input(DDRB, PB0);

	PCICR = (1 << PCIE0);
	PCMSK0 = (1 << PCINT0);

    ADCSRA = 0x80;

	sei();
	while(1)
	{
	}
	
    return 0;
}

ISR(PCINT0_vect)
{
	adc_val = read_adc();
}


