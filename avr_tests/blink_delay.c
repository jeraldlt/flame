#ifndef F_CPU
#define F_CPU 16000000U
#endif

#include <avr/io.h>
#include <util/delay.h>

#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define output_toggle(port,pin) port ^= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

int main()
{
    
    set_output(DDRB, PB0);
    
    while (1)
    {
        output_toggle(PORTB, PB0);
        _delay_ms(1000);
    }
    return 0;
}
