"""
This file is part of glaed.

    glaed is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    glaed is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with glaed.  If not, see <https://www.gnu.org/licenses/>.

------------------------------------------------------------------------

gen_instructions.py

"""

def bin_to_dec(binary):
    dec = 0
    power = len(binary) - 1
    for char in binary:
        dec += int(char) * (2 ** power)
        power -= 1
        
    return dec

def add_opcode(code, operation, dictionary):
    if 'x' in code:
        add_opcode(code.replace('x', '0', 1), operation, dictionary)
        add_opcode(code.replace('x', '1', 1), operation, dictionary)
    
    else:
        val = bin_to_dec(code)
        dictionary[val] = operation
                 
def generate_opcode_dice(instruction_set):
    
    complete_codes = {}
    
    most_xs = 0
    for code in instruction_set:
        n = code.count('x')
        if n > most_xs:
            most_xs = n
                
    while most_xs >= 0: 
        for code, operation in instruction_set.items():
            if code.count('x') == most_xs:
                add_opcode(code, operation, complete_codes)
                
        most_xs -= 1
        
    return complete_codes
        

AVR1 = {
"000111xxxxxxxxxx": "adc",
"000011xxxxxxxxxx": "add",
"001000xxxxxxxxxx": "and",
"0111xxxxxxxxxxxx": "andi",
"1001010xxxxx0101": "asr",
"100101001xxx1000": "bclr",
"1111100xxxxx0xxx": "bld",
"111101xxxxxxxxxx": "brbc",
"111100xxxxxxxxxx": "brbs",
"111101xxxxxxx000": "brcc",
"111100xxxxxxx000": "brcs",
"111100xxxxxxx001": "breq",
"111101xxxxxxx100": "brge",
"111101xxxxxxx101": "brhc",
"111100xxxxxxx101": "brhs",
"111101xxxxxxx111": "brid",
"111100xxxxxxx111": "brie",
"111100xxxxxxx000": "brlo",
"111100xxxxxxx100": "brlt",
"111100xxxxxxx010": "brmi",
"111101xxxxxxx001": "brne",
"111101xxxxxxx010": "brpl",
"111101xxxxxxx000": "brsh",
"111101xxxxxxx110": "brtc",
"111100xxxxxxx110": "brts",
"111101xxxxxxx011": "brvc",
"111100xxxxxxx011": "brvs",
"100101000xxx1000": "bset",
"1111101xxxxx0xxx": "bst",
"10011000xxxxxxxx": "cbi",
#"": "cbr",
"1001010010001000": "clc",
"1001010011011000": "clh",
"1001010011111000": "cli",
"1001010010101000": "cln",
"001001xxxxxxxxxx": "clr",
"1001010011001000": "cls",
"1001010011101000": "clt",
"1001010010111000": "clv",
"1001010010011000": "clz",
"1001010xxxxx0000": "com",
"000101xxxxxxxxxx": "cp",
"000001xxxxxxxxxx": "cpc",
"0011xxxxxxxxxxxx": "cpi",
"000100xxxxxxxxxx": "cpse",
"1001010xxxxx1010": "dec",
"001001xxxxxxxxxx": "eor",
"10110xxxxxxxxxxx": "in",
"1001010xxxxx0011": "inc",
"1001000xxxxx1100": "ldx1",
"1001000xxxxx1101": "ldx2",
"1001000xxxxx1110": "ldx3",
"1000000xxxxx1000": "ldy1",
"1001000xxxxx1001": "ldy2",
"1001000xxxxx1010": "ldy3",
"10x0xx0xxxxx1xxx": "ldy4",
"1000000xxxxx0000": "ldz1",
"1001000xxxxx0001": "ldz2",
"1001000xxxxx0010": "ldz3",
"10x0xx0xxxxx0xxx": "ldz4",
"1110xxxxxxxxxxxx": "ldi",
"1001010111001000": "lpm1",
"1001000xxxxx0100": "lpm2",
"1001000xxxxx0101": "lpm3",
#"000011xxxxxxxxxx": "lsl",
"1001010xxxxx0110": "lsr",
"001011xxxxxxxxxx": "mov",
"1001010xxxxx0001": "neg",
"0000000000000000": "nop",
"001010xxxxxxxxxx": "or",
"0110xxxxxxxxxxxx": "ori",
"10111xxxxxxxxxxx": "out",
"1101xxxxxxxxxxxx": "rcall",
"1001010100001000": "ret",
"1001010100011000": "reti",
"1100xxxxxxxxxxxx": "rjmp",
#"000111xxxxxxxxxx": "rol",
"1001010xxxxx0111": "ror",
"000010xxxxxxxxxx": "sbc",
"0100xxxxxxxxxxxx": "sbci",
"10011010xxxxxxxx": "sbi",
"10011001xxxxxxxx": "sbic",
"10011011xxxxxxxx": "sbis",
#"0110xxxxxxxxxxxx": "sbr",
"1111110xxxxx0xxx": "sbrc",
"1111111xxxxx0xxx": "sbrs",
"1001010000001000": "sec",
"1001010001011000": "seh",
"1001010001111000": "sei",
"1001010000101000": "sen",
"11101111xxxx1111": "ser",
"1001010001001000": "ses",
"1001010001101000": "set",
"1001010000111000": "sev",
"1001010000011000": "sez",
"1001010110001000": "sleep",
"1001001xxxxx1100": "stx1",
"1001001xxxxx1101": "stx2",
"1001001xxxxx1110": "stx3",
"1000001xxxxx1000": "sty1", 
"1001001xxxxx1001": "sty2", 
"1001001xxxxx1010": "sty3",
"10x0xx1xxxxx1xxx": "sty4",
"1000001xxxxx0000": "stz1",
"1001001xxxxx0001": "stz2",
"1001001xxxxx0010": "stz3",
"10x0xx1xxxxx0xxx": "stz4",
"000110xxxxxxxxxx": "sub",
"0101xxxxxxxxxxxx": "subi",
"1001010xxxxx0010": "swap",
#"001000xxxxxxxxxx": "tst",
"1001010110101000": "wdr",
}

AVR2 = {
**AVR1,
"10010110xxxxxxxx": "adiw",
"1001010100001001": "icall",
"1001010000001001": "ijmp",
"1001000xxxxx0000": "lds32",
"10100xxxxxxxxxxx": "lds16",
"1001000xxxxx1111": "pop",
"1001001xxxxx1111": "push",
"10010111xxxxxxxx": "sbiw",
"1001001xxxxx0000": "sts32",
"10101xxxxxxxxxxx": "sts16",
}

AVR2_5 = {
**AVR2,
"00000001xxxxxxxx": "movw",
}

AVR3 = {
**AVR2_5,
"1001010xxxxx111x": "call",
"1001010xxxxx110x": "jmp",
}

AVR3_1 = {
**AVR3,
"1001010111011000": "elpmz1",
"1001000xxxxx0110": "elpmz2",
"1001000xxxxx0111": "elpmz3",
}

AVR4 = {
**AVR3_1,
"000000110xxx1xxx": "fmul",
"000000111xxx0xxx": "fmuls",
"000000111xxx1xxx": "fmulsu",
"100111xxxxxxxxxx": "mul",
"00000010xxxxxxxx": "muls",
"000000110xxx0xxx": "mulsu",
"1001010111101000": "spm",
# figure out spm #2
}

AVR5 = {
**AVR4,
"1001010110011000": "break",
}

AVR5_1 = {
**AVR5,
"": "elpmx",
}







if __name__ == '__main__':
    opcodes = generate_opcode_dice(AVR4)
    
    largest = 0
    for code in opcodes:
        if code > largest:
            largest = code
            
    print("Compiles %i opcodes, largest: %i" %(len(opcodes), largest))
    
    
    opcode_funcs = "void (*opcodes[%i]) (microcontroller_t*) = {" %largest
    for i in range(largest):
        if i in opcodes:
            opcode_funcs += '_%s,' %opcodes[i]
        else:
            opcode_funcs += '0,'
            
        if i % 16 == 0 and i != 0:
            opcode_funcs += '\n'
    
    opcode_funcs += "};\n"
    
    f = open("include/instructions.h", 'w')
    f.write("#ifndef INSTRUCTIONS_H\n")
    f.write("#define INSTRUCTIONS_H\n")
    f.write('#include "microcontroller.h"\n')
    f.write("\n")
    
    for code, op in AVR5_1.items():
        f.write('void _%s(microcontroller_t* microcontroller);\n' %op)
    
    f.write("\n");
    f.write(opcode_funcs)
    f.write("#endif")
    f.close()

    opcode_names = "void (*print_opcodes[%i]) (WINDOW*, microcontroller_t*) = {" %largest
    for i in range(largest):
        if i in opcodes:
            opcode_names += 'print_%s,' %opcodes[i]
        else:
            opcode_names += '0,'
            
        if i % 16 == 0 and i != 0:
            opcode_names += '\n'
    
    opcode_names += "};\n"
    
    f = open("include/print_instructions.h", 'w')
    f.write("#ifndef PRINT_INSTRUCTIONS_H\n")
    f.write("#define PRINT_INSTRUCTIONS_H\n")
    f.write("#include <ncurses.h>\n")
    f.write('#include "microcontroller.h"\n')
    f.write("\n")
    
    for code, op in AVR5_1.items():
        f.write('void print_%s(WINDOW* win, microcontroller_t* microcontroller);\n' %op)
    
    f.write("\n");
    f.write(opcode_names)
    f.write("#endif")
    f.close()
    """
    f = open('src/instructions.c', 'w')
    f.write('#include <stdio.h>\n')
    f.write('#include <stdint.h>\n')
    f.write('#include "util.h"\n')
    f.write('#include "microcontroller.h"\n')
    f.write('\n')
    
    for code, op in AVR5_1.items():
        f.write('void _%s(microcontroller_t* microcontroller)\n' %op)
        f.write('{\n')
        f.write('    printf("Calling _%s");\n' %op)
        f.write('}\n')
        f.write('\n')
        
    f.close()
    """
